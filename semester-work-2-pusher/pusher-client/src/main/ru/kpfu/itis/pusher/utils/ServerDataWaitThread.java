package main.ru.kpfu.itis.pusher.utils;

import javafx.collections.ObservableList;
import main.ru.kpfu.itis.pusher.Application;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.model.PlayerProps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class ServerDataWaitThread extends Thread {

    private Socket serverSocket;
    private Application application;
    private PlayerJsonConverter playerJsonConverter;

    public ServerDataWaitThread(Socket serverSocket, Application application) {
        this.serverSocket = serverSocket;
        this.application = application;
        playerJsonConverter = new PlayerJsonConverter();
    }

    @Override
    public void run() {
        BufferedReader bufferedReader = getSocketBufferedReader();
        while (true) {
            String jsonResultTable = "";
            String s;
            try {
                while ((s = bufferedReader.readLine()) == null) {
                }
                jsonResultTable += s;
            } catch (IOException e) {
                e.printStackTrace();
            }
            updateCurrentResultTable(jsonResultTable);
        }
    }

    private void updateCurrentResultTable(String jsonResultTable) {
        List<PlayerProps> updatedPlayers = getPlayersByJsonResultTable(jsonResultTable);
        ObservableList<PlayerProps> currentPlayers = application.getPlayers();
        currentPlayers.clear();
        currentPlayers.addAll(updatedPlayers);
    }

    private List<PlayerProps> getPlayersByJsonResultTable(String jsonResultTable) {
        List<Player> players = playerJsonConverter.getPlayerListByJsonString(jsonResultTable);
        return players.stream()
                .map(PlayerProps::initOnPlayer)
                .collect(Collectors.toList());
    }

    private BufferedReader getSocketBufferedReader() {
        try {
            return new BufferedReader(
                    new InputStreamReader(
                            serverSocket.getInputStream()
                    )
            );
        } catch (IOException e) {
            throw new IllegalArgumentException("Socket Problem");
        }
    }
}
