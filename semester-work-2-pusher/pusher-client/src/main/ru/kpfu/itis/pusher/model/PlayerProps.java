package main.ru.kpfu.itis.pusher.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class PlayerProps {
    private StringProperty username;
    private IntegerProperty score;

    public PlayerProps(StringProperty username, IntegerProperty score) {
        this.username = username;
        this.score = score;
    }

    public PlayerProps(String username, Integer score) {
        this.username = new SimpleStringProperty(username);
        this.score = new SimpleIntegerProperty(score);
    }

    private PlayerProps(Player player) {
        this.username = new SimpleStringProperty(player.getUsername());
        this.score = new SimpleIntegerProperty(player.getScore());
    }

    public static PlayerProps initOnPlayer(Player player) {
        return new PlayerProps(player);
    }

    public String getUsername() {
        return username.get();
    }

    public StringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public int getScore() {
        return score.get();
    }

    public IntegerProperty scoreProperty() {
        return score;
    }

    public void setScore(int score) {
        this.score.set(score);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        PlayerProps player = (PlayerProps) obj;
        String username = player.getUsername();
        Integer score = player.getScore();
        return score.equals(getScore()) && username.equals(getUsername());
    }

    @Override
    public String toString() {
        return "PlayerProps: [username: " + getUsername() + ", score: " + getScore() + "]";
    }
}
