package main.ru.kpfu.itis.pusher.viewAndControllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import main.ru.kpfu.itis.pusher.Application;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.model.PlayerProps;
import main.ru.kpfu.itis.pusher.utils.PlayerSender;

import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class GameOverController {

    @FXML
    private Label result;
    @FXML
    private Label position;

    private Application application;
    private PlayerSender playerSender;
    private Player player;

    @FXML
    public void nextGameButtonHandle() {
        player.setScore(0);
        playerSender.sendPlayerToServer(player);
        application.loadGameWindow();
    }

    @FXML
    public void mainPageButtonHandle() {
        player.setFinished(true);
        playerSender.sendPlayerToServer(player);
        application.loadMainWindow();
    }

    public void init(Socket socket, Application application) {
        playerSender = new PlayerSender(socket);
        this.application = application;
        player = application.getPlayer();
        fillLabels();

    }

    private void fillLabels() {
        result.setText(player.getUsername() + ", your result is " + player.getScore());
        PlayerProps currentPlayerProps = PlayerProps.initOnPlayer(player);
        int rating = application.getPlayers().indexOf(currentPlayerProps) + 1;
        position.setText("Your current rating: " + rating);
    }
}
