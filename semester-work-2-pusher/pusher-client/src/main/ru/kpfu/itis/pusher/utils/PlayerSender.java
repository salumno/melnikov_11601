package main.ru.kpfu.itis.pusher.utils;

import main.ru.kpfu.itis.pusher.model.Player;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class PlayerSender {
    private PlayerJsonConverter playerJsonConverter;
    private PrintWriter printWriter;

    public PlayerSender(Socket socket) {
        printWriter = getServerSocketPrintWriter(socket);
        playerJsonConverter = new PlayerJsonConverter();
    }

    public void sendPlayerToServer(Player player) {
        String jsonPlayer = playerJsonConverter.getJsonStringOnPlayer(player);
        printWriter.println(jsonPlayer);
    }

    private PrintWriter getServerSocketPrintWriter(Socket socket) {
        try {
            return new PrintWriter(
                    socket.getOutputStream(), true
            );
        } catch (IOException e) {
            throw new IllegalArgumentException("Socket IO exception");
        }
    }


}
