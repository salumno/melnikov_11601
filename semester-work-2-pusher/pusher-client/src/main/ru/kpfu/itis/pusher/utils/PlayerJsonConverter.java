package main.ru.kpfu.itis.pusher.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import main.ru.kpfu.itis.pusher.model.Player;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class PlayerJsonConverter {

    public String getJsonStringOnPlayerList(List<Player> players) {
        ObjectWriter ow = new ObjectMapper().writer();
        try {
            return ow.writeValueAsString(players);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getJsonStringOnPlayer(Player player) {
        ObjectWriter ow = new ObjectMapper().writer();
        try {
            return ow.writeValueAsString(player);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Player> getPlayerListByJsonString(String jsonPlayerList) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return Arrays.asList(objectMapper.readValue(jsonPlayerList, Player[].class));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
