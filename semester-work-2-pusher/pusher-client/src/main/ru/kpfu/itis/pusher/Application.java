package main.ru.kpfu.itis.pusher;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.model.PlayerProps;
import main.ru.kpfu.itis.pusher.utils.ServerDataWaitThread;
import main.ru.kpfu.itis.pusher.viewAndControllers.GameController;
import main.ru.kpfu.itis.pusher.viewAndControllers.GameOverController;
import main.ru.kpfu.itis.pusher.viewAndControllers.MainController;

import java.io.IOException;
import java.net.Socket;

public class Application extends javafx.application.Application {

    private final int PORT = 1234;
    private String host;

    private ObservableList<PlayerProps> players = FXCollections.observableArrayList();
    private Player player;

    private Stage primaryStage;
    private Socket serverSocket;
    private ServerDataWaitThread serverDataWaitThread;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        this.primaryStage = primaryStage;
        primaryStage.setTitle("Pusher");

        loadMainWindow();
    }

    public void connectToHost(String host) {
        this.host = host;
        try {
            serverSocket = new Socket(host, PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connected to the PUSHER server on HOST " + host + " and PORT " + PORT + ".");
        startServerDataWaitThread(serverSocket);
    }

    private void startServerDataWaitThread(Socket serverSocket) {
        serverDataWaitThread = new ServerDataWaitThread(serverSocket, this);
        serverDataWaitThread.start();
    }


    public void loadMainWindow() {
        FXMLLoader loader = new FXMLLoader(Application.class.getResource("viewAndControllers/main.fxml"));
        try {
            AnchorPane root = loader.load();
            primaryStage.setScene(new Scene(root, 400, 400));

            MainController mainController = loader.getController();
            mainController.init(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadGameWindow() {
        FXMLLoader loader = new FXMLLoader(Application.class.getResource("viewAndControllers/game.fxml"));
        try {
            HBox root = loader.load();
            primaryStage.setScene(new Scene(root, 600, 400));

            GameController gameController = loader.getController();
            gameController.init(serverSocket, this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadGameOverWindow() {
        FXMLLoader loader = new FXMLLoader(Application.class.getResource("viewAndControllers/game-over.fxml"));
        try {
            AnchorPane root = loader.load();
            primaryStage.setScene(new Scene(root, 400, 400));

            GameOverController gameOverController = loader.getController();
            gameOverController.init(serverSocket, this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ObservableList<PlayerProps> getPlayers() {
        return players;
    }

    public void setPlayers(ObservableList<PlayerProps> players) {
        this.players = players;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Socket getServerSocket() {
        return serverSocket;
    }

    public String getHost() {
        return host;
    }
}
