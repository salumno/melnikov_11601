package main.ru.kpfu.itis.pusher.viewAndControllers;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Duration;
import main.ru.kpfu.itis.pusher.Application;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.model.PlayerProps;
import main.ru.kpfu.itis.pusher.utils.PlayerSender;

import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class GameController {

    @FXML
    private Label scoreLabel = new Label();

    @FXML
    private Label timeLabel = new Label();

    @FXML
    private TableView<PlayerProps> resultTableView = new TableView<>();

    @FXML
    private TableColumn<PlayerProps, String> usernameColumn = new TableColumn<>();

    @FXML
    private TableColumn<PlayerProps, Integer> scoreColumn = new TableColumn<>();

    private Integer timeCounter;
    private Timeline timeline;

    private Application application;
    private Player player;
    private PlayerSender playerSender;

    private boolean isTimerStarted = false;

    @FXML
    public void pushButtonHandle() {
        if (!isTimerStarted) {
            isTimerStarted = true;
            startTimer();
        }
        player.setScore(player.getScore() + 1);
        playerSender.sendPlayerToServer(player);
        Integer currentScore = player.getScore();
        scoreLabel.setText(currentScore + "");
    }

    private void startTimer() {
        timeline = new Timeline(new KeyFrame(
                Duration.millis(1000),
                ae -> changeTimerStaffState()
        ));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void changeTimerStaffState() {
        timeCounter -= 1;
        timeLabel.setText("0:" + timeCounter);
        if (timeCounter == 0) {
            timeline.stop();
            timeIsOver();
        }
    }


    private void timeIsOver() {
        player.setScore(Integer.parseInt(scoreLabel.getText()));
        playerSender.sendPlayerToServer(player);
        application.loadGameOverWindow();
    }

    @FXML
    private void initialize() {
        usernameColumn.setCellValueFactory(cellData -> cellData.getValue().usernameProperty());
        scoreColumn.setCellValueFactory(cellData -> cellData.getValue().scoreProperty().asObject());
    }

    public void init(Socket serverSocket, Application application) {
        this.application = application;
        this.player = application.getPlayer();
        playerSender = new PlayerSender(serverSocket);
        fillComponents();

    }

    private void fillComponents() {
        timeCounter = 30;
        timeLabel.setText("0:" + timeCounter);
        scoreLabel.setText("0");
        resultTableView.setItems(application.getPlayers());
    }
}
