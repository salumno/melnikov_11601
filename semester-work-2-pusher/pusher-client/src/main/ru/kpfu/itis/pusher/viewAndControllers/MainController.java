package main.ru.kpfu.itis.pusher.viewAndControllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.ru.kpfu.itis.pusher.Application;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.utils.PlayerSender;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MainController {

    @FXML
    private Label errorLabel;

    @FXML
    private TextField usernameTextField;

    @FXML
    private TextField host;

    private Application application;
    private PlayerSender playerSender;

    @FXML
    public void startGameButtonHandle() {
        if (checkFields()) {
            confirmEnterToGame();
        } else {
            printError();
        }
    }

    private boolean checkFields() {
        return !usernameTextField.getText().isEmpty() && !host.getText().isEmpty();
    }

    private void printError() {
        errorLabel.setText("ERROR! YOUR FIELDS ARE EMPTY!");
    }

    private void confirmEnterToGame() {
        Player player = new Player(usernameTextField.getText(), 0);
        application.setPlayer(player);
        application.connectToHost(host.getText());
        playerSender = new PlayerSender(application.getServerSocket());
        playerSender.sendPlayerToServer(player);
        application.loadGameWindow();
    }

    public void init(Application application) {
        this.application = application;
    }
}
