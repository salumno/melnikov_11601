package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.ru.kpfu.itis.pusher.model.Player;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class testPlayerEqualsMethod {

    private Player player;

    @Before
    public void init() {
        player = new Player("Sema", 5);
    }

    @Test
    public void testEquals() {
        Player testPlayer = new Player("Sema", 5);
        boolean actualEquals = player.equals(testPlayer);
        Assert.assertEquals(true, actualEquals);
    }
}
