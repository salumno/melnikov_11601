package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.utils.PlayerJsonConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class testPlayerListByJsonStringMethodOfPlayerJsonConverter {
    private final PlayerJsonConverter playerJsonConverter = new PlayerJsonConverter();
    private String jsonPlayersList;

    @Before
    public void init() {
        jsonPlayersList = "[{\"username\":\"Sema\",\"score\":50,\"finished\":false}, {\"username\":\"Leha\",\"score\":45,\"finished\":false}]";
    }

    @Test
    public void testGetJsonStringOnPlayer() {
        List<Player> expectedPlayerList = new ArrayList<>();
        expectedPlayerList.add(new Player("Sema", 50));
        expectedPlayerList.add(new Player("Leha", 45));
        Assert.assertEquals(
                expectedPlayerList,
                playerJsonConverter.getPlayerListByJsonString(jsonPlayersList)
        );
    }
}
