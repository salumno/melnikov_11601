package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.ru.kpfu.itis.pusher.model.PlayerProps;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class testPlayerPropsEquals {

    private final List<PlayerProps> playerPropsList = new ArrayList<>();

    @Before
    public void setup() {
        playerPropsList.add(new PlayerProps("Sema", 0));
        playerPropsList.add(new PlayerProps("Timur", 14));
        playerPropsList.add(new PlayerProps("Konstantin", 15));
        playerPropsList.add(new PlayerProps("Shimon", 5));
    }

    @Test
    public void testEquals() {
        PlayerProps neededPlayerProps = new PlayerProps("Konstantin", 15);
        PlayerProps actualPlayerProps = playerPropsList.get(2);
        Assert.assertEquals(true, neededPlayerProps.equals(actualPlayerProps));
    }
}
