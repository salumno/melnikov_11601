package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.ru.kpfu.itis.pusher.model.PlayerProps;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class testFindPlayerPropsInListByIndexOf {

    private final List<PlayerProps> playerPropsList = new ArrayList<>();

    @Before
    public void setup() {
        playerPropsList.add(new PlayerProps("Sema", 0));
        playerPropsList.add(new PlayerProps("Timur", 14));
        playerPropsList.add(new PlayerProps("Konstantin", 15));
        playerPropsList.add(new PlayerProps("Shimon", 5));
    }

    @Test
    public void testPlayerPropsIndexOf() {
        PlayerProps neededPlayerProps = new PlayerProps("Konstantin", 15);
        int expectedIndex = 2;
        int actualIndex = playerPropsList.indexOf(neededPlayerProps);
        Assert.assertEquals(expectedIndex, actualIndex);
    }

}
