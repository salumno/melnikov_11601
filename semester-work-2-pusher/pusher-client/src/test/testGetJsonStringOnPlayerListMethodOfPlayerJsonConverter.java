package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.utils.PlayerJsonConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class testGetJsonStringOnPlayerListMethodOfPlayerJsonConverter {
    private final PlayerJsonConverter playerJsonConverter = new PlayerJsonConverter();
    private List<Player> players = new ArrayList<>();

    @Before
    public void init() {
        players.add(new Player("Sema", 50));
        players.add(new Player("Kesha", 48));
    }

    @Test
    public void testGetJsonStringOnPlayer() {
        String expectedJson = "[{\"username\":\"Sema\",\"score\":50,\"finished\":false},{\"username\":\"Kesha\",\"score\":48,\"finished\":false}]";
        Assert.assertEquals(expectedJson, playerJsonConverter.getJsonStringOnPlayerList(players));
    }
}
