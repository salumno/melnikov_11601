package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.ru.kpfu.itis.pusher.model.Player;
import main.ru.kpfu.itis.pusher.utils.PlayerJsonConverter;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class testGetJsonStringOnPlayerMethodOfPlayerJsonConverter {
    private final PlayerJsonConverter playerJsonConverter = new PlayerJsonConverter();
    private Player player;

    @Before
    public void init() {
        player = new Player("Sema", 50);
    }

    @Test
    public void testGetJsonStringOnPlayer() {
        String expectedJson = "{\"username\":\"Sema\",\"score\":50,\"finished\":false}";
        Assert.assertEquals(expectedJson, playerJsonConverter.getJsonStringOnPlayer(player));
    }
}
