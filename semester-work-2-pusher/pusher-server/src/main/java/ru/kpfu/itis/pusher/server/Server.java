package ru.kpfu.itis.pusher.server;

import ru.kpfu.itis.pusher.model.Player;
import ru.kpfu.itis.pusher.utils.PlayerJsonConverter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Server {

    private final int PORT = 1234;
    private List<Connection> connections;
    private List<Player> players;
    private PlayerJsonConverter playerJsonConverter;

    public Server() {
        this.playerJsonConverter = new PlayerJsonConverter();
        this.connections = new ArrayList<>();
        this.players = new ArrayList<>();
    }

    public void start() {
        printServerStartMessage();
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            while (true) {
                Socket client = serverSocket.accept();
                Connection currentClientConnection = new Connection(this, client);
                connections.add(currentClientConnection);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printServerStartMessage() {
        System.out.println("PUSHER server started on PORT " + PORT);
        System.out.println(LocalDate.now().toString());
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public PlayerJsonConverter getPlayerJsonConverter() {
        return playerJsonConverter;
    }
}
