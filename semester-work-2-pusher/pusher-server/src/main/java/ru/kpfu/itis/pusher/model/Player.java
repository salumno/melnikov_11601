package ru.kpfu.itis.pusher.model;


/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Player {
    private String username;
    private Integer score;
    private boolean isFinished;

    public Player() {
    }

    public Player(String username, Integer score) {
        this.username = username;
        this.score = score;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Player player = (Player) obj;
        return score.equals(player.getScore()) && username.equals(player.getUsername());
    }

    @Override
    public String toString() {
        return "Player{" +
                "username='" + username + '\'' +
                ", score=" + score +
                '}';
    }
}
