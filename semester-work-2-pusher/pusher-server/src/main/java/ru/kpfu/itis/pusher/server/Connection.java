package ru.kpfu.itis.pusher.server;

import ru.kpfu.itis.pusher.model.ConnectionStatus;
import ru.kpfu.itis.pusher.model.Player;
import ru.kpfu.itis.pusher.utils.PlayerComparator;
import ru.kpfu.itis.pusher.utils.PlayerJsonConverter;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Connection implements Runnable {

    private Server server;
    private Thread thread;
    private Socket socket;
    private ConnectionStatus connectionStatus;
    private PlayerJsonConverter playerJsonConverter;

    public Connection(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
        connectionStatus = ConnectionStatus.NOT_CONFIRMED;
        playerJsonConverter = server.getPlayerJsonConverter();
        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        BufferedReader bufferedReader = getSocketBufferedReader(socket);
        while (true) {
            String s;
            String jsonPlayer = "";
            try {
                while ((s = bufferedReader.readLine()) == null) {
                }
                jsonPlayer += s;
                processCurrentPlayer(jsonPlayer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void processCurrentPlayer(String jsonPlayer) {
        Player player = playerJsonConverter.getPlayerByJsonString(jsonPlayer);
        List<Player> players = server.getPlayers();
        if (connectionStatus.equals(ConnectionStatus.CONFIRMED)) {
            updatePlayer(player, players);
        } else if (connectionStatus.equals(ConnectionStatus.NOT_CONFIRMED)) {
            printNewPlayerMessage(player);
            addNewPlayer(player, players);
        }
        returnToAllClientsJSONResultTable(players);
    }

    private void printNewPlayerMessage(Player player) {
        System.out.println("Player " + player.getUsername() + " connected to the PUSHER");
    }

    private void printExitPlayerMessage(Player player) {
        System.out.println("Player " + player.getUsername() + " disconnected from the PUSHER");
    }

    private void returnToAllClientsJSONResultTable(List<Player> players) {
        players.sort(new PlayerComparator());
        String resultTableJSON = playerJsonConverter.getJsonStringOnPlayerList(players);
        List<Connection> connections = server.getConnections();
        for (Connection connection: connections) {
            PrintWriter printWriter = getSocketPrintWriter(connection.getSocket());
            printWriter.println(resultTableJSON);
        }
    }

    private void updatePlayer(Player player, List<Player> players) {
        if (player.isFinished()) {
            players.remove(player);
            printExitPlayerMessage(player);
        } else {
            players.stream()
                    .filter(
                            currentPlayer -> currentPlayer.getUsername().equals(player.getUsername())
                    )
                    .forEach(
                            currentPlayer -> currentPlayer.setScore(player.getScore())
                    );
        }
    }

    private void addNewPlayer(Player player, List<Player> players) {
        players.add(player);
        connectionStatus = ConnectionStatus.CONFIRMED;
    }

    private PrintWriter getSocketPrintWriter(Socket socket) {
        try {
            return new PrintWriter(
                    socket.getOutputStream(), true
            );
        } catch (IOException e) {
            throw new IllegalArgumentException("Socket IO exception");
        }
    }

    private BufferedReader getSocketBufferedReader(Socket socket) {
        try {
            return new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );
        } catch (IOException e) {
            throw new IllegalArgumentException("Socket IO exception");
        }
    }

    private Socket getSocket() {
        return socket;
    }
}
