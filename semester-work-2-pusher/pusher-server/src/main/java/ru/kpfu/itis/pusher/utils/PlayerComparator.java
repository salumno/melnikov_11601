package ru.kpfu.itis.pusher.utils;

import ru.kpfu.itis.pusher.model.Player;

import java.util.Comparator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class PlayerComparator implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        int score1 = o1.getScore();
        int score2 = o2.getScore();
        if (score1 > score2) {
            return -1;
        } else if (score1 < score2) {
            return 1;
        } else return 0;
    }
}
