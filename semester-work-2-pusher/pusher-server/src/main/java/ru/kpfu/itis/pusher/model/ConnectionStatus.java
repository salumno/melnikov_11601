package ru.kpfu.itis.pusher.model;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public enum ConnectionStatus {
    CONFIRMED, NOT_CONFIRMED
}
