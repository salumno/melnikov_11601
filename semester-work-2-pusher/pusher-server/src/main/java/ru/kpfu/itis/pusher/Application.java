package ru.kpfu.itis.pusher;

import ru.kpfu.itis.pusher.server.Server;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Application {
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
}
