package compilator;

import compilator.view.CompilerController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main extends Application{

    private final int identificatorLength = 4;
    private final int numberLength = 8;
    private final List<Character> specialOperators = new ArrayList<>(Arrays.asList('→', '*', '∨', '∧', '+'));
    private Map<String, String> values = new HashMap<>();
    private final String[] ERROR = {
            "Incorrect identifier in the left part.",
            "Incorrect assignment.",
            "Incorrect statement in the right part.",
            "NullPointerException.",
            "Missing semicolon."
    };
    private CompilerController controller;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/Compiler.fxml"));
        AnchorPane page = loader.load();
        Stage dialogStage = new Stage();
        dialogStage.setResizable(false);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);
        dialogStage.setTitle("Nescafe 3 in 1 Compiler");

        controller = loader.getController();

        controller.setMain(this);
        controller.setValue(values);

        dialogStage.showAndWait();
    }

    public boolean compile(String input) {
        values.clear();
        input = input.trim();
        String program = input.replaceAll("\n", "");
        if(checkSemicolon(input)) {
            String[] lines = program.split(";");
            System.out.println(lines.length);
            for (String line : lines) {
                if (checkAssignOperatorExist(line)) {
                    String[] parts = line.split(":=");
                    if (!isIdentifier(parts[0])) {
                        controller.setError(ERROR[0]);
                        return false;
                    }
                    parts[0] = parts[0].trim();
                    if (!resolveRightPart(parts[0], parts[1])) {
                        return false;
                    }
                } else {
                    controller.setError(ERROR[1]);
                    return false;
                }
            }
            return true;
        } else {
            controller.setError(ERROR[4]);
            return false;
        }
    }

    private boolean checkSemicolon(String input){
        int countAssignment = 0;
        int countSemicolon = 0;
        for (int i = 0; i < input.length() - 1 ; i++) {
            if (input.charAt(i) == ':' && input.charAt(i + 1) == '=') {
                countAssignment++;
            }
        }
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ';'){
                countSemicolon++;
            }
        }
        return countAssignment == countSemicolon;

    }
    private boolean resolveRightPart(String identifier, String statement) {
        if (checkOperationExist(statement)) {
            int operatorIndex = findOperatorIndex(statement);
            char operator = statement.charAt(operatorIndex);
            String num1 = returnValue(statement.substring(0, operatorIndex));
            String num2 = returnValue(statement.substring(operatorIndex + 1));
            String result = calculate(num1, num2, operator);
            if (result != null) {
                values.put(identifier, result);
            } else {
                controller.setError(ERROR[3]);
                return false;
            }
        } else {
            statement = statement.trim();
            if (isNumber(statement)) {
                values.put(identifier, statement);
            } else {
                if (values.containsKey(statement)) {
                    values.put(identifier, values.get(statement));
                } else {
                    controller.setError(ERROR[2]);
                    return false;
                }
            }
        }
        return true;
    }

    private String returnValue(String num) {
        String number = num.trim();
        if (isNumber(number)) {
            return number;
        } else {
            if (values.containsKey(number)) {
                return values.get(number);
            } else {
                return null;
            }
        }
    }

    private String calculate(String num1, String num2, Character operator) {
        if (num1 == null || num2 == null) {
            return null;
        }
        switch (operator) {
            case '∨':
                return disjunction(num1, num2);
            case '∧':
                return conjunction(num1, num2);
            case '+':
                return xor(num1, num2);
            case '→':
                return implication(num1, num2);
            case '*':
                return shefferBar(num1, num2);
            default:
                return "I'm sorry, something went wrong.";
        }
    }

    private String disjunction(String num1, String num2) {
        String result = "";
        for (int i = 0; i < num1.length(); i++) {
            if (num1.charAt(i) == '1' || num2.charAt(i) == '1') {
                result += '1';
            } else {
                result += '0';
            }
        }
        return result;
    }

    private String conjunction(String num1, String num2) {
        String result = "";
        for (int i = 0; i < num1.length(); i++) {
            if (num1.charAt(i) == '1' && num2.charAt(i) == '1') {
                result += '1';
            } else {
                result += '0';
            }
        }
        return result;
    }

    private String xor(String num1, String num2) {
        String result = "";
        for (int i = 0; i < num1.length(); i++) {
            char currentSymbol1 = num1.charAt(i);
            char currentSymbol2 = num2.charAt(i);
            if ((currentSymbol1 == '1' && currentSymbol2 == '0') || (currentSymbol1 == '0' && currentSymbol2 == '1')) {
                result += '1';
            } else {
                result += '0';
            }
        }
        return result;
    }

    private String shefferBar(String num1, String num2) {
        String result = "";
        for (int i = 0; i < num1.length(); i++) {
            if (num1.charAt(i) == '1' && num2.charAt(i) == '1') {
                result += '0';
            } else {
                result += '1';
            }
        }
        return result;
    }

    private String implication(String num1, String num2) {
        String result = "";
        for (int i = 0; i < num1.length(); i++) {
            if (num1.charAt(i) == '1' && num2.charAt(i) == '0') {
                result += '0';
            } else {
                result += '1';
            }
        }
        return result;
    }

    private int findOperatorIndex(String right) {
        for (int i = 0; i < right.length(); i++) {
            if (specialOperators.contains(right.charAt(i))) {
                return i;
            }
        }
        return 'ё';
    }

    private boolean isNumber(String input) {
        String regex = "(\\s)*[01]{" + numberLength + "}(\\s)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(input);
        return m.matches();
    }

    private boolean isIdentifier(String input) {
        String regex = "(\\s)*[A-Za-z][A-Za-z\\d]{0," + (identificatorLength - 1) + "}(\\s)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(input);
        return m.matches();
    }

    private boolean checkOperationExist(String line) {
        int count = 0;
        for (int i = 0; i < line.length() && count < 2; i++) {
            if (specialOperators.contains(line.charAt(i))) {
                count++;
            }
        }
        return count == 1;
    }

    private boolean checkAssignOperatorExist(String line) {
        int count = 0;
        for (int i = 0; i < line.length() - 1 && count < 2; i++) {
            if (line.charAt(i) == ':' && line.charAt(i + 1) == '=') {
                count++;
            }
        }
        return count == 1;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
