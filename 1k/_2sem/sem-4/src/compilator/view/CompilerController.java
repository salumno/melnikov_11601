package compilator.view;

import compilator.Main;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.util.Map;

public class CompilerController {
    @FXML
    private TextArea textArea;
    @FXML
    private TextArea log;

    private Main main;
    private Map<String, String> value;
    private String error;

    @FXML
    private void resultButtonHandle() {
        log.clear();
        String program = textArea.getText();
        boolean compileResult = main.compile(program);
        System.out.println(compileResult);
        if (compileResult) {
            StringBuilder logText = new StringBuilder("Successful compilation\n");
            for(Map.Entry<String,String> data : value.entrySet()){
                String identifier = data.getKey();
                String value = data.getValue();
                String statement = identifier + " = " + value + ";\n";
                logText.append(statement);
            }
            log.setText(logText.toString());
        } else {
            log.setText("Compilation error!\n" + error);
        }
        System.out.println(value.toString());
    }

    @FXML
    private void disjunctionButtonHandle(){
        textArea.setText(textArea.getText() + "∨");
    }
    @FXML
    private void conjunctionButtonHandle(){
        textArea.setText(textArea.getText() + "∧");
    }
    @FXML
    private void shefferBarButtonHandle(){
        textArea.setText(textArea.getText() + "*");
    }
    @FXML
    private void xorButtonHandle(){
        textArea.setText(textArea.getText() + "+");
    }
    @FXML
    private void implicationButtonHandle(){
        textArea.setText(textArea.getText() + "→");
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public void setValue(Map<String, String> value) {
        this.value = value;
    }

    public void setError(String error){this.error = error;}
}
