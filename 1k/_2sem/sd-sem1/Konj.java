import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 * Variant 4 
 */
public class Konj {

    private Konj next;
    private ArrayList<Integer> konjSet = new ArrayList<>();

    public Konj() {}

    public Konj(String konj) {
        boolean checkMinus = false;
        for (int i = 0; i < konj.length(); i++) {
            char curr = konj.charAt(i);
            if (curr == '-') {
                checkMinus = true;
            } else if (curr > '0' && curr < '6') {
                if (checkMinus) {
                    addElemToKonj(-1*(curr - '0'));
                    checkMinus = false;
                } else {
                    addElemToKonj(curr - '0');
                }
            }
        }
        sort();
    }

    public boolean value(boolean[] value) {
        boolean check = true;
        boolean[] v = Arrays.copyOf(value, value.length);
        for (int i = 0; i < getKonjSize() && check; i++) {
            int curr = Math.abs(getKonjElem(i));
            if (getKonjElem(i) < 0) {
                v[curr - 1] = !v[curr - 1];
            }
            check = check & v[curr - 1];
        }
        return check;
    }

    public Konj getNext() {
        return next;
    }

    public void setNext(Konj konj) {
        next = konj;
    }

    public void addElemToKonj(Integer elem) {
        konjSet.add(elem);

    }

    public int getKonjElem(int i) {
        return konjSet.get(i);
    }

    public int getKonjSize() {
        return konjSet.size();
    }

    public ArrayList<Integer> getKonjSet() {
        return konjSet;
    }

    public void setKonjSet(ArrayList<Integer> set) {
        this.konjSet = set;
    }

    public void sort() {
        konjSet.sort((o1, o2) -> {
            int num1 = Math.abs(o1);
            int num2 = Math.abs(o2);
            if (num1 < num2) {
                return -1;
            } else if (num1 == num2) {
                return 0;
            } else {
                return 1;
            }
        });
    }

    public Konj cloneKonjSet() {
        Konj newKonj = new Konj();
        ArrayList<Integer> currentKonjSet = (ArrayList<Integer>)getKonjSet().clone();
        newKonj.setKonjSet(currentKonjSet);
        return newKonj;
    }

    public boolean equals(Konj konj) {
        if (konj == null) {
            return false;
        }
        if (getKonjSize() != konj.getKonjSize()) {
            return false;
        }
        for (Integer currentElem : konjSet) {
            if (!konj.getKonjSet().contains(currentElem)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < getKonjSize(); i++) {
            int currElem = getKonjElem(i);
            if (currElem < 0) {
                string.append("-");
            }
            string.append("X").append(Math.abs(currElem));
            if (i != getKonjSize() - 1) {
                string.append("&");
            }
        }
        return string.toString();
    }
}
