import java.util.Arrays;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 * Variant 4
 */
public class DNFTestDrive {
    public static void main(String[] args) {
        DNF dnf1 = new DNF("X1&X3&X2V-X3&-X2");
        System.out.println("dnf1: " + dnf1);

        boolean[] value = {false, true, false, false, true};
        System.out.println("We've got this array of values: " + Arrays.toString(value));
        System.out.println("Value of dnf1: " + dnf1.value(value));

        boolean[] value2 = {false, false, false, false, true};
        System.out.println("Change a bit: " + Arrays.toString(value2));
        System.out.println("Value of dnf1 now: " + dnf1.value(value2));

        DNF dnf2 = new DNF("X3&X2");
        System.out.println("dnf2: " + dnf2);

        DNF dnf3 = dnf1.dnfWith(1);
        dnf3.insert(new Konj("X1&-X2&-X3"));
        System.out.println("dnf3 = dnf1 with 1: " + dnf3);

        System.out.println("dnf1 still without changes " + dnf1);

        DNF dnf4 = dnf1.disj(dnf2);
        System.out.println("new dnf4 = dnf1 V dnf2: " + dnf4);

        DNF dnf5 = dnf4.disj(dnf2);
        System.out.println("new dnf5 = (dnf4 V dnf2) looks like dnf4, cause already have dnf2: " + dnf5);

        dnf2.insert(new Konj("X4"));
        System.out.println("We changed dnf2(add X4): " + dnf2);
        System.out.println("But dnf4 that includes dnf2 ain't change. dnf4: " + dnf4);

        dnf5.insert(new Konj("X1&X4&X3&X5"));
        dnf5.insert(new Konj("X1"));
        System.out.println("Updated dnf5(Add X1&X4&X3&X5 and X1): " + dnf5);
        dnf5.sortByLength();
        System.out.println("Sorted dnf5: " + dnf5);

    }
}
