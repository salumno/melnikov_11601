import java.util.ArrayList;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 * Variant 4
 */
public class DNF {

    private Konj head;
    private Konj tail;

    public DNF() {}

    public DNF(String s) {
        String[] konjs = s.split("V");
        for (String konj : konjs) {
            this.insert(new Konj(konj));
        }
    }

    private Konj getHead() {
        return head;
    }

    public void insert(Konj konj) {
        if (!checkKonjInDNF(konj)) {
            konj.sort();
            if (head == null) {
                head = konj;
                tail = konj;
            } else {
                tail.setNext(konj);
                tail = konj;
            }
        }
    }

    public DNF disj(DNF dnf) {
        DNF result = new DNF();
        for (Konj konj = head; konj != null; konj = konj.getNext()) {
            Konj newKonj = konj.cloneKonjSet();
            result.insert(newKonj);
        }
        for (Konj konj = dnf.getHead(); konj != null; konj = konj.getNext()) {
            if (!checkKonjInDNF(konj)) {
                Konj newKonj = konj.cloneKonjSet();
                result.insert(newKonj);
            }
        }
        return result;
    }

    public boolean value(boolean[] values) {
        boolean check = false;
        for (Konj konj = head; konj != null & !check; konj = konj.getNext()) {
            check = check | konj.value(values);
        }
        return check;
    }

    public void sortByLength() {
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (Konj i = head; i.getNext() != null; i = i.getNext()) {
                if (i.getKonjSize() > i.getNext().getKonjSize()) {
                    ArrayList<Integer> temp = i.getKonjSet();
                    i.setKonjSet(i.getNext().getKonjSet());
                    i.getNext().setKonjSet(temp);
                    swapped = true;
                }
            }
        }
    }

    public DNF dnfWith(int i) {
        DNF dnf = new DNF();
        for (Konj konj = head; konj != null; konj = konj.getNext()) {
            Konj newKonj = konj.cloneKonjSet();
            if (!newKonj.getKonjSet().contains(i)) {
                newKonj.addElemToKonj(i);
            }
            dnf.insert(newKonj);
        }
        return dnf;
    }

    private boolean checkKonjInDNF(Konj konj) {
        for (Konj currentKonj = head; currentKonj != null; currentKonj = currentKonj.getNext()) {
            if (currentKonj.equals(konj)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder string = new StringBuilder();
        for (Konj konj = head; konj != null; konj = konj.getNext()) {
            string.append(konj);
            if (konj.getNext() != null) {
                string.append("V");
            }
        }
        return string.toString();
    }
}
