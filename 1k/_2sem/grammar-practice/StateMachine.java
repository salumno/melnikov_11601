
/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class StateMachine {
    public boolean checkByStateMachineLeftGrammar(String input) {
        int state = 0;
        for (int i = 0; i < input.length(); i++) {
            boolean isZero = input.charAt(i) == '0';
            if (state == 0) {
                if (isZero) {
                    return false;
                } else {
                    state = 1;
                }
            } else if (state == 1) {
                if (isZero) {
                    return false;
                } else {
                    state = 2;
                }
            } else if (state == 2) {
                if (isZero) {
                    state = 3;
                }
            } else if (state == 3){
                if (!isZero) {
                    return false;
                }
            }
        }
        return state == 2 || state == 3;
    }

    public boolean checkByStateMachineRightGrammar(String input) {
        int state = 0;
        for (int i = 0; i < input.length(); i++) {
            boolean isZero = input.charAt(i) == '0';
            if (state == 0) {
                if (isZero) {
                    state = 1;
                } else {
                    state = 2;
                }
            } else if (state == 1) {
                if (isZero) {
                    state = 3;
                } else {
                    return false;
                }
            } else if (state == 2) {
                if (isZero) {
                    state = 4;
                } else {
                    return false;
                }
            } else if (state == 3 || state == 4){
                if (!isZero) {
                    return false;
                }
            }
        }
        return state == 3 || state == 4;
    }

    public static void main(String[] args) {
        StateMachine stateMachine = new StateMachine();
        System.out.println(stateMachine.checkByStateMachineLeftGrammar("111111100000001"));
        System.out.println(stateMachine.checkByStateMachineRightGrammar("1000"));
    }
}
