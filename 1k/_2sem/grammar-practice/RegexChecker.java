

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class RegexChecker {

    public boolean checkByRegex(String input, String regex) {
        return input.matches(regex);
    }

    public static void main(String[] args) {
        RegexChecker regexChecker = new RegexChecker();
        System.out.println(regexChecker.checkByRegex("0000", "[01]0+"));
        System.out.println(regexChecker.checkByRegex("10","11+0*"));
    }
}
