
/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class HardcodedAutomata {

    public boolean checkByAutomata(int[][] transition, int[] finalStates, String input) {
        int currentState = 0;
        input = input.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '0') {
                currentState = transition[currentState][0];
            } else {
                currentState = transition[currentState][1];
            }
        }
        for (int finalState : finalStates) {
            if (finalState == currentState) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        HardcodedAutomata hardcodedAutomata = new HardcodedAutomata();
        int[][] rightGrammarTable = {
                {1,2},
                {3,5},
                {4,5},
                {3,5},
                {4,5},
                {5,5}
        };
        int[] rightGrammarFinalStates = {3, 4};
        System.out.println(
                hardcodedAutomata.checkByAutomata(
                        rightGrammarTable,
                        rightGrammarFinalStates,
                        "00000000"
                )
        );
        int[][] leftGrammarTable = {
                {4,1},
                {4,2},
                {3,2},
                {3,4},
                {4,4}
        };
        int[] leftGrammarFinalStates = {2, 3};
        System.out.println(
                hardcodedAutomata.checkByAutomata(
                        leftGrammarTable,
                        leftGrammarFinalStates,
                        "0111000"
                )
        );
    }
}
