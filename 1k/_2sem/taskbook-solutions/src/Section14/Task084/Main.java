package Section14.Task084;

import java.io.*;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    private static String in = "src/Section14/Task084/in.txt";
    private static String out = "src/Section14/Task084/out.txt";

    public static void main(String[] args) {
        System.out.println(readAndWriteByLine());
        System.out.println(readAndWriteBySymbol());
    }

    private static long readAndWriteBySymbol() {
        try {
            long start = System.currentTimeMillis();
            FileReader fr = new FileReader(in);
            FileWriter fw = new FileWriter(out, true);
            int c;
            while ((c = fr.read()) != -1) {
                fw.write((char)c);
            }
            fr.close();
            fw.close();
            long finish = System.currentTimeMillis();
            return finish - start;
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private static long readAndWriteByLine() {
        try {
            long start = System.currentTimeMillis();
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(in)
                    )
            );
            PrintWriter pw = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(out)
                    )
            , true);
            String line;
            while ((line = br.readLine()) != null) {
                pw.println(line);
            }
            br.close();
            pw.close();
            long finish = System.currentTimeMillis();
            return finish - start;
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
