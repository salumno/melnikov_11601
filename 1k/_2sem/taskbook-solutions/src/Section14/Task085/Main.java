package Section14.Task085;

import Section12.Graph;

import java.io.*;
import java.util.Arrays;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    private static String in = "src/Section14/Task085/file.txt";

    public static void main(String[] args) {
        Graph[] graphs = new Graph[10];
        for (int i = 0; i < graphs.length; i++) {
            int currentNodeCount = (i+2) * 3;
            graphs[i] = new Graph(currentNodeCount, currentNodeCount * 2);
        }
        System.out.println(Arrays.toString(graphs));
        serialize(graphs);
        Graph[] graphs1 = deserialize();
        for (Graph graph: graphs1) {
            if (graph.getCountOfNodes() % 2 == 0) {
                System.out.println(graph);
            }
        }
    }

    private static Graph[] deserialize() {
        try {
            ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(in)
            );
            return (Graph[]) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void serialize(Graph[] graphs) {
        try {
            ObjectOutputStream ois = new ObjectOutputStream(
                    new FileOutputStream(in)
            );
            ois.writeObject(graphs);
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
