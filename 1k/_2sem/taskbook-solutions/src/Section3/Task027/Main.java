package Section3.Task027;

import Section3.MyStack;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyStack<Integer> stack = new MyArrayStack<>();
        System.out.println(stack.isEmpty());
        stack.push(5);
        stack.push(7);
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.pop());
    }
}
