package Section3.Task027;

import Section3.MyStack;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyArrayStack<T> implements MyStack<T> {

    //Capacity equals to 1 specially for tests
    private int capacity = 1;
    private int size = 0;

    private T[] dataArray = (T[]) new Object[capacity];

    @Override
    public void push(T data) {
        dataArray[size++] = data;
        if (size == capacity) {
            System.out.println("Ah, I'm full of it");
            capacity = 2 * capacity;
            T[] newDataArray = (T[]) new Object[capacity];
            System.arraycopy(dataArray, 0, newDataArray, 0, size);
            dataArray = newDataArray;
        }
    }

    @Override
    public T pop() {
        if (isEmpty()) {
            return null;
        }
        size--;
        return dataArray[size];
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return dataArray[size - 1];
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}
