package Section3.Task032B;

import Section3.Elem;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedQueue<T> implements Queue<T> {

    private Elem<T> head;
    private Elem<T> tail;

    @Override
    public boolean add(T t) {
        Elem<T> newElem = new Elem<>();
        newElem.setData(t);
        if (tail == null) {
            head = newElem;
            tail = newElem;
        } else {
            tail.setNext(newElem);
            tail = newElem;
        }
        return true;
    }

    @Override
    public boolean offer(T t) {
        if (t == null) {
            return false;
        }
        Elem<T> newElem = new Elem<>();
        newElem.setData(t);
        if (tail == null) {
            head = newElem;
            tail = newElem;
        } else {
            tail.setNext(newElem);
            tail = newElem;
        }
        return true;
    }

    @Override
    public T remove() {
        try {
            T currentElem = head.getData();
            head = head.getNext();
            return currentElem;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public T poll() {
        if (head == null) {
            return null;
        }
        T currentElem = head.getData();
        head = head.getNext();
        return currentElem;
    }

    @Override
    public T element() {
        try {
            return head.getData();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return head.getData();
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
