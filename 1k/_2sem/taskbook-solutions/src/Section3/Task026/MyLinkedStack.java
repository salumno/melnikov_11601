package Section3.Task026;

import Section3.Elem;
import Section3.MyStack;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedStack<T> implements MyStack<T> {

    private Elem<T> head = null;

    @Override
    public void push(T data) {
        head = new Elem<>(data, head);
    }

    @Override
    public T pop() {
        if (head == null) {
            return null;
        } else {
            T data = head.getData();
            head = head.getNext();
            return data;
        }
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return head.getData();
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
