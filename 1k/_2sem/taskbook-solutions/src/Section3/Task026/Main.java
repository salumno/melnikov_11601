package Section3.Task026;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyLinkedStack<Integer> stack = new MyLinkedStack<>();
        System.out.println(stack.isEmpty());
        stack.push(5);
        stack.push(7);
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.pop());
    }
}
