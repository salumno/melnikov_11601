package Section3;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface MyQueue<T> {
    void offer(T data);
    T peek();
    T poll();
    boolean isEmpty();
}
