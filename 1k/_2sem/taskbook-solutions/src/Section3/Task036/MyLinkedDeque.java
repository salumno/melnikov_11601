package Section3.Task036;

import Section3.Elem;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedDeque<T> implements Deque<T> {

    private Elem<T> head;
    private Elem<T> tail;

    @Override
    public void addFirst(T t) {
        head = new Elem<>(t, head);
        if (tail == null) {
            tail = head;
        }
    }

    @Override
    public void addLast(T t) {
        Elem<T> newElem = new Elem<>();
        newElem.setData(t);
        if (tail == null) {
            head = newElem;
            tail = newElem;
        } else {
            tail.setNext(newElem);
            tail = newElem;
        }
    }

    @Override
    public boolean offerFirst(T t) {
        if (t == null) {
            return false;
        } else {
            head = new Elem<>(t, head);
            if (tail == null) {
                tail = head;
            }
            return true;
        }
    }

    @Override
    public boolean offerLast(T t) {
        if (t == null) {
            return false;
        }
        Elem<T> newElem = new Elem<>();
        newElem.setData(t);
        if (tail == null) {
            head = newElem;
            tail = newElem;
        } else {
            tail.setNext(newElem);
            tail = newElem;
        }
        return true;
    }

    @Override
    public T removeFirst() {
        try {
            T currentElem = head.getData();
            head = head.getNext();
            return currentElem;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public T removeLast() {
        try {
            T data = tail.getData();
            Elem<T> preTail = null;
            for (Elem p = head; p.getNext() != null; p = p.getNext()) {
                preTail = p;
            }
            if (preTail == null) {
                head = null;
                tail = null;
            } else {
                tail = preTail;
            }
            return data;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public T pollFirst() {
        if (head == null) {
            return null;
        }
        T currentElem = head.getData();
        head = head.getNext();
        return currentElem;
    }

    @Override
    public T pollLast() {
        if (head == null) {
            return null;
        }
        T data = tail.getData();
        Elem<T> preTail = null;
        for (Elem p = head; p.getNext() != null; p = p.getNext()) {
            preTail = p;
        }
        if (preTail == null) {
            head = null;
        } else {
            tail = preTail;
        }
        return data;
    }

    @Override
    public T getFirst() {
        try {
            return head.getData();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public T getLast() {
        try {
            return tail.getData();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public T peekFirst() {
        if (isEmpty()) {
            return null;
        }
        return head.getData();
    }

    @Override
    public T peekLast() {
        if (isEmpty()) {
            return null;
        }
        return tail.getData();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean offer(T t) {
        return false;
    }

    @Override
    public T remove() {
        return null;
    }

    @Override
    public T poll() {
        return null;
    }

    @Override
    public T element() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }

    @Override
    public void push(T t) {

    }

    @Override
    public T pop() {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return null;
    }
}
