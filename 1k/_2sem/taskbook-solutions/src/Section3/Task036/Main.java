package Section3.Task036;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyLinkedDeque<Integer> deque = new MyLinkedDeque<>();
        deque.addFirst(5);
        System.out.println(deque.peekFirst());
        deque.addFirst(7);
        System.out.println(deque.peekFirst());
        System.out.println(deque.peekLast());
        System.out.println(deque.pollLast());
        System.out.println("Peek last: " + deque.peekLast());
    }
}
