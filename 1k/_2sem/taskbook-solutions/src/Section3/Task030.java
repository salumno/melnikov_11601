package Section3;

import Section3.Task026.MyLinkedStack;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task030 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String reverseLine = sc.nextLine();
        int result = valueCount(reverseLine);
        System.out.println("Final result is " + result);
    }

    private static int valueCount(String num) {
        String[] elements = num.split(" ");
        MyLinkedStack<Integer> stack = new MyLinkedStack<>();
//        System.out.println(Arrays.toString(elements));
        for (int i = 0; i < elements.length; i++) {
            if (Character.isDigit(elements[i].charAt(0))) {
                int currentNum = Integer.parseInt(elements[i]);
//                System.out.println((i+1) + ") " + currentNum);
                stack.push(currentNum);
            } else {
                char currentOperation = elements[i].charAt(0);
                int num2 = stack.pop();
                int num1 = stack.pop();
                int newElem = operationDefine(currentOperation, num1, num2);
//                System.out.println("Add new elem: " + newElem);
                stack.push(newElem);
            }
        }
        if (!stack.isEmpty()) {
            return stack.pop();
        } else {
            return -1;
        }
    }

    private static boolean isDigit(char character) {
        return character >= '0' && character <= '9';
    }

    private static int operationDefine(char operation, int num1, int num2) {
        if (operation == '+') {
            return num1 + num2;
        } else if (operation == '-') {
            return num1 - num2;
        } else if (operation == '*') {
            return num1 * num2;
        } else if (operation == '/'){
            return num1 / num2;
        }
        return 0;
    }
}
