package Section3.Task032A;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyArrayQueue<Integer> queue = new MyArrayQueue<>();
        System.out.println(queue.isEmpty());
        queue.offer(5);
        queue.offer(7);
        queue.offer(3);
        System.out.println(queue.poll());
        System.out.println(queue.isEmpty());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.isEmpty());
        System.out.println(queue.peek());
    }
}
