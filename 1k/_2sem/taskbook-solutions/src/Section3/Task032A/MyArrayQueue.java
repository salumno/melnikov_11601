package Section3.Task032A;

import Section3.MyQueue;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyArrayQueue<T> implements MyQueue<T> {
    private int capacity = 1000;

    private T[] dataArray = (T[]) new Object[capacity];
    private int tail = 0;
    private int head = 0;

    @Override
    public void offer(T data) {
        dataArray[tail] = data;
        tail++;
        /*if (tail == capacity) {
            System.out.println("Ah, I'm full of it");
            capacity = 2 * capacity;
            T[] newDataArray = (T[]) new Object[capacity];
            System.arraycopy(dataArray, 0, newDataArray, 0, tail);
            dataArray = newDataArray;
        }*/
    }

    @Override
    public T poll() {
        if (isEmpty()) {
            return null;
        }
        T currentElement = dataArray[head];
        head++;
        return currentElement;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return dataArray[head];
    }

    public boolean isEmpty() {
        return tail - head == 0;
    }
}
