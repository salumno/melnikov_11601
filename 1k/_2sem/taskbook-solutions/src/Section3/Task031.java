package Section3;

import Section3.Task026.MyLinkedStack;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task031 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String reverseLine = toReversePolishNotation(sc.nextLine());
        System.out.println(reverseLine);
    }

    private static String toReversePolishNotation(String num) {
        StringBuilder result = new StringBuilder();
        MyLinkedStack<Character> stack = new MyLinkedStack<>();
        for (int i = 0; i < num.length(); i++) {
            char currentElem = num.charAt(i);
            if (currentElem == ' ') {
                continue;
            }
            if (Character.isDigit(currentElem)) {
                result.append(currentElem);
                if (!(i + 1 < num.length() && Character.isDigit(num.charAt(i + 1)))) {
                    result.append(" ");
                }
            } else if (currentElem == '(' || stack.isEmpty() || operationPriority(currentElem) > operationPriority(stack.peek())) {
                stack.push(currentElem);
            } else if (currentElem == ')'){
                char upperElem = stack.peek();
                while (upperElem != '(') {
                    upperElem = stack.pop();
                    result.append(upperElem).append(" ");
                    upperElem = stack.peek();
                }
                stack.pop();
            } else if (operationPriority(currentElem) <= operationPriority(stack.peek())){
                result.append(stack.pop()).append(" ");
                stack.push(currentElem);
            }
        }
        while (!stack.isEmpty()) {
            result.append(stack.pop()).append(" ");
        }
        return result.toString();
    }

    private static int operationPriority(char op) {
        switch (op) {
            case '*':
                return 4;
            case '/':
                return 4;
            case '-':
                return 3;
            case '+':
                return 3;
            case '(':
                return 2;
        }
        return -1;
    }
}
