package Section3.Task020A;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyDoubleStackQueue<Integer> queue = new MyDoubleStackQueue<>();
        System.out.println(queue.isEmpty());
        queue.offer(5);
        queue.offer(7);
        System.out.println(queue.poll());
        queue.offer(3);
        System.out.println(queue.poll());
        System.out.println(queue.isEmpty());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.isEmpty());
        System.out.println(queue.peek());
    }
}
