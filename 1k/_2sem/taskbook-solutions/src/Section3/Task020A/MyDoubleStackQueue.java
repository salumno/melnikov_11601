package Section3.Task020A;

import Section3.MyQueue;
import Section3.Task026.MyLinkedStack;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyDoubleStackQueue<T> implements MyQueue<T> {
    private MyLinkedStack<T> in;
    private MyLinkedStack<T> out;

    public MyDoubleStackQueue() {
        in = new MyLinkedStack<>();
        out = new MyLinkedStack<>();
    }

    @Override
    public void offer(T data) {
        in.push(data);
    }

    @Override
    public T poll() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.pop();
    }

    @Override
    public boolean isEmpty() {
        return out.isEmpty() && in.isEmpty();
    }

    @Override
    public T peek() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.peek();
    }
}
