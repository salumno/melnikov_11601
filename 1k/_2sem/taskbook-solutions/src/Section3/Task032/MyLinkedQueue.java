package Section3.Task032;

import Section3.Elem;
import Section3.MyQueue;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedQueue<T> implements MyQueue<T> {

    private Elem<T> head;
    private Elem<T> tail;

    @Override
    public void offer(T data) {
        Elem<T> newElem = new Elem<>();
        newElem.setData(data);
        if (tail == null) {
            head = newElem;
            tail = newElem;
        } else {
            tail.setNext(newElem);
            tail = newElem;
        }
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return head.getData();
    }

    @Override
    public T poll() {
        if (head == null) {
            return null;
        }
        T currentElem = head.getData();
        head = head.getNext();
        return currentElem;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
