package Section3;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Elem<T> {
    private T data;
    private Elem<T> next;

    public Elem(T data, Elem next) {
        this.data = data;
        this.next = next;
    }

    public Elem() {
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem<T> next) {
        this.next = next;
    }
}
