package Section3;

import Section3.Task026.MyLinkedStack;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task028 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        MyLinkedStack<Integer> stack = new MyLinkedStack<>();
        System.out.println("Количество элементов в массиве: ");
        int size = sc.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            int currentNumber = sc.nextInt();
            array[i] = currentNumber;
            stack.push(currentNumber);
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = stack.pop();
        }
        if (stack.isEmpty()) {
            System.out.println(Arrays.toString(array));

        }
    }
}
