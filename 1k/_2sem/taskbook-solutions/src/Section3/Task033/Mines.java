package Section3.Task033;

import Section3.Task032A.MyArrayQueue;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Mines {

    private static class FieldSell {
        private int x;
        private int y;
        private char data;
        private boolean isCheck;

        public FieldSell(int x, int y, char data) {
            this.data = data;
            this.x = x;
            this.y = y;
            isCheck = false;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public boolean isSawCheck() {
            return isCheck;
        }

        public void setSawCheck(boolean sawCheck) {
            this.isCheck = sawCheck;
        }

        public char getData() {
            return data;
        }

        public void setData(char data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        FieldSell[][] field = fieldInitial();
        printField(field);
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        handleFieldSell(x, y, field);
        System.out.println();
        printField(field);
    }

    private static void handleFieldSell(int x, int y, FieldSell[][] field) {
        MyArrayQueue<FieldSell> queue = new MyArrayQueue<>();
        sellSawCheckSwitch(field[x][y], queue);
        while (!queue.isEmpty()) {
            FieldSell currentElement = queue.poll();
            if (currentElement.getData() == '0') {
                addNeighbours(currentElement.getX(), currentElement.getY(), queue, field);
                currentElement.setData('1');
            }
        }

    }

    private static void addNeighbours(int x, int y, MyArrayQueue<FieldSell> queue, FieldSell[][] field) {
        for (int i = -1; i < 2; i++) {
            int currentX = x + i;
            for (int j = -1; j < 2; j++) {
                int currentY = y + j;
                if (currentX == x & currentY == y) {
                    continue;
                }
                if (currentX >= 0 && currentX < field.length && currentY >= 0 && currentY < field.length) {
                    sellSawCheckSwitch(field[currentX][currentY], queue);
                }
            }
        }
    }

    private static void sellSawCheckSwitch(FieldSell currentElement, MyArrayQueue<FieldSell> queue) {
        if (!currentElement.isSawCheck()) {
            if (currentElement.getData() == '0') {
                queue.offer(currentElement);
            }
            currentElement.setSawCheck(true);
        }
    }

    private static void printField(FieldSell[][] field) {
        for (FieldSell[] currentField : field) {
            for (FieldSell aCurrentField : currentField) {
                System.out.print(aCurrentField.getData() + " ");
            }
            System.out.println();
        }
    }

    private static FieldSell[][] fieldInitial() {
        Random r = new Random();
        FieldSell[][] field = new FieldSell[5][5];
        char[] elements = {'0', '1'};
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                int currentElem = r.nextInt(2);
                field[i][j] = new FieldSell(i, j, elements[currentElem]);
            }
        }
        return field;
    }
}
