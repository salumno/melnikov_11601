package Section3;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task029A {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        boolean inputSubmit = true;
        int checker = 0;
        for (int i = 0; i < input.length() && inputSubmit; i++) {
            if (input.charAt(i) == '(') {
                checker++;
            } else if (input.charAt(i) == ')') {
                if (checker - 1 < 0) {
                    inputSubmit = false;
                } else {
                    checker--;
                }
            }
        }
        if (checker > 0) {
            inputSubmit = false;
        }
        System.out.println(inputSubmit);
    }
}
