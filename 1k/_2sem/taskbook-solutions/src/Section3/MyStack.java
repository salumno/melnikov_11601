package Section3;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface MyStack<T> {
    void push(T data);
    T pop();
    T peek();
    boolean isEmpty();
}
