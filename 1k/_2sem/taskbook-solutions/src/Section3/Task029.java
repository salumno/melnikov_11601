package Section3;

import Section3.Task026.MyLinkedStack;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task029 {

    public static void main(String[] args) {
        String error1 = "Не все открывающие закрыты";
        String error2 = "Встретилась лишняя закрывающая";
        String error3 = "Скобки не соответствуют друг другу";
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        MyLinkedStack<Character> stack = new MyLinkedStack<>();
        boolean check = true;
        for (int i = 0; i < line.length() && check; i++) {
            char symbol = line.charAt(i);
            if (symbol == '(' || symbol == '[' || symbol == '{') {
                stack.push(symbol);
            } else if (symbol == ')') {
                if (stack.isEmpty()) {
                    check = false;
                    System.out.println("At position: " + i);
                    System.out.println(error2);
                    continue;
                }
                if (stack.peek() == '(') {
                    stack.pop();
                } else {
                    check = false;
                    System.out.println("At position: " + i);
                    System.out.println(error3);
                }
            } else if (symbol == '}') {
                if (stack.isEmpty()) {
                    check = false;
                    System.out.println("At position: " + i);
                    System.out.println(error2);
                    continue;
                }
                if (stack.peek() == '{') {
                    stack.pop();
                } else {
                    check = false;
                    System.out.println("At position: " + i);
                    System.out.println(error3);
                }
            } else if (symbol == ']') {
                if (stack.isEmpty()) {
                    check = false;
                    System.out.println("At position: " + i);
                    System.out.println(error2);
                    continue;
                }
                if (stack.peek() == '[') {
                    stack.pop();
                } else {
                    check = false;
                    System.out.println("At position: " + i);
                    System.out.println(error3);
                }
            }
        }
        if (!stack.isEmpty()) {
            check = false;
            System.out.println(error1);
        }
        System.out.println(check);
    }
}
