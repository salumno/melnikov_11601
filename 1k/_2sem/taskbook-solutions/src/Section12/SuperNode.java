package Section12;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class SuperNode implements Serializable{
    private int id;
    private int color = -1;
    private List<SuperNode> jointNode = new LinkedList<>();
    private List<Integer> jointNodeId = new LinkedList<>();

    public SuperNode(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Integer> getJointNodeId() {
        return jointNodeId;
    }

    public void setJointNodeId(List<Integer> jointNodeId) {
        this.jointNodeId = jointNodeId;
    }

    public List<SuperNode> getJointNode() {
        return jointNode;
    }

    public void setJointNode(List<SuperNode> jointNode) {
        this.jointNode = jointNode;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        String result = "SuperNode{" +
                "id=" + id + ", color: " + color +", nodes: ";
        for (Integer node: jointNodeId) {
            result += node + ", ";
        }
        return result + "}";
    }
}
