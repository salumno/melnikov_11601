package Section12;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class Graph implements Serializable{
    private int countOfNodes;
    private int countOfEdges;

    private List<Edge> edgeList;
    private List<SuperNode> adjacencyList;

    public Graph(int countOfNodes, int countOfEdges) {
        this.countOfNodes = countOfNodes;
        this.countOfEdges = countOfEdges;
        edgeList = new ArrayList<>();
        adjacencyList = new ArrayList<>(countOfNodes);
        for (int i = 0; i < countOfNodes; i++) {
            adjacencyList.add(new SuperNode(i));
        }
    }

    public boolean createGraphByConsole() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write edges of your graph");
        System.out.println("Schema: <node1><space><node2>");
        for (int i = 0; i < countOfEdges; i++) {
            String currentLine = sc.nextLine();
            if (checkInput(currentLine)) {
                createGraph(currentLine);
            } else {
                return false;
            }
        }
        return true;
    }

    public boolean createGraphByFile(String filepath) {
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(filepath)
                    )
            );
            for (int i = 0; i < countOfEdges; i++) {
                String currentLine = br.readLine();
                if (checkInput(currentLine)) {
                    createGraph(currentLine);
                } else {
                    return false;
                }
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean checkInput(String input) {
        Pattern p = Pattern.compile("[0-9]+(\\s)[0-9]+");
        Matcher m = p.matcher(input);
        return m.matches();
    }

    private void createGraph(String rawEdge) {
        String[] rawData = rawEdge.split(" ");
        int firstNode = Integer.parseInt(rawData[0]);
        int secondNode = Integer.parseInt(rawData[1]);
        edgeList.add(new Edge(firstNode, secondNode));
        adjacencyList.get(firstNode).getJointNode().add(adjacencyList.get(secondNode));
        adjacencyList.get(secondNode).getJointNode().add(adjacencyList.get(firstNode));
        adjacencyList.get(firstNode).getJointNodeId().add(secondNode);
        adjacencyList.get(secondNode).getJointNodeId().add(firstNode);
    }

    public static int[][] parseEdgeListToAdjacencyMatrix(List<Edge> edgeList, int countOfNodes) {
        int[][] adjacencyMatrix = new int[countOfNodes][countOfNodes];
        for (Edge edge: edgeList) {
            int first = edge.getFirstNodeId();
            int second = edge.getSecondNodeId();
            adjacencyMatrix[first][second] = 1;
            adjacencyMatrix[second][first] = 1;
        }
        return adjacencyMatrix;
    }

    public static int[][] parseEdgeListToIncidenceMatrix(List<Edge> edgeList, int countOfNodes) {
        int countOfEdges = edgeList.size();
        int[][] incidenceMatrix = new int[countOfNodes][countOfEdges];
        for (int i = 0; i < countOfEdges; i++) {
            Edge edge = edgeList.get(i);
            incidenceMatrix[edge.getFirstNodeId()][i] = 1;
            incidenceMatrix[edge.getSecondNodeId()][i] = 1;
        }
        return incidenceMatrix;
    }

    public static List<SuperNode> parseEdgeListToAdjacencyList(List<Edge> edgeList, int countOfNodes) {
        List<SuperNode> adjacencyList = new ArrayList<>(countOfNodes);
        for (int i = 0; i < countOfNodes; i++) {
            adjacencyList.add(new SuperNode(i));
        }
        for (Edge currentEdge : edgeList) {
            int firstNode = currentEdge.getFirstNodeId();
            int secondNode = currentEdge.getSecondNodeId();
            adjacencyList.get(firstNode).getJointNode().add(adjacencyList.get(secondNode));
            adjacencyList.get(secondNode).getJointNode().add(adjacencyList.get(firstNode));
            adjacencyList.get(firstNode).getJointNodeId().add(secondNode);
            adjacencyList.get(secondNode).getJointNodeId().add(firstNode);
        }
        return adjacencyList;
    }

    public static List<Edge> parseAdjacencyMatrixToEdgeList(int[][] adjacencyMatrix) {
        List<Edge> edgeList = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] == 1) {
                    Edge newEdge = new Edge(i, j);
                    if (!edgeList.contains(newEdge)) {
                        edgeList.add(newEdge);
                    }
                }
            }
        }
        return edgeList;
    }

    public static int[][] parseAdjacencyMatrixToIncidenceMatrix(int[][] adjacencyMatrix, int countOfEdges) {
        List<Edge> edgeList = Graph.parseAdjacencyMatrixToEdgeList(adjacencyMatrix);
        return Graph.parseEdgeListToIncidenceMatrix(edgeList, adjacencyMatrix.length);
    }

    public static List<SuperNode> parseAdjacencyMatrixToAdjacencyList(int[][] adjacencyMatrix) {
        int countOfNodes = adjacencyMatrix.length;
        List<SuperNode> adjacencyList = new ArrayList<>(countOfNodes);
        for (int i = 0; i < countOfNodes; i++) {
            adjacencyList.add(new SuperNode(i));
        }
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] == 1) {
                    adjacencyList.get(i).getJointNode().add(adjacencyList.get(j));
                    adjacencyList.get(i).getJointNodeId().add(j);
                }
            }
        }
        return adjacencyList;
    }

    public static List<Edge> parseIncidenceMatrixToEdgeList(int[][] incidenceMatrix) {
        List<Edge> edgeList = new ArrayList<>();
        int countOfNodes = incidenceMatrix.length;
        int countOfEdges = incidenceMatrix[0].length;
        int[] index = new int[2];
        int currentIndexPos = 0;
        for (int i = 0; i < countOfEdges; i++) {
            for (int j = 0; j < countOfNodes; j++) {
                if (incidenceMatrix[j][i] == 1) {
                    index[currentIndexPos] = j;
                    currentIndexPos++;
                }
            }
            currentIndexPos = 0;
            edgeList.add(new Edge(index[0], index[1]));
        }
        return edgeList;
    }

    public static int[][] parseIncidenceMatrixToAdjacencyMatrix(int[][] incidenceMatrix) {
        List<Edge> edgeList = Graph.parseIncidenceMatrixToEdgeList(incidenceMatrix);
        return Graph.parseEdgeListToAdjacencyMatrix(edgeList, incidenceMatrix.length);
    }

    public static List<SuperNode> parseIncidenceMatrixToAdjacencyList(int[][] incidenceMatrix) {
        int countOfNodes = incidenceMatrix.length;
        int countOfEdges = incidenceMatrix[0].length;
        List<SuperNode> adjacencyList = new ArrayList<>(countOfNodes);
        for (int i = 0; i < countOfNodes; i++) {
            adjacencyList.add(new SuperNode(i));
        }
        int[] index = new int[2];
        int currentIndexPos = 0;
        for (int i = 0; i < countOfEdges; i++) {
            for (int j = 0; j < countOfNodes; j++) {
                if (incidenceMatrix[j][i] == 1) {
                    index[currentIndexPos] = j;
                    currentIndexPos++;
                }
            }
            currentIndexPos = 0;
            int firstNode = index[0];
            int secondNode = index[1];
            adjacencyList.get(firstNode).getJointNode().add(adjacencyList.get(secondNode));
            adjacencyList.get(secondNode).getJointNode().add(adjacencyList.get(firstNode));
            adjacencyList.get(firstNode).getJointNodeId().add(secondNode);
            adjacencyList.get(secondNode).getJointNodeId().add(firstNode);
        }
        return adjacencyList;
    }

    public static int[][] parseAdjacencyListToAdjacencyMatrix(List<SuperNode> adjacencyList) {
        List<Edge> edgeList = Graph.parseAdjacencyListToEdgeList(adjacencyList);
        return Graph.parseEdgeListToAdjacencyMatrix(edgeList, adjacencyList.size());
    }

    public static int[][] parseAdjacencyListToIncidenceMatrix(List<SuperNode> adjacencyList) {
        List<Edge> edgeList = Graph.parseAdjacencyListToEdgeList(adjacencyList);
        return Graph.parseEdgeListToIncidenceMatrix(edgeList, adjacencyList.size());
    }

    public static List<Edge> parseAdjacencyListToEdgeList(List<SuperNode> adjacencyList) {
        List<Edge> edgeList = new ArrayList<>();
        for (int i = 0; i < adjacencyList.size(); i++) {
            List<Integer> nodes = adjacencyList.get(i).getJointNodeId();
            for (Integer node : nodes) {
                Edge edge = new Edge(i, node);
                if (!edgeList.contains(edge)) {
                    edgeList.add(edge);
                }
            }
        }
        return edgeList;
    }

    public List<Edge> getEdgesList() {
        return edgeList;
    }

    public int[][] getAdjacencyMatrix() {
        int[][] adjacencyMatrix = new int[countOfNodes][countOfNodes];
        for (Edge edge: edgeList) {
            int first = edge.getFirstNodeId();
            int second = edge.getSecondNodeId();
            adjacencyMatrix[first][second] = 1;
            adjacencyMatrix[second][first] = 1;
        }
        return adjacencyMatrix;
    }

    public int[][] getIncidenceMatrix() {
        int[][] incidenceMatrix = new int[countOfNodes][countOfEdges];
        for (int i = 0; i < countOfEdges; i++) {
            Edge edge = getEdgesList().get(i);
            incidenceMatrix[edge.getFirstNodeId()][i] = 1;
            incidenceMatrix[edge.getSecondNodeId()][i] = 1;
        }
        return incidenceMatrix;
    }

    public List<SuperNode> getAdjacencyList() {
        return adjacencyList;
    }

    public int getCountOfNodes() {
        return countOfNodes;
    }

    public int getCountOfEdges() {
        return countOfEdges;
    }

    @Override
    public String toString() {
        return "Graph{" +
                "countOfNodes=" + countOfNodes +
                ", countOfEdges=" + countOfEdges +
                '}';
    }
}
