package Section12;

import java.io.Serializable;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Edge implements Serializable{
    private int firstNodeId;
    private int secondNodeId;

    public Edge(int firstNodeId, int secondNodeId) {
        this.firstNodeId = firstNodeId;
        this.secondNodeId = secondNodeId;
    }

    public int getFirstNodeId() {
        return firstNodeId;
    }

    public void setFirstNodeId(int firstNodeId) {
        this.firstNodeId = firstNodeId;
    }

    public int getSecondNodeId() {
        return secondNodeId;
    }

    public void setSecondNodeId(int secondNodeId) {
        this.secondNodeId = secondNodeId;
    }

    @Override
    public String toString() {
        return firstNodeId + " - " + secondNodeId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Edge) {
            Edge object = (Edge) obj;
            boolean check1 = getFirstNodeId() == object.getFirstNodeId() && getSecondNodeId() == object.getSecondNodeId();
            boolean check2 = getFirstNodeId() == object.getSecondNodeId() && getSecondNodeId() == object.getFirstNodeId();
            return check1 || check2;
        } else {
            return false;
        }
    }
}
