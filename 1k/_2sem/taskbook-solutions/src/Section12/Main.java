package Section12;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

/*
0 1
0 4
1 4
1 2
2 3
3 4
3 5
 */

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph(6, 7);
        graph.createGraphByConsole();
        System.out.println("Edge List");
        System.out.println(graph.getEdgesList());
        int[][] adjacencyMatrix = graph.getAdjacencyMatrix();
        int[][] incidenceMatrix = graph.getIncidenceMatrix();
        System.out.println("-----//-----");
        System.out.println("Adjacency Matrix");
        printArray(adjacencyMatrix);
        System.out.println("-----//-----");
        System.out.println("Incidence Matrix");
        printArray(incidenceMatrix);
        System.out.println("-----//-----");
        System.out.println("Adjacency List");
        List<SuperNode> adjacencyList = graph.getAdjacencyList();
        System.out.println(adjacencyList);
        /*System.out.println("-----------////---------------");
        int[][] adjacencyMatrix1 = Graph.parseEdgeListToAdjacencyMatrix(graph.getEdgesList(), graph.getCountOfNodes());
        printArray(adjacencyMatrix1);
        int[][] incidenceMatrix1 = Graph.parseEdgeListToIncidenceMatrix(graph.getEdgesList(), graph.getCountOfNodes());
        printArray(incidenceMatrix1);
        System.out.println(Graph.parseEdgeListToAdjacencyList(graph.getEdgesList(), graph.getCountOfNodes()));*/
        /*System.out.println("-----------////---------------");
        printArray(Graph.parseAdjacencyMatrixToIncidenceMatrix(adjacencyMatrix, graph.getCountOfEdges()));
        System.out.println(Graph.parseAdjacencyMatrixToEdgeList(adjacencyMatrix));
        System.out.println(Graph.parseAdjacencyMatrixToAdjacencyList(adjacencyMatrix));*/
        /*System.out.println("-----------////---------------");
        printArray(Graph.parseIncidenceMatrixToAdjacencyMatrix(incidenceMatrix));
        System.out.println(Graph.parseIncidenceMatrixToEdgeList(incidenceMatrix));
        System.out.println(Graph.parseIncidenceMatrixToAdjacencyList(incidenceMatrix));*/
        System.out.println("-----------////---------------");
        printArray(Graph.parseAdjacencyListToAdjacencyMatrix(adjacencyList));
        printArray(Graph.parseAdjacencyListToIncidenceMatrix(adjacencyList));
        System.out.println((Graph.parseAdjacencyListToEdgeList(adjacencyList)));
    }

    private static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
