package Section1;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 *
 * Надо было проще? Надо
 */
public class Task001<T> {

    private class ListNode<T> {
        private ListNode next;
        private T data;

        public ListNode() {}

        public ListNode(T data, ListNode next) {
            this.data = data;
            this.next = next;
        }

        public ListNode getNext() {
            return next;
        }

        public void setNext(ListNode next) {
            this.next = next;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }

    private ListNode<T> head;
    private ListNode<T> tail;

    public void addBack(T data) {
        ListNode<T> newElem = new ListNode<>(data, null);
        if (head == null) {
            head = newElem;
            tail = newElem;
        } else {
            tail.setNext(newElem);
            tail = newElem;
        }
    }

    public void printList() {
        if (head == null) {
            return;
        }
        ListNode<T> currentNode = head;
        while (currentNode != null) {
            System.out.println(currentNode.getData());
            currentNode = currentNode.getNext();
        }
    }

    public static void main(String[] args) {
        Task001<Integer> myList = new Task001<>();
        myList.addBack(5);
        myList.addBack(7);
        myList.addBack(7);
        myList.printList();
    }
}
