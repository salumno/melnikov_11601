package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task011 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        ListNode<Integer> head = writeList(sc.nextInt());
        System.out.println("Before selection sort: ");
        printList(head);
        System.out.println("After selection sort: ");
        listSelectionSort(head);
        printList(head);
    }

    private static void listSelectionSort(ListNode<Integer> head) {
        for (ListNode p = head; p != null; p = p.getNext()) {
            ListNode<Integer> leftLimit = p;
            ListNode<Integer> nodeWithMinElem = findMin(p);
            int elem1 = leftLimit.getData();
            int elem2 = nodeWithMinElem.getData();
            nodeWithMinElem.setData(elem1);
            leftLimit.setData(elem2);
        }
    }

    private static ListNode<Integer> findMin(ListNode localHead) {
        ListNode<Integer> min = localHead;
        for (ListNode<Integer> p = localHead; p != null; p = p.getNext()) {
            if (p.getData() < min.getData()) {
                min = p;
            }
        }
        return min;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
