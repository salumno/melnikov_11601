package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task012 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        ListNode<Integer> head = writeList(sc.nextInt());
        System.out.println("Before selection shift: ");
        printList(head);
        System.out.println("Count of positions: ");
        int k = sc.nextInt();
        System.out.println("After selection shift: ");
        shiftByPos(head, k);
        printList(head);
    }

    private static void shiftByPos(ListNode<Integer> head, int k) {
        for (int i = 0; i < k; i++) {
            aloneShift(head);
        }
    }

    private static void aloneShift(ListNode<Integer> head) {
        int nextElemData = head.getData();
        for (ListNode<Integer> p = head; p.getNext() != null; p = p.getNext()) {
            int temp = (Integer) p.getNext().getData();
            p.getNext().setData(nextElemData);
            nextElemData = temp;
        }
        head.setData(nextElemData);
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
