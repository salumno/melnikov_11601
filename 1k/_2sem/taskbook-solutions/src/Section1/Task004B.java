package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task004B {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }

        int prod = 1;
        boolean posCheck = true;
        p = head;
        while (p != null) {
            if (posCheck) {
                prod *= p.getData();
            }
            posCheck = !posCheck;
            p = p.getNext();
        }

        System.out.println(prod);
    }
}
