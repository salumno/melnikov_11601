package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task010 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        ListNode<Integer> head = writeList(sc.nextInt());
        System.out.println("Before bubble sort: ");
        printList(head);
        System.out.println("After bubble sort: ");
        listBubbleSort(head);
        printList(head);
    }

    private static void listBubbleSort(ListNode<Integer> head) {
        for (ListNode p = head; p.getNext() != null; p = p.getNext()) {
            for (ListNode h = head; h.getNext() != null; h = h.getNext()) {
                int elem1 = (Integer) h.getData();
                int elem2 = (Integer) h.getNext().getData();
                if (elem1 > elem2) {
                    h.getNext().setData(elem1);
                    h.setData(elem2);
                }
            }
        }
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
