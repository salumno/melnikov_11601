package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task008 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }

        int minCount = 0;
        int maxCount = 0;
        int prev = head.getData();
        int next;
        p = head.getNext();
        while (p != null) {
            if (p.getNext() != null) {
                next = (int)p.getNext().getData();
                if (p.getData() < next && p.getData() < prev) {
                    minCount++;
                } else if (p.getData() > next && p.getData() > prev) {
                    maxCount++;
                }
                prev = p.getData();
            }
            p = p.getNext();
        }
        //С учетом того, что граничные элементы не рассматривались в качестве локальных минимумов или максимумов
        System.out.println("local min count " + minCount);
        System.out.println("local max count " + maxCount);
    }
}
