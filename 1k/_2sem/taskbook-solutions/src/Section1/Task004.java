package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task004 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = sc.nextInt();

        int evenSum = 0;
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            if (p.getData() % 2 == 0) {
                evenSum += p.getData();
            }
            head = p;
        }
        System.out.println(evenSum);
    }
}
