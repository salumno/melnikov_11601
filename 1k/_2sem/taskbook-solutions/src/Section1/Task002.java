package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task002 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = 5;
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }

        int max = head.getData();
        p = head;
        while (p != null) {
            max = (p.getData() > max) ? p.getData() : max;
            p = p.getNext();
        }
        System.out.println(max);
    }
}
