package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task009 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = writeList(sc.nextInt());
        System.out.println("Before: ");
        printList(head);
        System.out.println("After: ");
        ListNode<Integer> headOfBackwardList = backwardsList(head);
        printList(headOfBackwardList);
    }

    private static ListNode<Integer> backwardsList(ListNode<Integer> head) {
        ListNode<Integer> current = head;
        ListNode<Integer> next;
        ListNode<Integer> prev = null;
        do {
            next = current.getNext();
            current.setNext(prev);
            prev = current;
            current = next;
        } while (current != null);
        head = prev;
        return head;
    }

    private static ListNode<Integer> writeList(int n) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
