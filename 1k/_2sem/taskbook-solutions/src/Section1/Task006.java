package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task006 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }

        //Print list
        p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();

        p = head;
        while (p.getNext() != null) {
            p = p.getNext();
        }

        int temp = p.getData();
        p.setData(head.getData());
        head.setData(temp);

        p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
