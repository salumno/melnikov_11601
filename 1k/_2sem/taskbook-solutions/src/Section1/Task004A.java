package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task004A {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }

        boolean zeroCheck = false;
        p = head;
        while (!zeroCheck && p != null) {
            if (p.getData() == 0) {
                zeroCheck = true;
            }
            p = p.getNext();
        }
        System.out.println(zeroCheck);
    }
}
