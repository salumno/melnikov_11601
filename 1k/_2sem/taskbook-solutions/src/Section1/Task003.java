package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task003 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head = null;
        ListNode<Integer> p;

        int n = 5;
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }

        p = head;
        int sum = 0;
        int pr = 1;
        while (p != null) {
            sum += p.getData();
            pr *= p.getData();
            p = p.getNext();
        }

        System.out.println("sum = " + sum);
        System.out.println("p = " + pr);
    }
}
