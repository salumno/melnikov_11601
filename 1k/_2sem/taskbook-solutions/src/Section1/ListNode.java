package Section1;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class ListNode<T> {

    private ListNode next;
    private T data;

    public ListNode() {}

    public ListNode(ListNode next, T data) {
        this.next = next;
        this.data = data;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Section1.ListNode{" +
                "data=" + data +
                '}';
    }
}
