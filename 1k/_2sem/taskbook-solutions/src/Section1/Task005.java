package Section1;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task005 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode<Integer> head1 = null;
        ListNode<Integer> tail1 = null;

        ListNode<Integer> head2 = null;
        ListNode<Integer> tail2 = null;

        ListNode<Integer> p;

        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(null, sc.nextInt());
            if (head1 == null) {
                head1 = p;
                tail1 = p;
            } else {
                tail1.setNext(p);
                tail1 = p;
            }
        }

        n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            p = new ListNode<>(null, sc.nextInt());
            if (head2 == null) {
                head2 = p;
                tail2 = p;
            } else {
                tail2.setNext(p);
                tail2 = p;
            }
        }

        tail1.setNext(head2);

        p = head1;
        while (p != null) {
            System.out.println(p.getData());
            p = p.getNext();
        }
    }
}
