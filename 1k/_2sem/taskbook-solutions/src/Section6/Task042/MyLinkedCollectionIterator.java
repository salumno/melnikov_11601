package Section6.Task042;

import Section3.Elem;

import java.util.Iterator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedCollectionIterator<T> implements Iterator<T> {
    private Elem<T> currentElement;

    public MyLinkedCollectionIterator(Elem<T> head) {
        System.out.println("I'm here");
        currentElement = head;
    }

    @Override
    public boolean hasNext() {
        return !(currentElement == null);
    }

    @Override
    public T next() {
        T next = currentElement.getData();
        currentElement = currentElement.getNext();
        return next;
    }
}
