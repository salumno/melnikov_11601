package Section6.Task042;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyLinkedCollection<Integer> collection = new MyLinkedCollection<>();
        collection.add(5);
        collection.add(7);
        collection.add(8);
        System.out.println(collection);
        List<Integer> list = Arrays.asList(5);
        System.out.println(list);
        collection.retainAll(list);
        System.out.println(collection);
    }
}
