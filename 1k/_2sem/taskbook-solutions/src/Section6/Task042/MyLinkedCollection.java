package Section6.Task042;

import Section3.Elem;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedCollection<T> implements Collection<T> {
    protected Elem<T> head;
    protected Elem<T> tail;
    protected int size = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (Elem p = head; p != null; p = p.getNext()) {
            if (o.equals(p.getData())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedCollectionIterator<>(head);
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        Elem<T> newElement = new Elem<>(t, null);
        if (head == null) {
            head = newElement;
            tail = newElement;
        } else {
            tail.setNext(newElement);
            tail = newElement;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            return false;
        }
        Elem<T> prev = null;
        for (Elem<T> p = head; p != null; p = p.getNext()) {
            T currentData = p.getData();
            if (currentData.equals(o)) {
                size--;
                if (prev == null) {
                    head = p.getNext();
                } else {
                    prev.setNext(p.getNext());
                    if (p == tail) {
                        tail = prev;
                    }
                }
            } else {
                prev = p;
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean check = true;
        for (Object object: c) {
            check = check && contains(object);
        }
        return check;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean check = true;
        for (T data: c) {
            check = check && add(data);
        }
        return check;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean check = true;
        for (Object object: c) {
            check = check && remove(object);
        }
        return check;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Elem<T> p = head;
        while (p != null) {
            if (!c.contains(p.getData())) {
                Elem<T> next = p.getNext();
                remove(p.getData());
                p = next;
            } else {
                p = p.getNext();
            }
        }
        return true;
    }

    @Override
    public void clear() {
        size = 0;
        head = null;
    }

    @Override
    public String toString() {
        String result = "Collection:";
        for (Elem p = head; p != null; p = p.getNext()) {
            result += " " + p.getData();
        }
        return result;
    }
}
