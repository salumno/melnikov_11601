package Section6.Task038;

import Section3.Task026.MyLinkedStack;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedQueue<T> implements Queue<T>{

    private MyLinkedStack<T> in = new MyLinkedStack<>();
    private MyLinkedStack<T> out = new MyLinkedStack<>();

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return out.isEmpty() && in.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (t == null) {
            return false;
        }
        in.push(t);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        in.push(t);
        return true;
    }

    @Override
    public T remove() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        if (out.pop() != null) {
            return out.pop();
        } else {
            throw new NullPointerException();
        }
    }

    @Override
    public T poll() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.pop();
    }

    @Override
    public T element() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        if (out.peek() != null) {
            return out.peek();
        } else {
            throw new NullPointerException();
        }
    }

    @Override
    public T peek() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
        return out.peek();
    }
}
