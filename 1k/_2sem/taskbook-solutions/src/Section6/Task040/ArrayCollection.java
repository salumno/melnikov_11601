package Section6.Task040;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 *
 */
public class ArrayCollection<T> implements Collection<T> {
    protected int size = 0;
    protected int capacity;
    protected T[] array;

    public ArrayCollection() {
        capacity = 1000;
        array = (T[]) new Object[capacity];
    }

    public ArrayCollection(int capacity) {
        this.capacity = capacity;
        array = (T[]) new Object[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        array[size] = t;
        size++;
        if (size == capacity) {
            capacity = 2 * capacity;
            T[] newDataArray = (T[]) new Object[capacity];
            System.arraycopy(array, 0, newDataArray, 0, size);
            array = newDataArray;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int objectIndex = -1;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(o)) {
                objectIndex = i;
            }
        }
        if (objectIndex == -1) {
            return false;
        } else {
            for (int i = objectIndex; i < size - 1; i++) {
                array[i] = array[i + 1];
            }
            size--;
            return true;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean check = true;
        for (Object object: c) {
            check = check && contains(object);
        }
        return check;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean check = true;
        for (T object: c) {
            check = check && add(object);
        }
        return check;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean check = true;
        for (Object object: c) {
            check = check && remove(object);
        }
        return check;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean check = true;
        for (int i = 0; i < size; i++) {
            if (!c.contains(array[i])) {
                for (int j = i; i < size - 1; j++) {
                    array[j] = array[j + 1];
                }
                size--;
            } else {
                check = false;
            }
        }
        return check;
    }

    @Override
    public void clear() {
        size = 0;
    }
}
