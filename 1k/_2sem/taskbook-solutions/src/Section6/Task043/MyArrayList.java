package Section6.Task043;

import Section6.Task040.ArrayCollection;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyArrayList<T> extends ArrayCollection<T> implements List<T> {


    public MyArrayList() {
        super();
    }

    public MyArrayList(int capacity) {
        super(capacity);
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (c.size() == 0) {
            return false;
        }
        for (T element: c) {
            add(index, element);
            index++;
        }
        return true;
    }

    @Override
    public T get(int index) {
        return array[index];
    }

    @Override
    public T set(int index, T element) {
        T previousData = array[index];
        array[index] = element;
        return previousData;
    }

    @Override
    public void add(int index, T element) {
        for (int i = index; i < size; i++) {
            if (i + 1 == capacity) {
                capacity = 2 * capacity;
                T[] newDataArray = (T[]) new Object[capacity];
                System.arraycopy(array, 0, newDataArray, 0, size);
                array = newDataArray;
            }
            array[i + 1] = array[i];
        }
        array[index] = element;
        size++;
    }

    @Override
    public T remove(int index) {
        T removedData = array[index];
        for (int j = index; j < size - 1; j++) {
            array[j] = array[j + 1];
        }
        size--;
        return removedData;
    }

    @Override
    public int indexOf(Object o) {
        if (o != null) {
            for (int i = 0; i < size; i++) {
                if (array[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        if (o != null) {
            for (int i = size - 1; i > -1; i--) {
                if (array[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> subList = new MyArrayList<>();
        for (int i = fromIndex; i < toIndex; i++) {
            subList.add(array[i]);
        }
        return subList;
    }

    @Override
    public String toString() {
        String result = "ArrayList:";
        for (int i = 0; i < size; i++) {
            result += " "  + array[i];
        }
        return result;
    }
}
