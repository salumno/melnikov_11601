package Section6.Task043;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        MyArrayList<Integer> arrayList = new MyArrayList<>();
        arrayList.add(5);
        System.out.println(arrayList);
        arrayList.add(0, 6);
        System.out.println(arrayList);
        arrayList.set(0, 7);
        System.out.println(arrayList);
    }
}
