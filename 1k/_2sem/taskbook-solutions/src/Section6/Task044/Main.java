package Section6.Task044;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> linkedList = new MyLinkedList<>();
        linkedList.add(5);
        linkedList.add(6);
        System.out.println(linkedList);
        linkedList.add(0, 7);
        System.out.println(linkedList);
        linkedList.add(9);
        List<Integer> linkedList2 = linkedList.subList(1, 3);
        System.out.println(linkedList2);
        System.out.println(linkedList.size());
        System.out.println(linkedList);
        System.out.println(linkedList.remove(2));
        System.out.println(linkedList);
    }
}
