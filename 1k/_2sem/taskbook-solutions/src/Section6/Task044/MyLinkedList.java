package Section6.Task044;

import Section3.Elem;
import Section6.Task042.MyLinkedCollection;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MyLinkedList<T> extends MyLinkedCollection<T> implements List<T> {

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (c.size() == 0) {
            return false;
        }
        for (T element: c) {
            add(index, element);
            index++;
        }
        return true;
    }

    @Override
    public T get(int index) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        int counter = 0;
        Elem<T> p;
        for (p = head; p != null && counter != index; p = p.getNext()) {
            counter++;
        }
        return p.getData();
    }

    @Override
    public T set(int index, T element) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        int counter = 0;
        Elem<T> p;
        for (p = head; p != null && counter != index; p = p.getNext()) {
            counter++;
        }
        T prevData = p.getData();
        p.setData(element);
        return prevData;
    }

    @Override
    public void add(int index, T element) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            head = new Elem<>(element, head);
        } else if (index == size()) {
            Elem<T> newElement = new Elem<>(element, null);
            tail.setNext(newElement);
            tail = newElement;
        } else {
            int counter = 0;
            Elem<T> p;
            for (p = head; p != null && counter != index - 1; p = p.getNext()) {
                counter++;
            }
            Elem<T> newElement = new Elem<>();
            newElement.setData(element);
            newElement.setNext(p.getNext());
            p.setNext(newElement);
        }
        size++;
    }

    @Override
    public T remove(int index) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        T removedData;
        if (index == 0) {
            removedData = head.getData();
            head = head.getNext();
        } else {
            int counter = 0;
            Elem<T> p;
            for (p = head; p != null && counter != index - 1; p = p.getNext()) {
                counter++;
            }
            Elem<T> neededElem = p.getNext();
            removedData = neededElem.getData();
            p.setNext(neededElem.getNext());
            size--;
        }
        return removedData;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        for (Elem p = head; p != null; p = p.getNext()) {
            if (p.getData().equals(o)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = -1;
        int counter = 0;
        for (Elem p = head; p != null; p = p.getNext()) {
            if (p.getData().equals(o)) {
                index = counter;
            }
            counter++;
        }
        return index;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex >= size() || fromIndex < 0 || toIndex - 1 >= size() || toIndex - 1 < 0) {
            throw new IndexOutOfBoundsException();
        }
        List<T> newList = new MyLinkedList<>();
        int counter = 0;
        Elem<T> p;
        for (p = head; p != null && counter != toIndex; p = p.getNext()) {
            if (counter == fromIndex) {
                newList.add(p.getData());
                fromIndex++;
            }
            counter++;
        }
        return newList;
    }

    @Override
    public String toString() {
        String result = "ArrayList:";
        for (Elem p = head; p != null; p = p.getNext()) {
            result += " " + p.getData();
        }
        return result;
    }
}
