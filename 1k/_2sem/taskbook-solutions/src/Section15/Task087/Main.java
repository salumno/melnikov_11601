package Section15.Task087;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        int[] array = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        ThreadSum myThread1 = new ThreadSum(array, 0, array.length/2);
        ThreadSum myThread2 = new ThreadSum(array, array.length/2, array.length);
        int sum = 0;
        try {
            myThread1.start();
            myThread2.start();
            myThread1.join();
            myThread2.join();
            sum = myThread1.getSum() + myThread2.getSum();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(sum);
    }
}
