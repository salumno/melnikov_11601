package Section15.Task087;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class ThreadSum extends Thread {
    private int sum = 0;
    private int[] array;
    private int left;
    private int right;

    public ThreadSum(int[] array, int left, int right) {
        this.array = array;
        this.left = left;
        this.right = right;
    }

    public void run() {
        for (int i = left; i < right; i++) {
            sum += array[i];
        }
    }

    public int getSum() {
        return sum;
    }
}
