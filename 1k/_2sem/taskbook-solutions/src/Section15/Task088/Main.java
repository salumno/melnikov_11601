package Section15.Task088;

import Section15.Task087.ThreadSum;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = sc.nextInt();
        int[] numbers = new int[n];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = sc.nextInt();
        }
        System.out.print("k = ");
        int k = sc.nextInt();
        if (n % k != 0) {
            System.out.println("N must be divisible by K. Sorry.");
            return;
        }
        int gap = n / k;
        int left = 0;
        int right = left + gap;
        int sum = 0;
        try {
            for (int i = 0; i < k; i++) {
                ThreadSum threadSum = new ThreadSum(numbers, left, right);
                left = right;
                right = left + gap;
                threadSum.start();
                threadSum.join();
                sum += threadSum.getSum();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(sum);
    }
}
