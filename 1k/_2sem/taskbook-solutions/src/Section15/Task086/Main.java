package Section15.Task086;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Count of the cities?");
        System.out.print("k = ");
        int k = sc.nextInt();
        sc.nextLine();
        Thread[] threads = new Thread[k];
        for (int i = 0; i < k; i++) {
            String city = sc.nextLine();
            String gap = sc.nextLine();
            TimeThread thread = new TimeThread(city, gap);
            threads[i] = thread;
        }
        for (Thread thread: threads) {
            thread.start();
        }
    }
}
