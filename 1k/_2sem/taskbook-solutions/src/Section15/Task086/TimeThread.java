package Section15.Task086;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class TimeThread extends Thread {

    private DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm:ss a");
    private String city;
    private int gap;

    public TimeThread(String city, String currentGap) {
        this.city = city;
        if (currentGap.startsWith("+")) {
            gap = Integer.parseInt(currentGap.substring(1));
        } else if (currentGap.startsWith("-")) {
            gap = (-1) * Integer.parseInt(currentGap.substring(1));
        } else {
            gap = Integer.parseInt(currentGap);
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                LocalDateTime localDateTime = LocalDateTime.now(ZoneOffset.UTC);
                if (gap >= 0) {
                    localDateTime = localDateTime.plusHours(gap);
                } else {
                    localDateTime = localDateTime.minusHours(Math.abs(gap));
                }
                System.out.println(city + ": " + format.format(localDateTime));
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
