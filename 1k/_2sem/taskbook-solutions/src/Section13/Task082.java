package Section13;

import Section12.Graph;
import Section12.SuperNode;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

/*
0 1
0 2
1 2
1 3
2 3
1 4
3 4
*/


public class Task082 {
    public static void main(String[] args) {
        Graph graph = new Graph(5, 7);
        graph.createGraphByConsole();
        List<SuperNode> adjacencyList = graph.getAdjacencyList();
        System.out.println(adjacencyList);
        startBacktracking(adjacencyList);
        System.out.println(adjacencyList);
    }

    private static void startBacktracking(List<SuperNode> list) {
        list.forEach(Task082::nodeBT);
    }

    private static void nodeBT(SuperNode node) {
        if (node.getColor() == -1) {
            node.setColor(chooseColor(node));
            node.getJointNode().forEach(Task082::nodeBT);
        }
    }

    private static int chooseColor(SuperNode node) {
        boolean[] colors = new boolean[4];
        Arrays.fill(colors, true);
        node.getJointNode().stream()
                .filter(
                        child -> child.getColor() != -1
                )
                .forEach(
                        child -> colors[child.getColor()] = false
                );
        for (int i = 0; i < colors.length; i++) {
            if (colors[i]) {
                return i;
            }
        }
        return -1;
    }
}
