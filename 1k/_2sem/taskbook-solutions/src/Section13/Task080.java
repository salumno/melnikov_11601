package Section13;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task080 {
    private static int count = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        bt("", length);
        System.out.println("Count: " + count);
    }

    private static void bt(String solution, int length) {
        if (solution.length() == length) {
            if (checkSolution(solution)) {
                System.out.println(solution + " [ OK ] ");
                count++;
            } else {
                return;
            }
        } else {
            for (char s = 'a'; s <= 'z'; s++) {
                solution += s;
                if (solution.length() <= length) {
                    bt(solution, length);
                }
                solution = solution.substring(0, solution.length() - 1);
            }
        }
    }

    private static boolean checkSolution(String solution) {
        int counter = 0;
        for (int i = 0; i < solution.length() && counter < 2; i++) {
            if (isVowel(solution.charAt(i))) {
                counter++;
            }
        }
        return counter < 4 ;
    }

    private static boolean isVowel(char symbol) {
        return "aeiouy".indexOf(symbol) >= 0;
    }
}
