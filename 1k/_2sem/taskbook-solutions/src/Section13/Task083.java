package Section13;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task083 {
    private static int count = 1;
    private static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        list.add(0);
        list.add(1);
        fibo(n);
        int fiboNumber = list.get(n);
        System.out.println(fiboNumber);
    }

    private static void fibo(int n) {
        if (n == 0 || n == 1) {
            return;
        }
        if (count == n) {
            return;
        } else {
            count++;
            list.add(count, list.get(count - 2) + list.get(count - 1));
            fibo(n);
        }
    }
}
