package Section13;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task079 {
    private static int count = 0;

    public static void main(String[] args) {
        int length = 5;
        bt("", length);
        System.out.println("Count: " + count);
    }

    private static void bt(String solution, int length) {
        if (solution.length() == length) {
            if (checkSolution(solution)) {
                System.out.println(solution + " [ OK ] ");
                count++;
            } else {
                return;
            }
        } else {
            for (char s = 'a'; s <= 'z'; s++) {
                solution += s;
                if (solution.length() <= 5) {
                    bt(solution, length);
                }
                solution = solution.substring(0, solution.length() - 1);
            }
        }
    }

    private static boolean checkSolution(String solution) {
        int counter = 0;
        for (int i = 0; i < solution.length() && counter < 2; i++) {
            if (isVowel(solution.charAt(i))) {
                counter++;
            }
        }
        return counter == 1 ;
    }

    private static boolean isVowel(char symbol) {
        return "aeiouy".indexOf(symbol) >= 0;
    }
}
