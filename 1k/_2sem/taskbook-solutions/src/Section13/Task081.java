package Section13;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task081 {
    private static int count = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int maxSum = sc.nextInt();
        bt("", length, maxSum);
        System.out.println("Count: " + count);
    }


    private static void bt(String solution, int length, int sum) {
        if (numLengthCheck(solution, length)) {
            if (!zeroCheck(solution)) {
                System.out.println(solution + " [ OK ] ");
                count++;
            } else {
                return;
            }
        } else {
            for (int i = 0; i < 10; i++) {
                solution += i;
                if (digitSumCheck(solution, sum)) {
                    bt(solution, length, sum);
                }
                solution = solution.substring(0, solution.length() - 1);
            }
        }
    }

    private static boolean zeroCheck(String solution) {
        boolean check = false;
        for (int i = 1; i < solution.length() && !check; i++) {
            if (Character.isDigit(solution.charAt(i))) {
                check = true;
            }
        }
        return solution.startsWith("0") && check;
    }

    private static boolean numLengthCheck(String solution, int length) {
        return solution.length() == length;
    }

    private static boolean digitSumCheck(String solution, int k) {
        int sum = 0;
        for (int i = 0; i < solution.length(); i++) {
            sum += Integer.parseInt(solution.charAt(i) + "");
        }
        return sum < k;
    }
}
