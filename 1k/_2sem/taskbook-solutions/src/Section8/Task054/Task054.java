package Section8.Task054;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task054 {
    private static String filepath = "src/Section8/Task054/in.txt";

    public static void main(String[] args) {
        Map<Integer, Integer> values = new HashMap<>();
        readFromFile(values);
        for (Map.Entry<Integer, Integer> entry: values.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue() + " times");
        }
    }

    private static void readFromFile(Map<Integer, Integer> values) {
        try {
            Scanner sc = new Scanner(new File(filepath));
            while (sc.hasNext()) {
                Integer number = Integer.parseInt(sc.next());
                if (values.containsKey(number)) {
                    values.put(number, values.get(number) + 1);
                } else {
                    values.put(number, 1);
                }
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
