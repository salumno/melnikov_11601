package Section8.Task055;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task055 {
    private static String filepath = "src/Section8/Task055/text.txt";

    public static void main(String[] args) {
        Map<Character, Integer> values = new HashMap<>();
        readFromFile(values);
        for (Map.Entry<Character, Integer> entry: values.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue() + " times");
        }
    }

    private static void readFromFile(Map<Character, Integer> values) {
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(filepath)
                    )
            );
            int c;
            while ((c = bufferedReader.read()) != -1) {
                Character character = (char) c;
                if (character >= 'A' && character <= 'Z') {
                    character = Character.toLowerCase(character);
                }
                if (character >= 'a' && character <= 'z') {
                    if (values.containsKey(character)) {
                        values.put(character, values.get(character) + 1);
                    } else {
                        values.put(character, 1);
                    }
                }
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
