package Section8.Task059;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Student {
    private String name;
    private String[] points;

    public Student(String rawLine) {
        String[] input = rawLine.split(" ");
        name = input[0];
        points = new String[input.length - 1];
        for (int i = 1; i < input.length; i++) {
            points[i - 1] = input[i];
        }
    }

    public int resolvedTaskCount() {
        int result = 0;
        for (int i = 0; i < points.length; i++) {
            if (!points[i].equals(".")) {
                result++;
            }
        }
        return result;
    }

    public double taskMidValue() {
        double sum = 0;
        for (int i = 0; i < points.length; i++) {
            int currentPoint = (points[i].equals(".")) ? 0 : Integer.parseInt(points[i]);
            sum += currentPoint;
        }
        return sum / points.length;
    }

    public String getName() {
        return name;
    }

    public String[] getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return getName();
    }
}
