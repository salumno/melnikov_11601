package Section8.Task059;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class Task059 {
    private static String inputPath = "src/Section8/Task059/in.txt";

    private static Map<Student, Integer> results = new HashMap<>();
    private static Map<Student, Double> midValue = new HashMap<>();
    private static Map<String , Integer> eachTaskCount = new HashMap<>();
    private static Map<String , Double> eachTaskMidValue = new HashMap<>();

    public static void main(String[] args) {
        statCollection();
        System.out.println(results);
        System.out.println(midValue);
        System.out.println(eachTaskCount);
        System.out.println(eachTaskMidValue);
    }

    private static void statCollection() {
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(inputPath)
                    )
            );

            int lineCount = 0;
            int taskCount = -1;
            int[] eachTaskPointSum = null;

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Student student = new Student(line);
                lineCount++;
                results.put(student, student.resolvedTaskCount());
                midValue.put(student, student.taskMidValue());
                String[] points = student.getPoints();
                //Изначально не знаем, сколько задач решали студенты
                //Если taskCount = -1, то мы еще не инициализировали массив для накопления суммы баллов задач
                //Иначе - мы узнали кол-во задач и провели инициализацию
                if (taskCount == -1) {
                    taskCount = points.length;
                    eachTaskPointSum = new int[taskCount];
                }
                //Накапливаем количество студентов на каждой задаче
                studentCountForEachTask(points);
                //Накапливаем баллы на каждой задаче. В итоге получим суммы баллов всех студентов на каждую задачу
                sumRise(points, eachTaskPointSum);
            }
            //Cчитаем среднее значение по каждой задаче
            for (int i = 0; i < eachTaskPointSum.length; i++) {
                double midValue = (double) eachTaskPointSum[i] / lineCount;
                eachTaskMidValue.put((i + 1) + "", midValue);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sumRise(String[] points, int[] currentSum) {
        for (int i = 0; i < points.length; i++) {
            int currentPoint = (points[i].equals(".")) ? 0 : Integer.parseInt(points[i]);
            currentSum[i] += currentPoint;
        }
    }

    private static void studentCountForEachTask(String[] points) {
        for (int i = 0; i < points.length; i++) {
            String taskId = (i + 1) + "";
            if (points[i].equals(".")) {
                continue;
            } else {
                if (eachTaskCount.containsKey(taskId)) {
                    eachTaskCount.put(taskId, eachTaskCount.get(taskId) + 1);
                } else {
                    eachTaskCount.put(taskId, 1);
                }
            }
        }
    }
}
