package Section8.Task058;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task058 {
    private static String dictionaryPath = "src/Section8/Task058/second.txt";
    private static String textPath = "src/Section8/Task058/first.txt";

    public static void main(String[] args) {
        Map<String, String> dictionary = new HashMap<>();
        readDictionary(dictionary);
        System.out.println(dictionary);
        String originalText = readText();
        System.out.println(originalText);
        String translation = translate(originalText, dictionary);
        System.out.println(translation);
    }

    private static String translate(String originalText, Map<String, String> dictionary) {
        String word = "";
        String translatedText = "";
        String text = originalText;
        boolean check = false;
        if (Character.isLetter(originalText.charAt(originalText.length() - 1))) {
            text += ".";
            check = true;
        }
        for (int i = 0; i < text.length(); i++) {
            char currentSymbol = text.charAt(i);
            if (Character.isLetter(currentSymbol)) {
                word += currentSymbol;
            } else {
                word = word.toLowerCase();
                if (dictionary.containsKey(word)) {
                    translatedText += dictionary.get(word);
                } else {
                    translatedText += word;
                }
                word = "";
                translatedText += currentSymbol;
            }
        }
        if (check) {
            return translatedText.substring(0, translatedText.length() - 1);
        } else {
            return translatedText;
        }
    }

    private static String readText() {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(textPath)
                    )
            );
            int c;
            while ((c = bufferedReader.read()) != -1) {
                stringBuilder.append((char)c);
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void readDictionary(Map<String, String> dictionary) {
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(dictionaryPath)
                    )
            );
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] words = line.split("=");
                dictionary.put(words[0].trim(), words[1].trim());
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
