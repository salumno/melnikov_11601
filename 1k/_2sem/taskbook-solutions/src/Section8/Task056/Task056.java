package Section8.Task056;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task056 {
    private static String filepath = "src/Section8/Task056/text.txt";

    public static void main(String[] args) {
        Map<String, Integer> values = new HashMap<>();
        readFromFile(values);
        for (Map.Entry<String, Integer> entry: values.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue() + " times");
        }
    }

    private static void readFromFile(Map<String, Integer> values) {
        try {
            Scanner sc = new Scanner(new File(filepath));
            while (sc.hasNext()) {
                String currentWord = sc.next();
                currentWord = currentWord.toLowerCase();
                if (values.containsKey(currentWord)) {
                    values.put(currentWord, values.get(currentWord) + 1);
                } else {
                    values.put(currentWord, 1);
                }
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
