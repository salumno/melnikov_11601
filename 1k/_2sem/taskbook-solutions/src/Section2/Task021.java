package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task021 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        int size = sc.nextInt();
        ListNode<Integer> head = writeList(size);
        System.out.println("Before: ");
        printList(head);
        int k = sc.nextInt();
        head = deleteSpecificElement(head, k);
        System.out.println("After: ");
        printList(head);
    }

    private static ListNode<Integer> deleteSpecificElement(ListNode<Integer> head, int num) {
        ListNode<Integer> prev = null;
        for (ListNode p = head; p != null; p = p.getNext()) {
            int currentData = (Integer) p.getData();
            if (currentData == num) {
                if (prev == null) {
                    head = p.getNext();
                } else {
                    prev.setNext(p.getNext());
                }
            } else {
                prev = p;
            }
        }
        return head;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
