package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task018 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        int size = sc.nextInt();
        ListNode<Integer> head = writeList(size);
        System.out.println("Before: ");
        printList(head);
        int l = sc.nextInt();
        for (int i = 0; i < l; i++) {
            int pos = sc.nextInt();
            int data = sc.nextInt();
            head = addElementByPosition(head, pos, data, size);
        }
        System.out.println("After: ");
        printList(head);
    }

    private static ListNode<Integer> addElementByPosition(ListNode<Integer> head, int pos, int data, int size) {
        int counter = 0;
        ListNode<Integer> prev = null;
        for (ListNode p = head; p != null; p = p.getNext()) {
            if (counter == pos) {
                ListNode<Integer> newNode = new ListNode<>(p, data);
                if (prev != null) {
                    prev.setNext(newNode);
                } else {
                    head = newNode;
                }
                break;
            }
            prev = p;
            counter++;
        }
        if (pos >= size) {
            ListNode<Integer> newNode = new ListNode<>(null, data);
            prev.setNext(newNode);
        }
        return head;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
