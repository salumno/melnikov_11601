package Section2;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class ListNodeWithBool<T> {
    private ListNodeWithBool next;
    private T data;
    private boolean checker;

    public ListNodeWithBool() {}

    public ListNodeWithBool(ListNodeWithBool next, T data) {
        this(next, data, false);
    }

    public ListNodeWithBool(ListNodeWithBool next, T data, boolean checker) {
        this.next = next;
        this.data = data;
        this.checker = checker;
    }

    public ListNodeWithBool getNext() {
        return next;
    }

    public void setNext(ListNodeWithBool next) {
        this.next = next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isChecker() {
        return checker;
    }

    public void setChecker(boolean checker) {
        this.checker = checker;
    }
}
