package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task016 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        ListNodeWithBool<Integer> head = writeList(sc.nextInt());
        System.out.println("Before: ");
        printList(head);
        System.out.println("User number: ");
        int x = sc.nextInt();
        System.out.println("After: ");
        ListNodeWithBool<Integer> newHead = addBeforeEven(head, x);
        printList(newHead);
    }

    private static ListNodeWithBool<Integer> addBeforeEven(ListNodeWithBool<Integer> head, int x) {
        ListNodeWithBool<Integer> prev = null;
        for (ListNodeWithBool p = head; p != null; p = p.getNext()) {
            int currentData = (Integer) p.getData();
            if (currentData % 2 == 0) {
                ListNodeWithBool<Integer> newNode = new ListNodeWithBool<>(p, x, true);
                if (prev != null) {
                    prev.setNext(newNode);
                } else {
                    head = newNode;
                }
            }
            prev = p;
        }
        return head;
    }

    private static ListNodeWithBool<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNodeWithBool<Integer> head = null;
        ListNodeWithBool<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNodeWithBool<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNodeWithBool<Integer> head) {
        ListNodeWithBool<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
