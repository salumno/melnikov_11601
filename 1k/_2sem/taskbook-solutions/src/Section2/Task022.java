package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов в первом списке: ");
        int size1 = sc.nextInt();
        ListNode<Integer> head1 = writeList(size1);
        System.out.println("Количество элементов во втором списке: ");
        int size2 = sc.nextInt();
        ListNode<Integer> head2 = writeList(size2);
        System.out.println("Before: ");
        printList(head1);
        printList(head2);
        ListNode<Integer> newHead = listMerge(head1, head2);
        System.out.println("After: ");
        printList(newHead);
    }

    private static ListNode<Integer> listMerge(ListNode<Integer> head1, ListNode<Integer> head2) {
        ListNode<Integer> head = null;
        ListNode<Integer> tail = null;
        ListNode<Integer> i = head1;
        ListNode<Integer> j = head2;
        while (i != null && j != null) {
            int data1 = i.getData();
            int data2 = j.getData();
            if (data1 <= data2) {
                if (head == null) {
                    head = i;
                    tail = i;
                } else {
                    tail.setNext(i);
                    tail = i;
                }
                i = i.getNext();
            } else {
                if (head == null) {
                    head = j;
                    tail = j;
                } else {
                    tail.setNext(j);
                    tail = j;
                }
                j = j.getNext();
            }
        }
        if (i == null) {
            tail.setNext(j);
        } else {
            tail.setNext(i);
        }
        return head;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
