package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task023 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        int size = sc.nextInt();
        ListNode<Integer> head = writeList(size);
        System.out.println("Before: ");
        printList(head);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            int pos = sc.nextInt();
            head = removeElementByPos(head, pos);
        }
        System.out.println("After: ");
        printList(head);
    }

    private static ListNode<Integer> removeElementByPos(ListNode<Integer> head, int pos) {
        ListNode<Integer> prev = null;
        int counter = 0;
        for (ListNode p = head; p != null; p = p.getNext()) {
            if (counter == pos) {
                if (prev == null) {
                    head = p.getNext();
                } else {
                    prev.setNext(p.getNext());
                }
            } else {
                prev = p;
            }
            counter++;
        }
        return head;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
