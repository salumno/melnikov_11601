package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task024 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        int size = sc.nextInt();
        ListNode<Integer> head = writeList(size);
        System.out.println("Before: ");
        printList(head);
        System.out.println("Odd: ");
        printList(getOddList(head));
        System.out.println("Even: ");
        printList(getEvenList(head));
    }

    private static ListNode<Integer> getOddList(ListNode<Integer> h) {
        ListNode<Integer> head = null;
        ListNode<Integer> tail = null;
        for (ListNode p = h; p != null; p = p.getNext()) {
            int data = (int) p.getData();
            if (data % 2 != 0) {
                ListNode<Integer> newElement = new ListNode<>();
                newElement.setData(data);
                if (head == null) {
                    head = newElement;
                    tail = newElement;
                } else {
                    tail.setNext(newElement);
                    tail = newElement;
                }
            }
        }
        return head;
    }

    private static ListNode<Integer> getEvenList(ListNode<Integer> head) {
        ListNode<Integer> prev = null;
        for (ListNode p = head; p != null; p = p.getNext()) {
            int data = (int) p.getData();
            if (data % 2 != 0) {
                if (prev == null) {
                    head = p.getNext();
                } else {
                    prev.setNext(p.getNext());
                }
            } else {
                prev = p;
            }
        }
        return head;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
