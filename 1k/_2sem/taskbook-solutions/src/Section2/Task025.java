package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task025 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество знаков в числе: ");
        int size = sc.nextInt();
        ListNode<Integer> head = writeList(size);
        printList(decimalList(toDecimal(head)));
    }

    private static int toDecimal(ListNode<Integer> head) {
        int number = 0;
        int degree = 1;
        for (ListNode p = head; p != null; p = p.getNext()) {
            if ((int)p.getData() == 1) {
                number += degree;
            }
            degree *= 2;
        }
        return number;
    }

    private static ListNode<Integer> decimalList(int num) {
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        while (num > 0) {
            int mod = num % 10;
            num = num / 10;
            p = new ListNode<>(head, mod);
            head = p;
        }
        return head;
    }

    private static ListNode<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNode<Integer> head = null;
        ListNode<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNode<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNode<Integer> head) {
        ListNode<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData());
            if (p.getNext() != null) {
                System.out.print(" -> ");
            }
            p = p.getNext();
        }
        System.out.println();
    }
}
