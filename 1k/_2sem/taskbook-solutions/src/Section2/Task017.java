package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        ListNodeWithBool<Integer> head = writeList(sc.nextInt());
        System.out.println("Before: ");
        printList(head);
        System.out.println("After: ");
        ListNodeWithBool<Integer> newHead = addPrimesDigit(head);
        printList(newHead);
    }

    private static ListNodeWithBool<Integer> addPrimesDigit(ListNodeWithBool<Integer> head) {
        ListNodeWithBool<Integer> prev = null;
        for (ListNodeWithBool p = head; p != null; p = p.getNext()) {
            int currentData = (Integer) p.getData();
            if (isPrime(currentData) && !p.isChecker()) {
                //Надо добавить и вперед, и назад
                ListNodeWithBool<Integer> before = new ListNodeWithBool<>(p, getFirstDigit(currentData), true);
                if (prev != null) {
                    prev.setNext(before);
                } else {
                    head = before;
                }
                ListNodeWithBool<Integer> after = new ListNodeWithBool<>(p.getNext(), getLastDigit(currentData) ,true);
                p.setNext(after);
            }
            prev = p;
        }
        return head;
    }

    private static int getLastDigit(int currentData) {
        return Math.abs(currentData) % 10;
    }

    private static int getFirstDigit(int currentData) {
        currentData = Math.abs(currentData);
        int mod = 0;
        while (currentData > 0) {
            mod = currentData % 10;
            currentData = currentData / 10;
        }
        return mod;
    }

    private static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i * i < num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static ListNodeWithBool<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNodeWithBool<Integer> head = null;
        ListNodeWithBool<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNodeWithBool<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNodeWithBool<Integer> head) {
        ListNodeWithBool<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }
}
