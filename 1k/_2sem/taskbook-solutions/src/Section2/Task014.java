package Section2;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task014 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        ListNodeWithBool<Integer> head = writeList(sc.nextInt());
        System.out.println("Before: ");
        printList(head);
        System.out.println("User number: ");
        int x = sc.nextInt();
        System.out.println("After: ");
        addAfterEven(head, x);
        printList(head);
    }

    private static void addAfterEven(ListNodeWithBool<Integer> head, int x) {
        for (ListNodeWithBool p = head; p != null; p = p.getNext()) {
            int data = (Integer)p.getData();
            if (data % 2 == 0 && !p.isChecker()) {
                ListNodeWithBool<Integer> newNode = new ListNodeWithBool<>(p.getNext(), x, true);
                p.setNext(newNode);
            }
        }
    }

    private static ListNodeWithBool<Integer> writeList(int countOfElements) {
        Scanner sc = new Scanner(System.in);
        ListNodeWithBool<Integer> head = null;
        ListNodeWithBool<Integer> p;
        for (int i = 0; i < countOfElements; i++) {
            p = new ListNodeWithBool<>(head, sc.nextInt());
            head = p;
        }
        return head;
    }

    private static void printList(ListNodeWithBool<Integer> head) {
        ListNodeWithBool<Integer> p = head;
        while (p != null) {
            System.out.print(p.getData() + " ");
            p = p.getNext();
        }
        System.out.println();
    }

}
