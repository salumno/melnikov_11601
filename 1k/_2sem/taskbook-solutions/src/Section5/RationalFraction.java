package Section5;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class RationalFraction implements Addable<RationalFraction>{
    private int num;
    private int denom;

    public RationalFraction(int num, int denom) {
        this.num = num;
        this.denom = denom;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getDenom() {
        return denom;
    }

    public void setDenom(int denom) {
        this.denom = denom;
    }

    @Override
    public RationalFraction add(RationalFraction data) {
        int d = denom * data.getDenom();
        int n = num * data.getDenom() + data.getNum() * denom;
        return new RationalFraction(n, d);
    }

    @Override
    public String toString() {
        return num + "/" + denom;
    }
}
