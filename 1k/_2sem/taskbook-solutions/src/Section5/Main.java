package Section5;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        ComplexNumber number1 = new ComplexNumber(5, 5);
        ComplexNumber number2 = new ComplexNumber(2, -3);
        Vector2D<ComplexNumber> complexVector1 = new Vector2D<>(number1, number2);
        ComplexNumber number3 = new ComplexNumber(2, 3);
        ComplexNumber number4 = new ComplexNumber(2, -1);
        Vector2D<ComplexNumber> complexVector2 = new Vector2D<>(number3, number4);
        Vector2D<ComplexNumber> result = complexVector1.add(complexVector2);
        System.out.println(result);

        RationalFraction rationalFraction1 = new RationalFraction(1, 2);
        RationalFraction rationalFraction2 = new RationalFraction(3, 5);
        Vector2D<RationalFraction> rationalVector1 = new Vector2D<>(rationalFraction1, rationalFraction2);
        RationalFraction rationalFraction3 = new RationalFraction(1, 3);
        RationalFraction rationalFraction4 = new RationalFraction(2, 5);
        Vector2D<RationalFraction> rationalVector2 = new Vector2D<>(rationalFraction3, rationalFraction4);
        Vector2D<RationalFraction> rationalResult = rationalVector1.add(rationalVector2);
        System.out.println(rationalResult);
    }
}
