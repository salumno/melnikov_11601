package Section5;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface Addable<T> {
    T add(T data);
}
