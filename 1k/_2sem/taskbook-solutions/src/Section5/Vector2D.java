package Section5;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Vector2D<T extends Addable> {
    private T x;
    private T y;

    public Vector2D(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D<T> add(Vector2D vector) {
        T newX = (T)x.add(vector.getX());
        T newY = (T)y.add(vector.getY());
        return new Vector2D<>(newX, newY);
    }

    public T getX() {
        return x;
    }

    public void setX(T x) {
        this.x = x;
    }

    public T getY() {
        return y;
    }

    public void setY(T y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "{" + x + ", " + y + "}";
    }
}
