package Section5;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class ComplexNumber implements Addable<ComplexNumber> {
    private int real;
    private int imaginary;

    public ComplexNumber(int real, int imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    @Override
    public ComplexNumber add(ComplexNumber data) {
        return new ComplexNumber(real + data.getReal(), imaginary + data.getImaginary());
    }

    public int getReal() {
        return real;
    }

    public void setReal(int real) {
        this.real = real;
    }

    public int getImaginary() {
        return imaginary;
    }

    public void setImaginary(int imaginary) {
        this.imaginary = imaginary;
    }

    @Override
    public String toString() {
        return real + " + (" + imaginary + "i)";
    }
}
