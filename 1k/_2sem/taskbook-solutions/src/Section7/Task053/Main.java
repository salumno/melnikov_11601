package Section7.Task053;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(123);
        list.add(51);
        list.add(70);
        list.add(25);
        list.add(19);
        System.out.println(list);
        list.sort((o1, o2) -> {
            int reverse1 = reverseNumber(o1);
            int reverse2 = reverseNumber(o2);
            if (reverse1 > reverse2) {
                return 1;
            } else if (reverse1 < reverse2) {
                return -1;
            } else {
                return 0;
            }
        });
        System.out.println(list);
    }

    private static int reverseNumber(int number) {
        int result = 0;
        while (number > 0) {
            int mod = number % 10;
            number = number / 10;
            result = (result + mod) * 10;
        }
        result /= 10;
        return result;
    }
}
