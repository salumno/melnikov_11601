package Section7.Task046;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Vector2D implements Comparable<Vector2D>{
    private int x;
    private int y;

    public Vector2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double length() {
        return Math.sqrt(x * x + y * y);
    }

    @Override
    public int compareTo(Vector2D o) {
        if (length() > o.length()) {
            return 1;
        } else if (length() < o.length()) {
            return -1;
        } else {
            return 0;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Vector2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
