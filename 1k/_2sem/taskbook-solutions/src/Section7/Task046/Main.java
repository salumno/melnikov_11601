package Section7.Task046;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        Vector2D vector1 = new Vector2D(1, 1);
        Vector2D vector2 = new Vector2D(4, 5);
        Vector2D vector3 = new Vector2D(2, 1);
        List<Vector2D> list = new ArrayList<>();
        list.add(vector1);
        list.add(vector2);
        list.add(vector3);
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
    }
}
