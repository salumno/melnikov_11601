package Section7.Task050;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new MyLinkedList<>();
        list.add(5);
        list.add(8);
        list.add(9);
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
