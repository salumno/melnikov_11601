package Section7.Task052;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class WebsiteData implements Comparable<WebsiteData>{
    private String name;
    private int midValue;

    public WebsiteData(String rawInput) {
        String[] data = rawInput.split(" ");
        name = data[0];
        int sum = 0;
        for (int i = 1; i < data.length; i++) {
            sum += Integer.parseInt(data[i]);
        }
        midValue = sum / 7;
    }

    public String getName() {
        return name;
    }

    public int getMidValue() {
        return midValue;
    }

    @Override
    public String toString() {
        return "WebsiteData{" +
                "name='" + name + '\'' +
                ", midValue=" + midValue +
                '}';
    }

    @Override
    public int compareTo(WebsiteData o) {
        return getName().compareTo(o.getName());
    }
}
