package Section7.Task052;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    private static final String OUTPUT_PATH = "src/Section7/Task052/out.txt";
    private static final String INPUT_PATH = "src/Section7/Task052/in.txt";

    public static void main(String[] args) {
        List<WebsiteData> dataList = new ArrayList<>();
        readFromFile(dataList);
        Collections.sort(dataList);
        writeToFile(dataList);
    }

    private static void readFromFile(List<WebsiteData> dataList) {
        try {
            Scanner sc = new Scanner(new File(INPUT_PATH));
            while (sc.hasNextLine()) {
                String currentData = sc.nextLine();
                dataList.add(new WebsiteData(currentData));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeToFile(List<WebsiteData> list) {
        try {
            PrintWriter printWriter = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(OUTPUT_PATH)
                    )
            , true);
            for (WebsiteData websiteData: list) {
                printWriter.println(websiteData.getName() + " " + websiteData.getMidValue());
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
