package Section7.Task051;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    private static final String OUTPUT_PATH = "src/Section7/Task051/out.txt";
    private static final String INPUT_PATH = "src/Section7/Task051/in.txt";

    public static void main(String[] args) {
        List<WebsiteData> dataList = new ArrayList<>();
        readFromFile(dataList);
        writeToFile(dataList);
    }

    private static void readFromFile(List<WebsiteData> dataList) {
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(INPUT_PATH)
                    )
            );
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                dataList.add(new WebsiteData(line));
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeToFile(List<WebsiteData> list) {
        try {
            PrintWriter printWriter = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(OUTPUT_PATH)
                    )
            , true);
            for (WebsiteData websiteData: list) {
                printWriter.println(websiteData.getName() + " " + websiteData.getMidValue());
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
