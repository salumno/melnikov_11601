package Section7.Task045;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task045 {
    private static String filepathIn = "src/Section7/Task045/in.txt";
    private static String filepathOut = "src/Section7/Task045/out.txt";

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        readFromFile(list);
        System.out.println(list);
        list.sort((o1, o2) -> {
            int count1 = digitsCount(o1);
            int count2 = digitsCount(o2);
            if (count1 > count2) {
                return 1;
            } else if (count1 < count2) {
                return -1;
            } else {
                return 0;
            }
        });
        System.out.println(list);
        writeToFile(list);
    }

    private static void writeToFile(List<Integer> list) {
        try {
            PrintWriter printWriter = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(filepathOut)
                    )
            , true);
            Iterator<Integer> iterator = list.iterator();
            while (iterator.hasNext()) {
                int data = iterator.next();
                printWriter.println(data);
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFromFile(List<Integer> list) {
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(filepathIn)
                    )
            );
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                list.add(Integer.parseInt(line));
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int digitsCount(int num) {
        int counter = 0;
        while (num > 0) {
            num = num / 10;
            counter++;
        }
        return counter;
    }
}
