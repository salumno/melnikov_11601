package Section7.Task048;

import java.util.Comparator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class StringByIndexComparator implements Comparator<String> {

    private int index;

    public StringByIndexComparator(int index) {
        this.index = index;
    }

    @Override
    public int compare(String o1, String o2) {
        char char1 = o1.charAt(index);
        char char2 = o2.charAt(index);
        if (char1 > char2) {
            return 1;
        } else if (char1 < char2) {
            return -1;
        } else {
            return 0;
        }
    }
}
