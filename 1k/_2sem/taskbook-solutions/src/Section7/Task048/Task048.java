package Section7.Task048;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task048 {
    private static int k;

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        readFromFile(list);
        try {
            PrintWriter printWriter = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream("src/Section7/Task048/out.txt")
                    )
            );
            for (int i = 1; i <= k; i++) {
                list.sort(new StringByIndexComparator(i - 1));
                writeToFile(printWriter, list);
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void writeToFile(PrintWriter printWriter, List<String> list) {
        printWriter.println("-----//-----");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String data = iterator.next();
            printWriter.println(data);
        }
    }

    private static void readFromFile(List<String> list) {
        try {
            Scanner sc = new Scanner(new File("src/Section7/Task048/in.txt"));
            k = sc.nextInt();
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                if (currentLine.length() >= k) {
                    list.add(currentLine);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
