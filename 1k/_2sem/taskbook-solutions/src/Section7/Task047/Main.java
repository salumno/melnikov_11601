package Section7.Task047;

import Section7.Task046.Vector2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        Vector2D vector1 = new Vector2D(1, 1);
        Vector2D vector4 = new Vector2D(1, 11);
        Vector2D vector2 = new Vector2D(4, 5);
        Vector2D vector3 = new Vector2D(2, 1);
        List<Vector2D> list = new ArrayList<>();
        list.add(vector1);
        list.add(vector2);
        list.add(vector3);
        list.add(vector4);
        System.out.println(list);
        list.sort(new VectorComparator());
        System.out.println(list);
    }
}
