package Section7.Task047;

import Section7.Task046.Vector2D;

import java.util.Comparator;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class VectorComparator implements Comparator<Vector2D>{

    @Override
    public int compare(Vector2D o1, Vector2D o2) {
        int x1 = o1.getX();
        int y1 = o1.getY();
        int x2 = o2.getX();
        int y2 = o2.getY();
        if (x1 > x2) {
            return 1;
        } else if (x1 < x2) {
            return -1;
        } else {
            if (y1 > y2) {
                return 1;
            } else if (y1 < y2) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
