import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class TestArraysGeneration {

    private int countOfArrays = 50;
    private String path = "test.txt";

    public void writeArraysToFile() {
        Random r = new Random();
        try {
            FileWriter writer = new FileWriter(path, true);
            for (int i = 0; i < countOfArrays; i++) {
                int countOfElement = r.nextInt(9901) + 100;
                for (int j = 0; j < countOfElement; j++) {
                    int currentElem = r.nextInt();
                    writer.write(currentElem + ", ");

                }
                writer.append("\n");
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPath() {
        return path;
    }

    public int getCountOfArrays() {
        return countOfArrays;
    }
}
