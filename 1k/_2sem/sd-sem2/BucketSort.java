import java.io.*;
import java.util.*;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class BucketSort {
    private static int iteratorCounter = 0;

    public static void main(String[] args) {
        TestArraysGeneration generation = new TestArraysGeneration();
        //generation.writeArraysToFile();
        String pathToFile = generation.getPath();
        List<List<Integer>> listOfTest = readFromFile(pathToFile);

        System.out.println("n   it  time");
        System.out.println("\nArray");
        for (int i = 0; i < listOfTest.size(); i++) {
            List<Integer> currentTest = listOfTest.get(i);
            Integer[] test = new Integer[currentTest.size()];
            test = currentTest.toArray(test);
            bucketSort(test);
        }
        System.out.println("\nArrayList");
        for (int i = 0; i < listOfTest.size(); i++) {
            List<Integer> currentTest = listOfTest.get(i);
            ArrayList<Integer> test = new ArrayList<>(currentTest);
            bucketSort(test);
        }
        System.out.println("\nLinkedList");
        for (int i = 0; i < listOfTest.size(); i++) {
            List<Integer> currentTest = listOfTest.get(i);
            LinkedList<Integer> test = new LinkedList<>(currentTest);
            bucketSort(test);
        }
    }

    private static void bucketSort(Integer[] array) {
        iteratorCounter = 0;

        long timeBefore = System.currentTimeMillis();

        if (array == null) {
            return;
        }
        int max = array[0];
        int min = array[0];

        for (int currElem: array) {
            iteratorCounter++;
            if (currElem > max) {
                max = currElem;
            }
            if (currElem < min) {
                min = currElem;
            }
        }

        int bucketSize = 10000;
        int bucketCount = (max / bucketSize) - (min / bucketSize) + 1;

        List<List<Integer>> bucket = new ArrayList<>(bucketCount);
        for (int i = 0; i < bucketCount; i++) {
            bucket.add(new ArrayList<>());
        }

        for (int i = 0; i < array.length; i++) {
            iteratorCounter++;
            bucket.get((array[i] / bucketSize) - (min / bucketSize)).add(array[i]);
        }

        int currentIndex = 0;
        for (int i = 0; i < bucket.size(); i++) {
            Integer[] currentBucketArray = new Integer[bucket.get(i).size()];
            currentBucketArray = bucket.get(i).toArray(currentBucketArray);
            insertionSort(currentBucketArray);
            for (int j = 0; j < currentBucketArray.length; j++) {
                iteratorCounter++;
                array[currentIndex++] = currentBucketArray[j];
            }
        }
        long timeAfter = System.currentTimeMillis();
        System.out.println(array.length + " " + iteratorCounter + " " + (timeAfter - timeBefore));

    }

    private static void bucketSort(List<Integer> list) {
        iteratorCounter = 0;

        long timeBefore = System.currentTimeMillis();

        if (list == null) {
            return;
        }
        int max = list.get(0);
        int min = list.get(0);

        for (int currElem: list) {
            iteratorCounter++;
            if (currElem > max) {
                max = currElem;
            }
            if (currElem < min) {
                min = currElem;
            }
        }

        int bucketSize = 10000;
        int bucketCount = (max / bucketSize) - (min / bucketSize) + 1;

        List<List<Integer>> bucket = new ArrayList<>(bucketCount);
        for (int i = 0; i < bucketCount; i++) {
            bucket.add(new ArrayList<>());
        }

        for (int i = 0; i < list.size(); i++) {
            iteratorCounter++;
            bucket.get((list.get(i) / bucketSize) - (min / bucketSize)).add(list.get(i));
        }
        list.clear();
        for (int i = 0; i < bucket.size(); i++) {
            Integer[] currentBucketArray = new Integer[bucket.get(i).size()];
            currentBucketArray = bucket.get(i).toArray(currentBucketArray);
            insertionSort(currentBucketArray);
            for (int j = 0; j < currentBucketArray.length; j++) {
                iteratorCounter++;
                list.add(currentBucketArray[j]);
            }
        }
        long timeAfter = System.currentTimeMillis();
        System.out.println(list.size() + " " + iteratorCounter + " " + (timeAfter - timeBefore));

    }

    private static void insertionSort(Integer[] bucket) {
        for (int i = 1; i < bucket.length; i++) {
            int currentElement = bucket[i];
            int index = i;
            while (index > 0 && bucket[index - 1].compareTo(currentElement) > 0) {
                iteratorCounter++;
                bucket[index] = bucket[--index];
            }
            bucket[index] = currentElement;
        }
    }

    private static List<List<Integer>> readFromFile(String path) {
        List<List<Integer>> result = new ArrayList<>();

        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                result.add(fromLineToArray(currentLine));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static ArrayList<Integer> fromLineToArray(String currentLine) {
        ArrayList<Integer> result = new ArrayList<>();
        String[] currentStringArrayOfElement = currentLine.split(", ");
        for (int i = 0; i < currentStringArrayOfElement.length; i++) {
            Integer currentElem = Integer.parseInt(currentStringArrayOfElement[i]);
            result.add(currentElem);
        }
        return result;
    }
}
