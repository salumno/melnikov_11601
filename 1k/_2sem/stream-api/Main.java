package stream.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    public static void main(String[] args) {
        String[] array = {"1", "-5", "3", "-6", "-343434",
                "6", "-36", "15", "59", "-8",
                "4554", "42", "-1337", "128", "-148",
                "5", "131", "23", "56.4", "5"};
        System.out.println(
                Arrays.stream(array).count()
        );
        System.out.println(
                Arrays.stream(array)
                        .map(
                                (x) -> x.substring(1)
                        )
                        .collect(Collectors.toList())

        );
        System.out.println(
                Arrays.stream(array)
                        .filter((x) -> (!x.contains(".")))
                        .map(Integer::parseInt)
                        .filter((x) -> (x < 0))
                        .collect(Collectors.toList())
        );
        Arrays.stream(array)
                .filter((x) -> (!x.contains(".")))
                .map(Integer::parseInt)
                .filter((x) -> (x < 0))
                .map((x) -> (x + 5))
                .forEach(System.out::println);
        System.out.println("Число чисел " +
                Arrays.stream(array)
                        .filter((x) -> (!x.contains(".")))
                        .map(Integer::parseInt)
                        .filter((x) -> (x < 0))
                        .map((x) -> (x + 5))
                        .peek((x) -> System.out.print(x + ", "))
                        .count()
        );
        Student s1 = new Student("Semen", 19, "male");
        Student s2 = new Student("Nikita", 18, "male");
        Student s3 = new Student("Vlad", 19, "male");
        Student s4 = new Student("Eldar", 18, "female");
        List<Student> students = new ArrayList<>();
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        System.out.println(students
                .stream()
                .filter((x) -> (x.getSex().equals("male")))
                .collect(Collectors.toList())
        );
    }
}
