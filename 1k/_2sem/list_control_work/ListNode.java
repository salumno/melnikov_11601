package list_control_work;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class ListNode {
    private ListNode next;
    private char[] data;

    public ListNode() {}

    public ListNode(ListNode next, char[] data) {
        this.next = next;
        this.data = data;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public char[] getData() {
        return data;
    }

    public void setData(char[] data) {
        this.data = data;
    }
}
