package list_control_work;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode head = null;
        ListNode p;

        System.out.println("Кол-во char массивов");
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("Введите кол-во элементов в текущем массиве");
            int size = sc.nextInt();
            char[] currentArray = new char[size];
            System.out.println("Введите элементы");
            for (int j = 0; j < currentArray.length; j++) {
                String currentElem = sc.next();
                currentArray[j] = currentElem.charAt(0);
            }
            p = new ListNode(head, currentArray);
            head = p;
        }
        printCharArrayList(head);
    }

    public static void printCharArrayList(ListNode head) {
        System.out.println("В листе на данный момент:");
        ListNode p = head;
        while (p != null) {
            char[] currentCharArray = p.getData();
            for (int i = 0; i < currentCharArray.length; i++) {
                System.out.print(currentCharArray[i] + " ");
            }
            System.out.println();
            p = p.getNext();
        }
    }
}
