package list_control_work;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode head = null;
        ListNode p;

        System.out.println("Кол-во char массивов");
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("Введите кол-во элементов в текущем массиве");
            int size = sc.nextInt();
            char[] currentArray = new char[size];
            System.out.println("Введите элементы");
            for (int j = 0; j < currentArray.length; j++) {
                String currentElem = sc.next();
                currentArray[j] = currentElem.charAt(0);
            }
            p = new ListNode(head, currentArray);
            head = p;
        }

        int max = -1;
        int firstIndex = -1;
        int secondIndex = -1;
        int currentIndex = 1;
        ListNode prev = head;
        ListNode currentNode = head.getNext();
        while (currentNode != null) {
            int currentSum = sumNumber(prev) + sumNumber(currentNode);
            if (currentSum > max) {
                max = currentSum;
                firstIndex = currentIndex - 1;
                secondIndex = currentIndex;
            }
            currentIndex++;
            prev = currentNode;
            currentNode = currentNode.getNext();
        }

        System.out.println(firstIndex);
        System.out.println(secondIndex);
        currentIndex = 0;
        p = head;
        while (p != null) {
            if (currentIndex == firstIndex) {
                System.out.println(Arrays.toString(p.getData()));
                System.out.println(Arrays.toString(p.getNext().getData()));
                break;
            }
            p = p.getNext();
            currentIndex++;
        }
    }

    private static int sumNumber(ListNode currentNode) {
        char[] currentChar = currentNode.getData();
        int count = 0;
        for (int i  = 0; i < currentChar.length; i++) {
            count += currentChar[i];
        }
        return count;
    }
}
