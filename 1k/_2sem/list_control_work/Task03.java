package list_control_work;

import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Task03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ListNode head = null;
        ListNode p;

        System.out.println("Кол-во char массивов");
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("Введите кол-во элементов в текущем массиве");
            int size = sc.nextInt();
            char[] currentArray = new char[size];
            System.out.println("Введите элементы");
            for (int j = 0; j < currentArray.length; j++) {
                String currentElem = sc.next();
                currentArray[j] = currentElem.charAt(0);
            }
            p = new ListNode(head, currentArray);
            head = p;
        }
        printCharArrayList(head);
        System.out.println("Введите строку");
        sc.nextLine();
        String userLine = sc.nextLine();
        ListNode newHead = deleteCorrectElements(head, userLine);
        printCharArrayList(newHead);

    }

    public static ListNode deleteCorrectElements(ListNode userHead, String userString) {
        boolean deleteCheck = true;
        ListNode head = userHead;
        while (deleteCheck) {
            deleteCheck = false;
            if (head == null) {
                return null;
            }
            if (findCharInString(head.getData(), userString)) {
                head = head.getNext();
                deleteCheck = true;
                continue;
            }
            ListNode currentNode = head.getNext();
            ListNode prev = head;
            while (currentNode != null) {
                if (findCharInString(currentNode.getData(), userString)) {
                    prev.setNext(currentNode.getNext());
                    deleteCheck = true;
                    break;
                }
                prev = currentNode;
                currentNode = currentNode.getNext();
            }
        }
        return head;
    }

    private static boolean findCharInString(char[] currentArray, String userString) {
        boolean check = false;
        int count = 0;
        for (int i = 0; i < currentArray.length && !check; i++) {
            char currentArrayElem = currentArray[i];
            for (int j = 0; j < userString.length() && !check; j++) {
                if (currentArrayElem == userString.charAt(j)) {
                    count++;
                    if (count > 3) {
                        check = true;
                    }
                }
            }
        }
        return check;
    }


    public static void printCharArrayList(ListNode head) {
        System.out.println("В листе на данный момент:");
        ListNode p = head;
        while (p != null) {
            char[] currentCharArray = p.getData();
            for (int i = 0; i < currentCharArray.length; i++) {
                System.out.print(currentCharArray[i] + " ");
            }
            System.out.println();
            p = p.getNext();
        }
    }
}
