/*
 * Melnikov Semen
 * 11-601
 */

public class FightingGameLauncher {
	
    public static void main(String[] args) {
        FightingGame game = new FightingGame();
        game.startGame();
    }
}