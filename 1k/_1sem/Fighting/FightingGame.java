/*

*/

import java.util.Scanner;

public class FightingGame {

    private boolean hitValue(int hit, String name) {
        boolean hitCheck = true;
        double p = 1.0 / hit;
        double randomValue = Math.random();
        if (randomValue > p) {
            hitCheck = false;
        }
        if (hitCheck) {
            System.out.println(name + " снес " + hit + " хп своему сопернику");
        } else {
            System.out.println(name + " промахнулся!");
            System.out.println();
        }
        return hitCheck;
    }

    public void startGame() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to battle area!");
        System.out.println("Игрок1, представьтесь!");
        String name1 = sc.nextLine();
        System.out.println("Игрок2, представьтесь!");
        String name2 = sc.nextLine();
        Player player1 = new Player(name1);
        Player player2 = new Player(name2);
        boolean gameCheck = true;
        boolean player = true;
        while (gameCheck) {
            String currentName;
            String enemyName;
            if (player) { 
                currentName = player1.getName();
                enemyName = player2.getName();
            } else {
                currentName = player2.getName();
                enemyName = player1.getName();
            }
            System.out.println(currentName + " бьёт!");
            int hit = sc.nextInt(); 
            if (hitValue(hit, currentName)) { 
                if (player) {
                    player2.damageHP(hit);
                } else {
                    player1.damageHP(hit);
                }
            }
            player = !player;
            if (player1.getHP() <= 0 || player2.getHP() <= 0) {
                gameCheck = false;
                System.out.println(enemyName + " слился! Муахахахахах");
            }
        }
    }
}