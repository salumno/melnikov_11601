/*

*/

public class Player {

    private double hp;
    private String name;

    public Player(String name) {
        this.name = name;
        hp = 10;
    }

    public String getName() {
        return name;
    }

    public double getHP() {
        return hp;
    }

    public void damageHP(double hit) {
        hp -= hit;
        System.out.print("У " + name + " осталось ");
        if (hp <= 0) {
        	System.out.println("0 хп!");
        } else {
        	System.out.println(hp + " хп!");
        }
        System.out.println();
    }

}