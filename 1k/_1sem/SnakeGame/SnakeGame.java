import java.util.Scanner;

public class SnakeGame {

    public void startGame() {
        System.out.println("Welcome to the Snake home!");
        System.out.println("Please, use only WASD to control the Snake");
        Field field = new Field();
        Snake snake = new Snake(field.getSIZE());
        Food food = new Food();
        field.render(snake);
        field.render(food, snake);
        field.show();
        boolean playCheck = true;
        do {
            char move = inputCheck();
            Coordinates nextPoint = newPoint(move, snake);
            if (checkPoint(nextPoint, field)) {
                if (checkFood(nextPoint, field)) {
                    snake.snakeAppleMove(nextPoint);
                    field.render(food, snake);
                } else {
                    snake.snakeMove(nextPoint);
                }
                field.vanish();
                field.render(snake);
                field.show();
            } else {
                playCheck = false;
                System.out.println("GAME OVER");
            }
        } while (playCheck);
    }

    private Coordinates newPoint(char move, Snake snake) {
        int row = snake.snakePoint(0).getX();
        int column = snake.snakePoint(0).getY();
        switch (move) {
            case 'W':
                row -= 1;
                break;
            case 'S':
                row += 1;
                break;
            case 'A':
                column -= 1;
                break;
            case 'D':
                column += 1;
                break;
        }
        Coordinates result = new Coordinates();
        result.setX(row);
        result.setY(column);
        return result;
    }

    private boolean checkPoint(Coordinates coordinates, Field field) {
        int row = coordinates.getX();
        int column = coordinates.getY();
        boolean wall = true;
        int size = field.getSIZE();
        //char symbol = field.getFieldPoint(row, column);
        if (row < 0 || row >= size || column < 0 || column >= size || field.getFieldPoint(row, column) == field.getBODY()) {
            wall = false;
        }
        return wall;
    }

    private boolean checkFood(Coordinates coordinates, Field field) {
        int row = coordinates.getX();
        int column = coordinates.getY();
        boolean food = false;
        char symbol = field.getFieldPoint(row, column);
        if (symbol == field.getAPPLE()) {
            food = true;
        }
        return food;
    }

    private char inputCheck() {
        Scanner sc = new Scanner(System.in);
        boolean check = false;
        char move = 'O';
        while (!check) {
            move = sc.nextLine().charAt(0);
            if (move == 'W' || move == 'S' || move == 'A' || move == 'D') {
                check = true;
            } else {
                System.out.println("Please, use only WASD to control the Snake");
            }
        }
        return move;
    }
}
