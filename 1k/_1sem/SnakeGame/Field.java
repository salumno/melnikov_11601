import java.util.Random;

public class Field {

    private final int SIZE = 11;
    private char[][] field;
    private final char FIELD = '.';
    private final char BODY = 'z';
    private final char HEAD = '0';
    private final char APPLE = '*';

    public Field() {
        field = new char[SIZE][SIZE];
        vanish();
    }

    public char getFieldPoint(int x, int y) {
        return field[x][y];
    }

    public int getSIZE() {
        return SIZE;
    }

    public char getAPPLE() {
        return APPLE;
    }

    public char getBODY() {
        return BODY;
    }

    public void vanish() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] != APPLE) {
                    field[i][j] = FIELD;
                }
            }
        }
    }

    public void render(Snake snake) {
        for (int i = 0; i < snake.getLength(); i++) {
            int tempX  = snake.snakePoint(i).getX();
            int tempY = snake.snakePoint(i).getY();
            if (i == 0) {
                field[tempX][tempY] = HEAD;
            } else {
                field[tempX][tempY] = BODY;
            }
        }
    }

    public void render(Food food, Snake snake) {
        Random r = new Random();
        boolean pointCheck = false;
        while (!pointCheck) {
            pointCheck = true;
            food.setX(r.nextInt(SIZE - 1));
            food.setY(r.nextInt(SIZE - 1));
            for (int i = 0; i < snake.getLength() && pointCheck; i++) {
                int snakeX = snake.snakePoint(i).getX();
                int snakeY = snake.snakePoint(i).getY();
                if (food.getX() == snakeX && food.getY() == snakeY) {
                    pointCheck = false;
                }
            }
        }
        field[food.getX()][food.getY()] = APPLE;
    }

    public void show() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }
    }
}
