public class Snake {

    private Coordinates[] snake;
    private int length;

    public Snake(int size) {
        length = 4;
        int row = size / 2;
        int column = row;
        snake = new Coordinates[size * size];
        for (int i = 0; i < length; i++) {
            snake[i] = new Coordinates();
            snake[i].setX(row);
            snake[i].setY(column);
            column -= 1;
        }
    }

    public int getLength() {
        return length;
    }

    public Coordinates snakePoint(int i) {
        return snake[i];
    }

    public void snakeMove(Coordinates coordinates) {
        if (!(snake[0].getX() == coordinates.getX() && snake[0].getY() == coordinates.getY())) {
            for (int i = length - 1; i > 0; i--) {
                snake[i].setX(snake[i - 1].getX());
                snake[i].setY(snake[i - 1].getY());
            }
            snake[0].setX(coordinates.getX());
            snake[0].setY(coordinates.getY());
        }
    }

    public void snakeAppleMove(Coordinates coordinates) {
        int tailRow = snake[length - 1].getX();
        int tailCol = snake[length - 1].getY();
        for (int i = length - 1; i > 0; i--) {
            snake[i].setX(snake[i - 1].getX());
            snake[i].setY(snake[i - 1].getY());
        }
        snake[0].setX(coordinates.getX());
        snake[0].setY(coordinates.getY());
        length += 1;
        snake[length - 1] = new Coordinates();
        snake[length - 1].setX(tailRow);
        snake[length - 1].setY(tailCol);
    }
}
