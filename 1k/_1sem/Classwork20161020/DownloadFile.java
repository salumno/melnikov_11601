/*
 *
 */

import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class DownloadFile {

    public void download(String fileName, String currURL) throws IOException {
        URL url = new URL(currURL);
        InputStream input = new BufferedInputStream(url.openStream()); // Соединились с url, в input попадают байты файла
        FileOutputStream output = new FileOutputStream(fileName); //Запись в вайл с именем HelloWorld
        byte[] buffer = new byte[1024]; //  
        int countBytes = 0;
        while ((countBytes = input.read(buffer)) != -1) { //read считывает из input stream байты данных и кладет в массив
            output.write(buffer, 0, countBytes); // Забирает данные, положенные в массив на предыдущем шаге, и записывает в fileName, который мы передали в FileOutputStream.
        }
        output.close();
    }
}