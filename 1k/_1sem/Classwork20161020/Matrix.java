/*

*/

public class Matrix {

    private double[][] matrix = new double[2][2];

    public Matrix(double[][] matrix) {
        this.matrix = matrix;
    }

    public Matrix (double a, double b, double c, double d) {
        matrix[0][0] = a;
        matrix[0][1] = b;
        matrix[1][0] = c;
        matrix[1][1] = d;
    }

    public Matrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = 0;
            }
        }
    }

    public String toString() {
        String s = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                s += matrix[i][j];
            }
            s += "\n";
        }
        return s;
    }

    public Matrix mult(Matrix m) {
        Matrix result = new Matrix();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int c = 0;
                for (int k = 0; k < matrix.length; k++) {
                    c += matrix[i][k]*m.matrix[k][j]; 
                }
                result.matrix[i][j] = c;
            }
        }
        return result;
    }
}