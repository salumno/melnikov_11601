/*
 *
 */

import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class DownloadCurrentFile {

    public static void main(String[] args) throws IOException {
        DownloadFile downloader = new DownloadFile();
        downloader.download("HelloWorld.java", "file:///home/salumno/MyWork/Melnikov_11601/HelloWorld.java");
    }
}