import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 3, Task 1
 */

public class Task1 {

    public static String day = "(0[1-9]|[1-2][0-9]|3[0-1])";
    public static String month = "(0[1-9]|1[0-2])";
    public static String year = "1(2(3[8-9]|[4-9][0-9])|([3-8][0-9]{2})|9([0-6][0-9]|7[0-7]))";
    public static String time = "([0-1][0-9]|2[0-3]):([0-5][0-9])";
    public static String pattern;

    public static boolean userDateCheck(String userDate) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userDate);
        return m.matches();
    }

    public static void main(String[] args) {
        String allTime = "(" + month + "/" + day + "/" + year + " " + time + ")";
        String year1237 = "(" + "(03/06/1237 (1[2-9]|2[0-3]):([0-5][0-9]))|((0[4-9]|1[0-2])/(0[7-9]|[1-2][0-9]|3[0-1])/1237 " + time + ")" + ")";
        String year1978 ="(" + "(02/27/1978 ((21:([0-2][0-9]|3[0-5]))|([0-1][0-9]|20):([0-5][0-9])))|(((01/" + day + ")|(02/(0[1-9]|1[0-9]|2[0-6])))/1978 " + time + ")" + ")";
        pattern = year1237 + "|" + allTime + "|" + year1978;
        Scanner sc = new Scanner(System.in);
        String userDate = sc.nextLine();
        System.out.println(userDateCheck(userDate));
    }
}
