import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 3, Task 2
 */

public class Task2 {

    public static String pattern = "0|((\\+|-)?0((\\.|,)([0-9]*[1-9]|[0-9]+\\([0-9]+\\))))|((\\+|-)?[1-9][0-9]*((\\.|,)([0-9]*[1-9]|[0-9]+\\([0-9]+\\)))?)";

    public static boolean userInputCheck(String userInput) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userInput);
        return m.matches();
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String userInput = sc.nextLine();
        System.out.println(userInputCheck(userInput));
    }
}
