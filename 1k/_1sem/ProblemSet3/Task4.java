import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 3, Task 4
 */

public class Task4 {

    public static String pattern = "[0-9]*(0|2|4|6|8)";

    public static boolean userInputCheck(int num) {
        String userInput = num + "";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userInput);
        return m.matches();
    }

    public static void main(String[] args) {
        Random r = new Random();
        int evenCount = 0;
        int count = 0;
        while (evenCount != 10) {
            int currentNumber = r.nextInt();
            count++;
            currentNumber = (currentNumber < 0) ? -1 * currentNumber : currentNumber;
            if (userInputCheck(currentNumber)) {
                evenCount++;
                System.out.println(currentNumber);
            }
        }
        System.out.println("In all = " + count);
    }
}
