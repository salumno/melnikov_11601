import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 3, Task 3
 */

public class Task3 {

    public static String pattern = "0+|1+|(101?)+|(010?)+";

    public static boolean userInputCheck(String userInput) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userInput);
        return m.matches();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        sc.nextLine();
        boolean[] checkArray = new boolean[size+1];
        Arrays.fill(checkArray, false);
        for (int i = 0; i < size; i++) {
            String inputString = sc.nextLine();
            if (userInputCheck(inputString)) {
                checkArray[i+1] = true;
            }
        }
        for (int i = 0; i < checkArray.length; i++) {
            if (checkArray[i]) {
                System.out.println(i);
            }
        }
    }
}
