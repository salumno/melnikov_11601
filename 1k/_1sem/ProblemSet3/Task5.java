import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 3, Task 5
 */

public class Task5 {

    public static String pattern = "[1-9]*(0|2|4|6|8){3}[0-9]*";

    public static boolean userInputCheck(int num) {
        String userInput = num + "";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userInput);
        return m.matches();
    }

    public static boolean userInputFindNumber(int num) {
        //boolean count = true;
        String userInput = num + "";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(userInput);
        return m.find();
    }

    public static void main(String[] args) {

        Random r = new Random();
        int numberCount = 0;
        int count = 0;
        while (numberCount != 10) {
            int currentNumber = r.nextInt(10000);
            count++;
            currentNumber = (currentNumber < 0) ? -1 * currentNumber : currentNumber;
            if (!userInputCheck(currentNumber)) {
                numberCount++;
                System.out.println(currentNumber);
            }
        }
        System.out.println("In all = " + count);
        System.out.println();
        numberCount = 0;
        count = 0;
        while (numberCount != 10) {
            int currentNumber = r.nextInt();
            count++;
            currentNumber = (currentNumber < 0) ? -1 * currentNumber : currentNumber;
            if (!userInputFindNumber(currentNumber)) {
                numberCount++;
                System.out.println(currentNumber);
            }
        }
        System.out.println("In all = " + count);

    }
}
