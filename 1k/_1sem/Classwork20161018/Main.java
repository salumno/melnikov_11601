/*
 * Melnikov Semen
 * OOP practice
 */

public class Main {

    public static void main(String [] args) {
    
        Vector2D v1 = new Vector2D(1,2);
        Vector2D v2 = new Vector2D(3,1);

        Vector2D v3 = v1.add(v2);
        System.out.println(v3);

        System.out.println(v1.length());
        System.out.println(v2.length());

        System.out.println(v1.scalarProd(v2));
        System.out.println(v2.scalarProd(v1));

        System.out.println(v1.cosAngle(v2));
    }

}