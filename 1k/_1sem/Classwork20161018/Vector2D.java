/*
 *
 */

public class Vector2D {

    private double x, y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public double length() {
        return Math.sqrt(x*x + y*y);
    }

    public Vector2D add(Vector2D v) {
        Vector2D result = new Vector2D(x + v.getX(), y + v.getY());
        return result;
    }

    public double scalarProd(Vector2D v) {
        return x * v.getX() + y * v.getY();
    }

    public double cosAngle(Vector2D v) {
        Vector2D v1 = new Vector2D(x,y);
        double cos = v1.scalarProd(v)/(v.length() * v1.length());
        return cos;
    }
    
    public String toString() {
        return "<" + x + ", " + y + ">";
    }
    
}