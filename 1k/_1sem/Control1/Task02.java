/*
 * Melnikov Semen
 * 11-601
 * Task 02(A)
 */

import java.util.Scanner;

public class Task02 {

    public static boolean digitCheck(int x) {
        boolean flag = true;
        while (x > 0 && flag) {
            int mod = x % 10;
            if (mod % 2 != 0) {
                flag = false;
            }
            x = x / 10;
        }
        return flag;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите 6 значений элементов массива");
        int size = 6;
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (digitCheck(arr[i])) {
                count += 1;
            }
        }
        if (count == 2) {
            System.out.println("TRUE");
        } else {
            System.out.println("FALSE");
        }
    }
}