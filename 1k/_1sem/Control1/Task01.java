/*
 * Melnikov Semen
 * 11-601
 * Task 01(K)
 */

import java.util.Scanner;

public class Task01 {

    public static void main(String[] args) {
        final double EPS = 10e-9;
        Scanner sc = new Scanner(System.in);
        System.out.println("Вводим x. |x| < 1");
        double x = sc.nextDouble();
        double up = 3 - 0.2 * x;
        double func = 0.6 * x - 0.03 * x * x;
        double pow = func * func;
        double upComp = 1;
        double downComp = 2;
        double currX = pow;
        double current = 1;
        double prevSum = 0;
        double currentSum =  current;
        do {
            prevSum = currentSum;
            current = upComp * currX / downComp;
            upComp *= (upComp + 2);
            downComp *= (downComp + 2);
            currX *= pow;
            currentSum = prevSum + current;
        } while (Math.abs(currentSum - prevSum) > EPS);
        double ans = up * currentSum;
        System.out.println(ans);
    }
}