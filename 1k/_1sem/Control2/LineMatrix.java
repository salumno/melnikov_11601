import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Semen Melnikov
 * 11-601
 * Control work 2
 * Variant 1
 * Task 2
 */

public class LineMatrix {

    public static int greatestCommonDivisor(int a, int b) {
        int result;
        if (a == 0) {
            result = b;
        } else {
            result = greatestCommonDivisor(b % a, a);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int m = sc.nextInt();
        int n = sc.nextInt();
        int[] array1 = new int[m];
        int[] array2 = new int[n];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = r.nextInt(5);
        }
        for (int i = 0; i < array2.length; i++) {
            array2[i] = r.nextInt(5);
        }
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));

        int bValue = greatestCommonDivisor(m, n);
        int aValue = m / bValue;
        int cValue = n / bValue;
        boolean flag = bValue != 1;
        if (aValue == 0 || cValue == 0) {
            flag = false;
        }

        if (flag) {
            System.out.println("YES!");
            System.out.println(aValue + "x" + bValue + " " + bValue + "x" + cValue);
            int sum = 0;
            int[] result = new int[aValue * cValue];
            int currentResultIndex = 0;
            for (int i = 0; i < m; i += bValue) {
                for (int j = 0; j < cValue; j++) {
                    int inx = i;
                    int k = j;
                    for (int l = 0; l < bValue; l++) {
                        sum += array1[inx] * array2[k];
                        k += cValue;
                        inx += 1;
                    }
                    result[currentResultIndex] = sum;
                    currentResultIndex += 1;
                    sum = 0;
                }
            }
            currentResultIndex = 0;
            for (int i = 0; i < aValue; i++) {
                for (int j = 0; j < cValue; j++) {
                    System.out.print(result[currentResultIndex] + " ");
                    currentResultIndex += 1;
                }
                System.out.println();
            }
        } else {
            System.out.println("NO! I cannot do it");
        }
    }
}
