import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Semen Melnikov
 * 11-601
 * Control work 2
 * Variant 1
 * Task 1
 */

public class Money {

    private long rubles;
    private byte kop;

    public Money() {
        this(0,(byte)0);
    }


    public Money(long r, byte k) {
        rubles = r;
        kop = k;
    }

    public Money(String num) throws InputMismatchException{
        if (!checkStringInput(num)) {
            throw new InputMismatchException();
        }
        rubles = convertStringtoRubles(num);
        kop = convertStringtoKop(num);
    }

    private long convertStringtoRubles(String num) {
        int i = 0;
        long tempRub = 0;
        while (num.charAt(i) != ',') {
            tempRub = (tempRub + Character.getNumericValue(num.charAt(i))) * 10;
            i++;
        }
        tempRub /= 10;
        return tempRub;
    }

    private byte convertStringtoKop(String num) {
        int i = num.length() - 2;
        int tempKop = 0;
        for (int j = i; j < num.length(); j++) {
            tempKop = (tempKop + Character.getNumericValue(num.charAt(j))) * 10;
        }
        tempKop /= 10;
        return (byte)tempKop;
    }

    private boolean checkStringInput(String num) {
        String pattern = "([1-9]\\d*|0),\\d{2}";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(num);
        return m.matches();
    }

    public String toString() {
        String s = rubles + ",";
        if (kop < 10) {
            s += "0";
        }
        s += kop;
        return s;
    }

    public boolean equals(Money num) {
        if (rubles > num.rubles) {
            return true;
        } else if (rubles < num.rubles) {
            return false;
        } else {
            if (kop >= num.kop) {
                return true;
            } else {
                return false;
            }
        }
    }

    public Money add(Money num) {
        long rubSum = rubles + num.rubles;
        int kopSum = kop + num.kop;
        while (kopSum >= 100) {
            kopSum -= 100;
            rubSum += 1;
        }
        Money result = new Money(rubSum, (byte)kopSum);
        return result;
    }

    public Money sub(Money num) throws Exception{
        byte tempKop = kop;
        long tempRub = rubles;
        if (!equals(num)) {
            throw new Exception("First sum must be more than second");
        }
        if (tempKop < num.kop) {
            tempKop += 100;
            tempRub -= 1;
        }
        long rubSub = tempRub - num.rubles;
        int kopSub = tempKop - num.kop;
        Money result = new Money(rubSub, (byte)kopSub);
        return result;
    }

    public long moneyDiv(Money num) {
        long temp1 = (rubles * 100) + kop;
        long temp2 = (num.rubles * 100) + num.kop;
        return temp1 / temp2;
    }

    public Money moneyNumberDiv (long num) {
        long temp = (rubles * 100) + kop;
        double rawResult = temp / (double)(num * 100);
        long rub = (int)rawResult;
        double kopTemp = (rawResult * 100) % 100;
        byte kop = (byte)kopTemp;
        Money result = new Money(rub, kop);
        return result;
    }

    public Money moneyNumberMult (long num) {
        long temp = (rubles * 100) + kop;
        double rawResult = temp * (double)(num);
        long rub = (long)(rawResult / 100);
        byte kop = (byte)(rawResult % 100);
        Money result = new Money(rub, kop);
        return result;
    }

    public static void main(String[] args) throws Exception {
        long rub = 35;
        byte kop = 50;
        Money m1 = new Money(rub, kop);
        System.out.println("m1 = " + m1);
        Money m2 = new Money(10, (byte)92);
        System.out.println("m2 = " + m2);
        Money m3 = m2.add(m1);
        System.out.println("m3 = " + m3);
        Money m4 = m1.sub(m2);
        System.out.println("m4 = " + m4);
        Money m5 = m2.moneyNumberDiv(5);
        System.out.println("m5 = " + m5);
        Money m6 = m2.moneyNumberMult(3);
        System.out.println("m6 = " + m6);
        long div = m6.moneyDiv(m5);
        System.out.println("m6 / m5 = " + div);
        Money m7 = new Money(new Scanner(System.in).nextLine());
        System.out.println("m7 = " + m7);

    }
}
