package task03;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 03
 */

public class RationalFraction {

    private int num;
    private int denom;

    public int getNum() {
        return num;
    }

    public int getDenom() {
        return denom;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDenom(int denom) {
        this.denom = denom;
    }

    public RationalFraction() {
        this(0,1);
    }

    public RationalFraction(int num, int denom) {
        this.num = num;
        this.denom = denom;
    }

    public void reduce() {
        int n = 2;
        while (n <= Math.abs(num)) {
            if (num % n == 0 && denom % n == 0) {
                num /= n;
                denom /= n;
            } else {
                n += 1;
            }
        }
        if (num < 0 && denom < 0) {
            num = Math.abs(num);
        } else if (num > 0 && denom < 0 || num < 0 && denom > 0) {
            num = (-1)*Math.abs(num);
        }
        denom = Math.abs(denom);
    }

    public String toString() {
        if (denom == 1) {
            return num + "";
        } else if (num == 0) {
            return "0";
        } else {
            return num + "/" + denom;
        }
    }

    public RationalFraction add(RationalFraction rf) {
        RationalFraction result = new RationalFraction();
        result.setNum(num * rf.getDenom() + denom * rf.getNum());
        result.setDenom(denom * rf.getDenom());
        result.reduce();
        return result;
    }

    public void add2(RationalFraction rf) {
        num = num * rf.getDenom() + denom * rf.getNum();
        denom = denom * rf.getDenom();
        reduce();
    }

    public RationalFraction sub(RationalFraction rf) {
        RationalFraction result = new RationalFraction();
        result.setNum(num * rf.getDenom() - denom * rf.getNum());
        result.setDenom(denom * rf.getDenom());
        result.reduce();
        return result;
    }

    public void sub2(RationalFraction rf) {
        num = num * rf.getDenom() - denom * rf.getNum();
        denom = denom * rf.getDenom();
        reduce();
    }

    public RationalFraction mult(RationalFraction rf) {
        RationalFraction result = new RationalFraction();
        result.setNum(num * rf.getNum());
        result.setDenom(denom * rf.getDenom());
        result.reduce();
        return result;
    }

    public void mult2(RationalFraction rf) {
        num = num * rf.getNum();
        denom = denom * rf.getDenom();
        reduce();
    }

    public RationalFraction div(RationalFraction rf) {
        RationalFraction result = new RationalFraction();
        result.setNum(num * rf.getDenom());
        result.setDenom(denom * rf.getNum());
        result.reduce();
        return result;
    }

    public void div2(RationalFraction rf) {
        num = num * rf.getDenom();
        denom = denom * rf.getNum();
        reduce();
    }

    public double value() {
        return (double)num / denom;
    }

    public boolean equals(RationalFraction rf) {
        boolean flag = false;
        rf.reduce();
        if (num == rf.getNum() && denom == rf.denom) {
            flag = true;
        }
        return flag;
    }

    public int numberPart() {
        return num / denom;
    }

    public static void main(String[] args) {
        RationalFraction rf = new RationalFraction(3, -6);
        System.out.println(rf);
        rf.reduce();
        System.out.println(rf);
        RationalFraction rf1 = rf.add(new RationalFraction(-2, 1));
        System.out.println(rf1);
        rf1.add2(new RationalFraction(-5, 2));
        System.out.println(rf1);
        rf1.sub2(new RationalFraction(-2, 4));
        System.out.println(rf1);
        rf1.mult2(new RationalFraction(-2, 3));
        System.out.println(rf1);
        RationalFraction rf2 = rf1.div(new RationalFraction(4, 2));
        System.out.println(rf2);
        rf1.div2(new RationalFraction(1, 2));
        System.out.println(rf1);
        System.out.println(rf2.value());
        System.out.println(rf2.equals(new RationalFraction(6, 4)));
        System.out.println(rf2.numberPart());
    }
}
