package task09;

import task04.ComplexNumber;
import task07.ComplexVector2D;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 09
 */

public class ComplexMatrix2x2 {

    private ComplexNumber[][] matrix = new ComplexNumber[2][2];

    public ComplexMatrix2x2() {
        this(new ComplexNumber());
    }

    public ComplexMatrix2x2(ComplexNumber cn) {
        this(cn, cn, cn, cn);
    }

    public ComplexMatrix2x2(ComplexNumber cn1, ComplexNumber cn2, ComplexNumber cn3, ComplexNumber cn4) {
        matrix[0][0] = cn1;
        matrix[0][1] = cn2;
        matrix[1][0] = cn3;
        matrix[1][1] = cn4;
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 cm) {
        ComplexMatrix2x2 result = new ComplexMatrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result.matrix[i][j] = matrix[i][j].add(cm.matrix[i][j]);
            }
        }
        return result;
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 cm) {
        ComplexMatrix2x2 result = new ComplexMatrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                ComplexNumber c = new ComplexNumber();
                for (int k = 0; k < matrix.length; k++) {
                    ComplexNumber tempMult = matrix[i][k].mult(cm.matrix[k][j]);
                    c = c.add(tempMult);
                }
                result.matrix[i][j] = c;
            }
        }
        return result;
    }

    public ComplexNumber det() {
        ComplexNumber fstAdd = matrix[0][0].mult(matrix[1][1]);
        ComplexNumber scdAdd = matrix[0][1].mult(matrix[1][0]);
        return fstAdd.sub(scdAdd);
    }

    ComplexVector2D multVector(ComplexVector2D cv) {
        ComplexVector2D result = new ComplexVector2D();
        ComplexNumber[] temp = new ComplexNumber[2];
        temp[0] = cv.getX();
        temp[1] = cv.getY();
        ComplexNumber zero = new ComplexNumber();
        for (int i = 0; i < matrix.length; i++) {
            ComplexNumber c = new ComplexNumber();
            for (int j = 0; j < matrix[i].length; j++) {
                ComplexNumber tempMult = matrix[i][j].mult(temp[j]);
                c = c.add(tempMult);
            }
            if (result.getX().equals(zero)) {
                result.setX(c);
            } else {
                result.setY(c);
            }
        }
        return result;
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result += matrix[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }

    public static void main(String[] args) {
        ComplexMatrix2x2 cm = new ComplexMatrix2x2();
        System.out.println(cm);
        ComplexNumber cn = new ComplexNumber(1, 2);
        ComplexNumber cn2 = new ComplexNumber(2, 3);
        ComplexNumber cn3 = new ComplexNumber(3, 4);
        ComplexNumber cn4 = new ComplexNumber(4, 5);
        ComplexMatrix2x2 cm1 = new ComplexMatrix2x2(cn);
        System.out.println(cm1);
        ComplexMatrix2x2 cm2 = new ComplexMatrix2x2(cn, cn2, cn3, cn4);
        System.out.println(cm2);
        ComplexMatrix2x2 cm3 = cm1.add(cm2);
        System.out.println(cm3);
        ComplexMatrix2x2 cm4 = cm1.mult(cm2);
        System.out.println(cm4);
        ComplexNumber det = cm1.det();
        System.out.println(det);
        ComplexVector2D cv = new ComplexVector2D(cn, cn);
        ComplexVector2D cv1 = cm1.multVector(cv);
        System.out.println(cv1);
    }

}
