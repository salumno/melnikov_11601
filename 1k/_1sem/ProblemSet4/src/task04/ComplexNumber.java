package task04;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 04
 */

public class ComplexNumber {

    private double re;
    private double im;

    public double getRe() {
        return re;
    }

    public void setRe(double re) {
        this.re = re;
    }

    public double getIm() {
        return im;
    }

    public void setIm(double im) {
        this.im = im;
    }

    public ComplexNumber() {
        this(0, 0);
    }

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public ComplexNumber add(ComplexNumber cn) {
        ComplexNumber result = new ComplexNumber();
        result.setRe(re + cn.getRe());
        result.setIm(im + cn.getIm());
        return  result;
    }

    public void add2(ComplexNumber cn) {
        re += cn.getRe();
        im += cn.getIm();
    }

    public ComplexNumber sub(ComplexNumber cn) {
        ComplexNumber result = new ComplexNumber();
        result.setRe(re - cn.getRe());
        result.setIm(im - cn.getIm());
        return  result;
    }

    public void sub2(ComplexNumber cn) {
        re -= cn.getRe();
        im -= cn.getIm();
    }

    public ComplexNumber multNumber(double scalar) {
        ComplexNumber result = new ComplexNumber();
        result.setRe(re * scalar);
        result.setIm(im * scalar);
        return  result;
    }

    public void multNumber2(double scalar) {
        re *= scalar;
        im *= scalar;
    }

    public ComplexNumber mult(ComplexNumber cn) {
        ComplexNumber result = new ComplexNumber();
        double tempRe = re;
        double tempIm = im;
        result.setRe(tempRe * cn.getRe() - tempIm * cn.getIm());
        result.setIm(tempIm * cn.getRe() + tempRe * cn.getIm());
        return  result;
    }

    public void mult2(ComplexNumber cn) {
        double tempRe = re;
        double tempIm = im;
        re = tempRe * cn.getRe() - tempIm * cn.getIm();
        im = tempIm * cn.getRe() + tempRe * cn.getIm();
    }

    public ComplexNumber div(ComplexNumber cn) {
        ComplexNumber result = new ComplexNumber();
        double tempRe = re;
        double tempIm = im;
        result.setRe((tempRe * cn.getRe() + tempIm * cn.getIm())/(cn.getRe() * cn.getRe() + cn.getIm() * cn.getIm()));
        result.setIm((tempIm * cn.getRe() - tempRe * cn.getIm())/(cn.getRe() * cn.getRe() + cn.getIm() * cn.getIm()));
        return  result;
    }

    public void div2(ComplexNumber cn) {
        double tempRe = re;
        double tempIm = im;
        re = (tempRe * cn.getRe() + tempIm * cn.getIm())/(cn.getRe() * cn.getRe() + cn.getIm() * cn.getIm());
        im = (tempIm * cn.getRe() - tempRe * cn.getIm())/(cn.getRe() * cn.getRe() + cn.getIm() * cn.getIm());
    }

    public double length() {
        return Math.sqrt(re*re + im*im);
    }

    public double arg() {
        if (re < 0) {
            return Math.atan(im / re) + Math.PI;
        } else {
            return Math.atan(im / re);
        }
    }

    public ComplexNumber pow (double p) {
        ComplexNumber result = new ComplexNumber();
        result.setRe(Math.pow(length(), p) * Math.cos(p*arg()));
        result.setIm(Math.pow(length(), p) * Math.sin(p*arg()));
        return result;
    }

    public boolean equals(ComplexNumber cn) {
        boolean flag = false;
        if (re == cn.getRe() && im == cn.getIm()) {
            flag = true;
        }
        return flag;
    }

    public String toString() {
        if (re == 0) {
            return im + "*i";
        }
        if (im == 0) {
            return re + "";
        }
        if (im > 0) {
            return re + " + " + im + "*i";
        } else {
            return re + " - " + Math.abs(im) + "*i";
        }

    }

    public static void main(String[] args) {
        ComplexNumber cm = new ComplexNumber();
        System.out.println(cm);
        cm.add2(new ComplexNumber(2, 3));
        System.out.println(cm);
        ComplexNumber cm1 = cm.add(new ComplexNumber(-4, -4));
        System.out.println(cm1);
        cm1.sub2(new ComplexNumber(-4, 3));
        System.out.println(cm1);
        cm1.multNumber2(2);
        System.out.println(cm1);
        cm1.mult2(new ComplexNumber(2, 2));
        System.out.println(cm1);
        cm1.div2(new ComplexNumber(2, 1));
        System.out.println(cm1);
        System.out.println(cm1.length());
        System.out.println(cm1.arg());
        ComplexNumber cnp = new ComplexNumber(8, -8);
        System.out.println(cnp.pow(3));
        System.out.println(cm1.equals(new ComplexNumber(8, -8)));
    }
}
