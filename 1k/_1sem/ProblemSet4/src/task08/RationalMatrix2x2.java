package task08;

import task03.RationalFraction;
import task06.RationalVector2D;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 08
 */

public class RationalMatrix2x2 {

    private RationalFraction[][] matrix = new RationalFraction[2][2];

    public RationalMatrix2x2() {
        this(new RationalFraction());
    }

    public RationalMatrix2x2(RationalFraction rf) {
        this(rf, rf, rf, rf);
        /*for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = rf;
            }
        }*/
    }

    public RationalMatrix2x2 (RationalFraction rf1, RationalFraction rf2, RationalFraction rf3, RationalFraction rf4) {
        matrix[0][0] = rf1;
        matrix[0][1] = rf2;
        matrix[1][0] = rf3;
        matrix[1][1] = rf4;
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 rm) {
        RationalMatrix2x2 result = new RationalMatrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result.matrix[i][j] = matrix[i][j].add(rm.matrix[i][j]);
            }
        }
        return result;
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 rm) {
        RationalMatrix2x2 result = new RationalMatrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                RationalFraction c = new RationalFraction();
                for (int k = 0; k < matrix.length; k++) {
                    RationalFraction tempMult = matrix[i][k].mult(rm.matrix[k][j]);
                    c = c.add(tempMult);
                }
                result.matrix[i][j] = c;
            }
        }
        return result;
    }

    public RationalFraction det() {
        RationalFraction fstAdd = matrix[0][0].mult(matrix[1][1]);
        RationalFraction scdAdd = matrix[0][1].mult(matrix[1][0]);
        return fstAdd.sub(scdAdd);
    }

    RationalVector2D multVector(RationalVector2D rv) {
        RationalVector2D result = new RationalVector2D();
        RationalFraction[] temp = new RationalFraction[2];
        temp[0] = rv.getX();
        temp[1] = rv.getY();
        RationalFraction zero = new RationalFraction();
        for (int i = 0; i < matrix.length; i++) {
            RationalFraction c = new RationalFraction();
            for (int j = 0; j < matrix[i].length; j++) {
                RationalFraction tempMult = matrix[i][j].mult(temp[j]);
                c = c.add(tempMult);
            }
            if (result.getX().equals(zero)) {
                result.setX(c);
            } else {
                result.setY(c);
            }
        }
        return result;
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result += matrix[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }

    public static void main(String[] args) {
        RationalMatrix2x2 rm = new RationalMatrix2x2();
        System.out.println(rm);
        RationalFraction rf1 = new RationalFraction(1, 2);
        RationalFraction rf2 = new RationalFraction(3, 2);
        RationalFraction rf3 = new RationalFraction(2, 3);
        RationalFraction rf4 = new RationalFraction(2, 4);
        RationalMatrix2x2 rm1 = new RationalMatrix2x2(rf4);
        System.out.println(rm1);
        RationalMatrix2x2 rm2 = new RationalMatrix2x2(rf1, rf2, rf3, rf4);
        System.out.println(rm2);
        RationalMatrix2x2 rm3 = rm1.add(rm2);
        System.out.println(rm3);
        RationalMatrix2x2 rm4 = rm1.mult(rm2);
        System.out.println(rm4);
        System.out.println(rm4.det());
        RationalVector2D rv = new RationalVector2D(rf1, rf2);
        RationalVector2D rv1 = rm1.multVector(rv);
        System.out.println(rv1);
    }
}
