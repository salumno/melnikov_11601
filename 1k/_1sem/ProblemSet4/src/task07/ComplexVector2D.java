package task07;

import task04.ComplexNumber;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 07
 */

public class ComplexVector2D {

    private ComplexNumber x;
    private ComplexNumber y;

    public ComplexNumber getX() {
        return x;
    }

    public ComplexNumber getY() {
        return y;
    }

    public void setX(ComplexNumber x) {
        this.x = x;
    }

    public void setY(ComplexNumber y) {
        this.y = y;
    }

    public ComplexVector2D() {
        this(new ComplexNumber(), new ComplexNumber());
    }

    public ComplexVector2D (ComplexNumber x, ComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public ComplexVector2D add(ComplexVector2D cv) {
        ComplexVector2D result = new ComplexVector2D();
        result.setX(x.add(cv.getX()));
        result.setY(y.add(cv.getY()));
        return result;
    }

    public ComplexNumber scalarProduct(ComplexVector2D cv) {
        ComplexNumber coord1 = x.mult(cv.getX());
        ComplexNumber coord2 = y.mult(cv.getY());
        ComplexNumber result = coord1.add(coord2);
        return result;
    }

    public boolean equals(ComplexVector2D cv) {
        boolean flag = false;
        if (x.equals(cv.getX()) && y.equals(cv.getY())) {
            flag = true;
        }
        return flag;
    }

    public String toString() {
        return "<" + x + ", " + y + ">";
    }

    public static void main(String[] args) {
        ComplexNumber cn1 = new ComplexNumber(2, 2);
        ComplexNumber cn2 = new ComplexNumber(2, 1);
        ComplexVector2D cv = new ComplexVector2D(cn1, cn2);
        System.out.println(cv);
        ComplexVector2D cv2 = new ComplexVector2D(cn1, cn2);
        System.out.println(cv2);
        ComplexVector2D cv3 = cv.add(cv2);
        System.out.println(cv3);
        ComplexNumber scalar = cv2.scalarProduct(cv3);
        System.out.println(scalar);
        System.out.println(cv.equals(cv2));
    }
}
