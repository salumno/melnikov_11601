package task01;

import java.util.Random;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 01
 */

public class Teacher {

    private String name;
    private String object;

    public Teacher (String name, String object) {
        this.name = name;
        this.object = object;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public void studentRate(Student student) {
        Random r = new Random();
        int rate = r.nextInt(4) + 2;
        String  mark;
        switch (rate) {
            case 2:
                mark = "неудовлетворительно";
                break;
            case 3:
                mark = "удовлетворительно";
                break;
            case 4:
                mark = "хорошо";
                break;
            default:
                mark = "отлично";
                break;
        }
        String st = "Преподаватель " + name + " оценил студента с именем " + student.getName() + " по предмету " + object + " на оценку " + mark + ".";
        System.out.println(st);
    }
}
