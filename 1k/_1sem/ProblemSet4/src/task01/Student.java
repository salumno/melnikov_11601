package task01;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 01
 */

public class Student {

    private String name;
    private String groupNumber;

    public Student (String name, String groupNumber) {
        this.name = name;
        this.groupNumber = groupNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

}
