package task01;

import java.util.Scanner;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 01
 */

public class TestClass {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя студента");
        String studentName = sc.nextLine();
        System.out.println("Введите номер группы студента");
        String groupNum = sc.nextLine();
        Student student = new Student(studentName, groupNum);
        student.setName("Ермолай");
        System.out.println("Введите имя преподавателя");
        String teacherName = sc.nextLine();
        System.out.println("Введите название предмета");
        String object = sc.nextLine();
        Teacher teacher = new Teacher(teacherName, object);
        teacher.studentRate(student);
    }
}
