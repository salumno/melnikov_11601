package task05;

import task02.Vector2D;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 05
 */

public class Matrix2x2 {

    private double[][] matrix = new double[2][2];

    public Matrix2x2() {
        this(0);
    }

    public Matrix2x2(double num) {
        this(num, num, num, num);
    }

    public  Matrix2x2(double[][] matrix) {
        this.matrix = matrix;
    }

    public Matrix2x2 (double a, double b, double c, double d) {
        matrix[0][0] = a;
        matrix[0][1] = b;
        matrix[1][0] = c;
        matrix[1][1] = d;
    }

    public Matrix2x2 add(Matrix2x2 m) {
        Matrix2x2 result = new Matrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result.matrix[i][j] = matrix[i][j] + m.matrix[i][j];
            }
        }
        return result;
    }

    public void add2(Matrix2x2 m) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] += m.matrix[i][j];
            }
        }
    }

    public Matrix2x2 sub(Matrix2x2 m) {
        Matrix2x2 result = new Matrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result.matrix[i][j] = matrix[i][j] - m.matrix[i][j];
            }
        }
        return result;
    }

    public void sub2(Matrix2x2 m) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] -= m.matrix[i][j];
            }
        }
    }

    public Matrix2x2 multNumber(double num) {
        Matrix2x2 result = new Matrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result.matrix[i][j] = matrix[i][j] * num;
            }
        }
        return result;
    }

    public void multNumber2(double num) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] *= num;
            }
        }
    }

    public Matrix2x2 mult(Matrix2x2 m) {
        Matrix2x2 result = new Matrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int c = 0;
                for (int k = 0; k < matrix.length; k++) {
                    c += matrix[i][k]*m.matrix[k][j];
                }
                result.matrix[i][j] = c;
            }
        }
        return result;
    }

    public void mult2(Matrix2x2 m) {
        double[][] temp = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int c = 0;
                for (int k = 0; k < matrix.length; k++) {
                    c += matrix[i][k]*m.matrix[k][j];
                }
                temp[i][j] = c;
            }
        }
        matrix = temp;
    }

    public double det() {
        return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
    }

    public void transpon() {
        double[][] temp = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                temp[j][i] = matrix[i][j];
            }
        }
        matrix = temp;
    }

    public Matrix2x2 inverseMatrix() {
        Matrix2x2 result = new Matrix2x2();
        if (det() == 0) {
            return result;
        }
        result.matrix[0][0] = matrix[1][1] / det();
        result.matrix[0][1] = -matrix[0][1] / det();
        result.matrix[1][0] = -matrix[1][0] / det();
        result.matrix[1][1] = matrix[0][0] / det();
        return result;
    }

    public Vector2D multiVector(Vector2D v) {
        Vector2D result = new Vector2D();
        double[] temp = new double[2];
        temp[0] = v.getX();
        temp[1] = v.getY();
        for (int i = 0; i < matrix.length; i++) {
            int c = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                c += matrix[i][j] * temp[j];
            }
            if (result.getX() == 0) {
                result.setX(c);
            } else {
                result.setY(c);
            }
        }
        return result;
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result += matrix[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }

    public static void main(String[] args) {
        Matrix2x2 matrix2x2 = new Matrix2x2(1, 2, 3, 4);
        System.out.println(matrix2x2);
        Matrix2x2 m = matrix2x2.add(new Matrix2x2(1, 2, 3, 4));
        System.out.println(m);
        matrix2x2.add2(new Matrix2x2(1, 2, 3, 4));
        System.out.println(matrix2x2);
        matrix2x2.sub2(new Matrix2x2(2, 2, 2, 2));
        System.out.println(matrix2x2);
        matrix2x2.multNumber2(5);
        System.out.println(matrix2x2);
        Matrix2x2 m2 = new Matrix2x2(1, 2, 3, 4);
        Matrix2x2 m3 = m2.mult(m2);
        System.out.println(m3);
        System.out.println(m2);
        m2.mult2(new Matrix2x2(1, 2, 3, 4));
        System.out.println(m2);
        System.out.println(m2.det());
        m2.transpon();
        System.out.println(m2);
        Matrix2x2 m4 = m2.inverseMatrix();
        System.out.println(m4);
        Vector2D vector2D = new Vector2D(1, 2);
        Matrix2x2 m5 = new Matrix2x2(1, 2, 3, 4);
        Vector2D result = m5.multiVector(vector2D);
        System.out.println(result);
    }

}
