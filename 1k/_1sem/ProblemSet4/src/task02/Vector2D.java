package task02;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 02
 */

public class Vector2D {

    private double x,y;

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Vector2D() {
        this(0,0);
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D vector2D) {
        Vector2D result = new Vector2D(x + vector2D.getX(), y + vector2D.getY());
        return result;
    }

    public void add2(Vector2D vector2D) {
        x += vector2D.getX();
        y += vector2D.getY();
    }

    public Vector2D sub(Vector2D vector2D) {
        Vector2D result = new Vector2D(x - vector2D.getX(), y - vector2D.getY());
        return result;
    }

    public void sub2(Vector2D vector2D) {
        x -= vector2D.getX();
        y -= vector2D.getY();
    }

    public Vector2D mult(double scalar) {
        Vector2D result = new Vector2D(x * scalar, y * scalar);
        return result;
    }

    public void mult2(double scalar) {
        x *= scalar;
        y *= scalar;
    }

    public String toString() {
        return "<" + x + ", " + y + ">";
    }

    public double length() {
        return Math.sqrt(x*x + y*y);
    }

    public double scalarProd(Vector2D vector2D) {
        return x * vector2D.getX() + y * vector2D.getY();
    }

    public double cos(Vector2D vector2D) {
        double cos = scalarProd(vector2D)/(vector2D.length() * length());
        return cos;
    }

    public boolean equals(Vector2D vector2D) {
        boolean flag = false;
        if (x == vector2D.getX() && y == vector2D.getY()) {
            flag = true;
        }
        return flag;
    }

    public static void main(String[] args) {
        Vector2D vector2D = new Vector2D();
        System.out.println(vector2D);
        vector2D.add2(new Vector2D(2, 3));
        System.out.println(vector2D);
        vector2D.mult2(2);
        System.out.println(vector2D);
        Vector2D v1 = vector2D.add(new Vector2D(2, 3));
        System.out.println(v1);
        System.out.println(new Vector2D(0, 2).cos(new Vector2D(0, 2)));
        System.out.println(vector2D.equals(new Vector2D(4, 6)));
    }

}
