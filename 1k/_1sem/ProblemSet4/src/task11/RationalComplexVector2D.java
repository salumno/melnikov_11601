package task11;

import task03.RationalFraction;
import task10.RationalComplexNumber;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 11
 */
public class RationalComplexVector2D {

    private RationalComplexNumber x;
    private RationalComplexNumber y;

    public RationalComplexNumber getX() {
        return x;
    }

    public RationalComplexNumber getY() {
        return y;
    }

    public void setX(RationalComplexNumber x) {
        this.x = x;
    }

    public void setY(RationalComplexNumber y) {
        this.y = y;
    }

    public RationalComplexVector2D() {
        this(new RationalComplexNumber(), new RationalComplexNumber());
    }

    public RationalComplexVector2D(RationalComplexNumber rcn1, RationalComplexNumber rcn2) {
        this.x = rcn1;
        this.y = rcn2;
    }

    public RationalComplexVector2D add(RationalComplexVector2D rcv) {
        RationalComplexVector2D result = new RationalComplexVector2D();
        result.setX(x.add(rcv.getX()));
        result.setY(y.add(rcv.getY()));
        return result;
    }

    public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv) {
        RationalComplexNumber coord1 = x.mult(rcv.getX());
        RationalComplexNumber coord2 = y.mult(rcv.getY());
        RationalComplexNumber result = coord1.add(coord2);
        return result;
    }

    public String toString() {
        return "<" + x + ", " + y + ">";
    }

    public static void main(String[] args) {
        RationalComplexVector2D rcv = new RationalComplexVector2D();
        System.out.println(rcv);
        RationalFraction rf1 = new RationalFraction(1, 2);
        RationalFraction rf2 = new RationalFraction(5, -6);
        RationalComplexNumber rcn1 = new RationalComplexNumber(rf1, rf2);
        RationalComplexNumber rcn2 = new RationalComplexNumber(rf1, rf2);
        RationalComplexVector2D rcv1 = new RationalComplexVector2D(rcn1, rcn2);
        System.out.println(rcv1);
        RationalComplexVector2D rcv2 = new RationalComplexVector2D(rcn1, rcn2);
        System.out.println(rcv2);
        RationalComplexVector2D rcv3 = rcv1.add(rcv2);
        System.out.println(rcv3);
        System.out.println(rcv1.scalarProduct(rcv2));
    }
}
