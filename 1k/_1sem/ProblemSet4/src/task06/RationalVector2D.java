package task06;

import task03.RationalFraction;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 06
 */
public class RationalVector2D {

    private RationalFraction x;
    private RationalFraction y;

    public RationalFraction getX() {
        return x;
    }

    public RationalFraction getY() {
        return y;
    }

    public void setX(RationalFraction x) {
        this.x = x;
    }

    public void setY(RationalFraction y) {
        this.y = y;
    }

    public RationalVector2D() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        this.x = new RationalFraction(x.getNum(), x.getDenom());
        this.y = new RationalFraction(y.getNum(), y.getDenom());
    }

    public RationalVector2D add(RationalVector2D rv) {
        RationalVector2D result = new RationalVector2D();
        result.setX(x.add(rv.getX()));
        result.setY(y.add(rv.getY()));
        return result;
    }

    public double length() {
        RationalFraction coord1 = x.mult(x);
        RationalFraction coord2 = y.mult(y);
        RationalFraction l = coord1.add(coord2);
        return Math.sqrt(l.value());
    }

    public RationalFraction scalarProduct(RationalVector2D rv) {
        RationalFraction x1 = x.mult(rv.getX());
        RationalFraction y1 = y.mult(rv.getY());
        RationalFraction result = x1.add(y1);
        return result;
    }

    public boolean equals(RationalVector2D rv) {
        boolean flag = false;
        if (x.equals(rv.getX()) && y.equals(rv.getY())) {
            flag = true;
        }
        return flag;
    }

    public String toString() {
        return "<" + x + ", " + y + ">";
    }

    public static void main(String[] args) {
        RationalFraction x = new RationalFraction(1, 2);
        RationalFraction y = new RationalFraction(3, 2);
        RationalVector2D rv = new RationalVector2D(x,y);
        RationalVector2D rv1 = new RationalVector2D(x,y);
        System.out.println(rv);
        RationalVector2D rv2 = rv.add(rv1);
        System.out.println(rv2);
        System.out.println(rv2.length());
        System.out.println(rv2);
        System.out.println(rv);
        RationalFraction sp = rv2.scalarProduct(rv);
        System.out.println(sp);
        System.out.println(rv2.equals(rv));
    }
}
