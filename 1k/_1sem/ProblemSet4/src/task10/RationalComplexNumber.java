package task10;

import task03.RationalFraction;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 10
 */

public class RationalComplexNumber {

    private RationalFraction re;
    private RationalFraction im;

    public RationalFraction getRe() {
        return re;
    }

    public RationalFraction getIm() {
        return im;
    }

    public void setRe(RationalFraction re) {
        this.re = re;
    }

    public void setIm(RationalFraction im) {
        this.im = im;
    }

    public RationalComplexNumber() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalComplexNumber(RationalFraction re, RationalFraction im) {
        this.re = re;
        this.im = im;
    }

    public RationalComplexNumber add(RationalComplexNumber rcn) {
        RationalComplexNumber result = new RationalComplexNumber();
        result.setRe(re.add(rcn.getRe()));
        result.setIm(im.add(rcn.getIm()));
        return result;
    }

    public RationalComplexNumber sub(RationalComplexNumber rcn) {
        RationalComplexNumber result = new RationalComplexNumber();
        result.setRe(re.sub(rcn.getRe()));
        result.setIm(im.sub(rcn.getIm()));
        return result;
    }

    public RationalComplexNumber mult(RationalComplexNumber rcn) {
        RationalComplexNumber result = new RationalComplexNumber();
        RationalFraction tempRe = re;
        RationalFraction tempIm = im;
        result.setRe(tempRe.mult(rcn.getRe()).sub(tempIm.mult(rcn.getIm())));
        result.setIm(tempIm.mult(rcn.getRe()).add(tempRe.mult(rcn.getIm())));
        return result;
    }

    public String toString() {
        if (re.value() == 0) {
            return im + "*i";
        }
        if (im.value() == 0) {
            return re + "";
        }
        if (im.value() > 0) {
            return re + " + " + im + "*i";
        } else {
            return re + "" + im + "*i";
        }
    }

    public static void main(String[] args) {
        RationalComplexNumber rcn = new RationalComplexNumber();
        System.out.println(rcn);
        RationalFraction rf1 = new RationalFraction(1,2);
        RationalFraction rf2 = new RationalFraction(5,-7);
        RationalComplexNumber rcn1 = new RationalComplexNumber(rf1, rf2);
        System.out.println(rcn1);
        RationalComplexNumber rcn2 = new RationalComplexNumber(rf1, rf2);
        System.out.println(rcn2);
        RationalComplexNumber rcn3 = rcn1.add(rcn2);
        System.out.println(rcn3);
        RationalComplexNumber rcn4 = rcn1.sub(rcn2);
        System.out.println(rcn4);
        RationalComplexNumber rcn5 = rcn1.mult(rcn2);
        System.out.println(rcn5);

    }
}
