package task12;

import task03.RationalFraction;
import task10.RationalComplexNumber;
import task11.RationalComplexVector2D;

/**
 * Melnikov Semen
 * 11-601
 * Problem Set 4, Task 12
 */

public class RationalComplexMatrix2x2 {

    private RationalComplexNumber[][] matrix = new RationalComplexNumber[2][2];

    public RationalComplexMatrix2x2() {
        this(new RationalComplexNumber());
    }

    public RationalComplexMatrix2x2(RationalComplexNumber rcn) {
        this(rcn, rcn, rcn, rcn);
    }

    public RationalComplexMatrix2x2(RationalComplexNumber rcn1, RationalComplexNumber rcn2, RationalComplexNumber rcn3, RationalComplexNumber rcn4) {
        matrix[0][0] = rcn1;
        matrix[0][1] = rcn2;
        matrix[1][0] = rcn3;
        matrix[1][1] = rcn4;
    }

    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 rcm) {
        RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result.matrix[i][j] = matrix[i][j].add(rcm.matrix[i][j]);
            }
        }
        return result;
    }

    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 rcm) {
        RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                RationalComplexNumber c = new RationalComplexNumber();
                for (int k = 0; k < matrix.length; k++) {
                    RationalComplexNumber tempMult = matrix[i][k].mult(rcm.matrix[k][j]);
                    c = c.add(tempMult);
                }
                result.matrix[i][j] = c;
            }
        }
        return result;
    }

    public RationalComplexNumber det() {
        RationalComplexNumber fstAdd = matrix[0][0].mult(matrix[1][1]);
        RationalComplexNumber scdAdd = matrix[0][1].mult(matrix[1][0]);
        return fstAdd.sub(scdAdd);
    }

    RationalComplexVector2D multVector(RationalComplexVector2D rcv) {
        RationalComplexVector2D result = new RationalComplexVector2D();
        RationalComplexNumber[] temp = new RationalComplexNumber[2];
        temp[0] = rcv.getX();
        temp[1] = rcv.getY();
        //RationalComplexNumber zero = new RationalComplexNumber();
        for (int i = 0; i < matrix.length; i++) {
            RationalComplexNumber c = new RationalComplexNumber();
            for (int j = 0; j < matrix[i].length; j++) {
                RationalComplexNumber tempMult = matrix[i][j].mult(temp[j]);
                c = c.add(tempMult);
            }
            if (result.getX().getRe().value() == 0) {
                result.setX(c);
            } else {
                result.setY(c);
            }
        }
        return result;
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result += matrix[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }

    public static void main(String[] args) {
        RationalComplexMatrix2x2 rcm = new RationalComplexMatrix2x2();
        System.out.print(rcm);
        RationalFraction rf1 = new RationalFraction(1,2);
        RationalFraction rf2 = new RationalFraction(5,7);
        RationalComplexNumber rcn1 = new RationalComplexNumber(rf1, rf2);
        RationalComplexNumber rcn2 = new RationalComplexNumber(rf2, rf2);
        RationalComplexNumber rcn3 = new RationalComplexNumber(rf2, rf1);
        RationalComplexNumber rcn4 = new RationalComplexNumber(rf1, rf1);
        RationalComplexMatrix2x2 rcm1 = new RationalComplexMatrix2x2(rcn1);
        System.out.println(rcm1);
        RationalComplexMatrix2x2 rcm2 = new RationalComplexMatrix2x2(rcn1, rcn2, rcn3, rcn4);
        System.out.println(rcm2);
        RationalComplexMatrix2x2 rcm3 = rcm1.add(rcm2);
        System.out.println(rcm3);
        RationalComplexMatrix2x2 rcm4 = rcm1.mult(rcm2);
        System.out.println(rcm4);
        System.out.println(rcm1.det());
        RationalComplexVector2D rcv = new RationalComplexVector2D(rcn1, rcn2);
        System.out.println(rcv);
        System.out.print(rcm2);
        RationalComplexVector2D rcv1 = rcm2.multVector(rcv);
        System.out.println(rcv1);
    }
}
