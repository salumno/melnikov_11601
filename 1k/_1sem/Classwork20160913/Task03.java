/*
Melnikov Semen 11-601. Task 03. Triangle
*/

import java.util.Scanner;

public class Task03 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = n;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                System.out.print("*");
            }
            k--;
            System.out.println();
        }
    }
}