/*
Melnikov Semen 11-601. Task 06. Circle
*/

import java.util.Scanner;

public class Task06 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int radius = sc.nextInt();
        int d = 2 * radius + 1;
        int x = radius + 1;
        int y = radius + 1;
        for (int i = 1; i <= d; i++) {
            for (int j = 1; j <= d; j++) {
                if ((i - x)*(i - x) + (j - y)*(j - y) <= radius*radius) {
                    System.out.print("0 ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}