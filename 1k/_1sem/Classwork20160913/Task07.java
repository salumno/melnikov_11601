/*
Melnikov Semen 11-601. Task 07. Yin and yang
*/

import java.util.Scanner;

public class Task07 {

    public static boolean inOutCircle (int i, int j, int x, int y, int radius) {
        return (i - x)*(i - x) + (j - y)*(j - y) <= radius*radius;
    }

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int radius = sc.nextInt();
        radius = (radius % 2 == 0) ? radius : radius + 1;
        int d = 2 * radius + 3;
        int x1 = radius + 2;
        int y1 = radius + 2;
        int x2 = x1 / 2;
        int y2 = y1;
        int radius1 = radius / 2;
        int x3 = 3 * x2;
        int y3 = y1;
        int radius2 = radius / 2;
        int radius3 = radius1 / 2;
        radius += 1;
        String black = "B ";
        String white = "- ";
        for (int i = 1; i <= d; i++) {
            for (int j = 1; j <= d; j++) {
                if (inOutCircle(i,j,x1,y1,radius)) {
                    if (i <= x1) {
                        if (inOutCircle(i,j,x2,y2,radius1)) {
                            if (inOutCircle(i,j,x2,y2,radius3)) {
                                System.out.print(white);
                            } else {
                                System.out.print(black);
                            }
                        } else {
                            if (j <= y1) {
                                System.out.print(black);
                            } else {
                                System.out.print(white);
                            }
                        }
                    } else {
                        if (inOutCircle(i,j,x3,y3,radius2)) {
                            if (inOutCircle(i,j,x3,y3,radius3)) {
                                System.out.print(black);
                            } else {
                                System.out.print(white);
                            }
                        } else {
                            if (j <= y1) {
                                System.out.print(black);
                            } else {
                                System.out.print(white);
                            }
                        }
                    }
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}