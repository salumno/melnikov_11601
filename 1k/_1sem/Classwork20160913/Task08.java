/*
Melnikov Semen 11-601. Task 08. Smile
*/

import java.util.Scanner;

public class Task08 {

    public static boolean inOutCircle (int i, int j, int x, int y, int radius) {
        return (i - x)*(i - x) + (j - y)*(j - y) <= radius*radius;
    }

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int radius = sc.nextInt();
        int eyeRadius = radius / 3;
        int d = 2 * radius + 1;
        int x = radius + 1;
        int y = radius + 1;
        int x1 = x - eyeRadius;
        int y1 = y - eyeRadius - 1;
        int x2 = x1;
        int y2 = y + eyeRadius + 1;
        String eye = "8 ";
        String mouth = "W ";
        String face = "- ";
        for (int i = 1; i <= d; i++) {
            for (int j = 1; j <= d; j++) {
                if (inOutCircle(i,j,x,y,radius)) {
                    if (inOutCircle(i,j,x1,y1,eyeRadius) || inOutCircle(i,j,x2,y2,eyeRadius)) {
                        System.out.print(eye);
                    } else {
                        if ((i >= x + eyeRadius + 1) & (i <= x + 2*eyeRadius) & (j >= y1) & (j <= y2)) {
                            System.out.print(mouth);
                        } else {
                            System.out.print(face);
                        }
                    }    
                } else {
                    System.out.print("0 ");
                }
            }
            System.out.println();
        }
    }
}