/*
Melnikov Semen 11-601. Task 04. Parallelogram
*/

import java.util.Scanner;

public class Task04 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int column = n;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < column - 1; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < n; j++) {
                System.out.print("*");
            }
            column--;
            System.out.println();
        }
    }
}