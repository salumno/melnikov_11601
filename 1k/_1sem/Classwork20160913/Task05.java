/*
Melnikov Semen 11-601. Task 05. Triforce
*/

import java.util.Scanner;

public class Task05 {

    public static void printOne(int upperLimit) {
        for (int j = 0; j < upperLimit; j++) {
            System.out.print("1");
        }
    }

    public static void printSpace(int upperLimit) {
        for (int j = 0; j < upperLimit; j++) {
            System.out.print(" ");
        }
    }

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        n = (n % 2 == 0) ? n + 1 : n;
        int height = (n + 1) / 2;
        int rightSpace = n;
        int currentDigit = 1;
        int insideSpace = n;
        int insideDigit = 1;
        for (int i = 1; i <= 2*height; i++) {
            printSpace(rightSpace);
            rightSpace--;
            if (i > height) {
                printOne(insideDigit);
                printSpace(insideSpace);
                printOne(insideDigit);
                insideDigit += 2;
                insideSpace -= 2;
            } else {
                printOne(currentDigit);
                currentDigit += 2;
            }
            System.out.println();
        }
    }
}