/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 77d
 */

import java.util.Scanner;

public class PS1_Task77D {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = sc.nextInt();
        double currSin = 0;
        double sum = 0;
        for (int i = 1; i <= n; i++) {
            currSin += Math.sin(i);
            sum += 1 / currSin;
        }
        System.out.println(sum);
    }
}