/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 84c
 */

import java.util.Scanner;

public class PS1_Task84C {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("n = ");
        double n = sc.nextInt();
        double currArg = Math.sin(x);
        double sum = currArg;
        for (int i = 2; i <= n; i++) {
            currArg = Math.sin(currArg);
            sum += currArg;
        }
        System.out.println(sum);
    }
}