/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 77b
 */

import java.util.Scanner;

public class PS1_Task77B {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = sc.nextInt();
        long ans = 1;
        for (int i = 1; i <= n; i++) {
            ans *= i;
        }
        System.out.println(ans);
    }
}