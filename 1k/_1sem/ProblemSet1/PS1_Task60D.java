/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 60d
 */

import java.util.Scanner;

public class PS1_Task60D {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        double ans = ((x * x + y * y <= 1) && (y >= -x) && (y >= x)) ? Math.sqrt(Math.abs(x*x - 1)) : y + x;
        System.out.println(ans);
    }
}