/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 77e
 */

import java.util.Scanner;

public class PS1_Task77E {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = sc.nextInt();
        double currSqrt = Math.sqrt(2);
        for (int i = 2; i <= n; i++) {
            currSqrt = Math.sqrt(2 + currSqrt);
        }
        System.out.println(currSqrt);
    }
}