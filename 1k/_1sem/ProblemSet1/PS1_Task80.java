/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 80
 */

import java.util.Scanner;

public class PS1_Task80 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        double pow = x * x;
        double currX = x;
        int fact = 1;
        int n = 1;
        int sign = 1;
        double ans = 0;
        for (int i = 0; i < 7; i++) {
        	ans += sign * currX / fact;
        	sign = (-1) * sign;
        	currX *= pow;
        	n += 2;
        	fact = fact * (n - 1) * n;
        }
        System.out.println(ans);
    }
}