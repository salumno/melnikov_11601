/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 84b
 */

import java.util.Scanner;

public class PS1_Task84B {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("n = ");
        double n = sc.nextInt();
        double currPow = x;
        double sum = Math.sin(currPow);
        for (int i = 2; i <= n; i++) {
            currPow *= x;
            sum += Math.sin(currPow);
        }
        System.out.println(sum);
    }
}