/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 14
 */

import java.util.Scanner;

public class PS1_Task14 {
    public static void main(String[] args) {
        final double G = 6.67408e-11;
        Scanner sc = new Scanner(System.in);
        System.out.println("Weight of the first object (kg.)");
        double w1 = sc.nextDouble();
        System.out.println("Weight of the second object  (kg.)");
        double w2 = sc.nextDouble();
        System.out.println("Distance between it (m.) ");
        double d = sc.nextDouble();
        double f = (G * w1 * w2)/(d * d);
        System.out.println(f);
    }
}