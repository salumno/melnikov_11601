/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 35b
 */

import java.util.Scanner;

public class PS1_Task35B {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("y = ");
        double y = sc.nextDouble();
        System.out.print("z = ");
        double z = sc.nextDouble();
        double sum = x + y + z/2;
        double comp = x * y * z;
        double ans = sum > comp ? comp : sum;
        ans = ans * ans + 1;
        System.out.println(ans);
    }
}