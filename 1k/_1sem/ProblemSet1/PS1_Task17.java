/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 17
 */

import java.util.Scanner;

public class PS1_Task17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("radius ( > 20) = ");
        double r = sc.nextDouble();
        double s = Math.PI * r * r - Math.PI * 400;
        System.out.println(Math.abs(s));
    }
}