/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 60b
 */

import java.util.Scanner;

public class PS1_Task60B {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        double ans = ((x * x + y * y <= 1) && (y <= 0.5*x)) ? -3 : (y * y);
        System.out.println(ans);
    }
}