/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 68c
 */

import java.util.Scanner;
import java.util.Arrays;

public class PS1_Task68C {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] digit = new int[10];
        Arrays.fill(digit, 0);
        System.out.print("n = ");
        String n = sc.nextLine();
        boolean flag = true;
        for (int i = 0; i < n.length(); i++) {
            digit[n.charAt(i) - '0'] += 1;
        }
        for (int i = 0; i < digit.length & flag; i++) {
            if (digit[i] > 1) {
                flag = false;
            }
        }
        System.out.println(flag);
    }
}