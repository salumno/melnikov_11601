/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 16
 */

import java.util.Scanner;

public class PS1_Task16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double l = sc.nextDouble();
        double s = (l * l) / (4 * Math.PI);
        System.out.println(s);
    }
}