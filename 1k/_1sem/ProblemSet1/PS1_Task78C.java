/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 78c
 */

import java.util.Scanner;

public class PS1_Task78C {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("a = ");
        double a = sc.nextDouble();
        System.out.print("n = ");
        int n = sc.nextInt();
        double sum = 0;
        double curr = a;
        for (int i = 0; i <= n; i++) {
            sum += 1 / curr;
            curr *= (a + i + 1);
        }
        System.out.println(sum);
    }
}