/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 77c
 */

import java.util.Scanner;

public class PS1_Task77C {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = sc.nextInt();
        double ans = 1;
        for (int i = 1; i <= n; i++) {
            ans *= (1 + 1.0/(i * i));
        }
        System.out.println(ans);
    }
}