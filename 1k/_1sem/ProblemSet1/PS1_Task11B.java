/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 11b
 */

import java.util.Scanner;

public class PS1_Task11B {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("y = ");
        double y = sc.nextDouble();
        System.out.print("z = ");
        double z = sc.nextDouble();
        double a = (3 + Math.exp(y - 1)) / (1 + x * x * Math.abs(y - Math.tan(z)));
        double abs = Math.abs(y - x);
        double b = 1 + abs + abs * abs /2 + abs*abs*abs/3;
        System.out.println("a = " + a);
        System.out.println("b = " + b);
    }
}