/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 83a
 */

import java.util.Scanner;

public class PS1_Task83A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("a = ");
        double a = sc.nextDouble();
        double prev = 1;
        double current = 1;
        int n = 2;
        do {
            prev = current;
            current += 1.0 / n;
            n += 1;
        } while (prev <= a);
        System.out.println(prev);
    }
}