/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 67d
 */

import java.util.Scanner;

public class PS1_Task67D {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n ( > 10) = ");
        int n = sc.nextInt();
        int mod = (n / 10) % 10;
        System.out.println(mod);
    }
}