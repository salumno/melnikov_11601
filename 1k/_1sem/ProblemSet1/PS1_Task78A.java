/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 78a
 */

import java.util.Scanner;

public class PS1_Task78A {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("a = ");
        double a = sc.nextDouble();
        System.out.print("n = ");
        int n = sc.nextInt();
        double ans = 1;
        for (int i = 1; i <= n; i++) {
            ans *= a;
        }
        System.out.println(ans);
    }
}