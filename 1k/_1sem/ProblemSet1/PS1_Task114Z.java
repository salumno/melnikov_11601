/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 114z
 */

public class PS1_Task114Z {

    public static void main(String[] args) {
        double fact = 1;
        double currentComp = 1;
        double comp = 1;
        for (int i = 2; i <= 10; i++) {
            fact *= i;
            currentComp = (1 - 1 / fact)*(1 - 1 / fact);
            comp *= currentComp;
        }
        System.out.println(comp);
    }
}