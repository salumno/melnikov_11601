/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 114d
 */

public class PS1_Task114D {

    public static void main(String[] args) {
        double currentComp = 1;
        double comp = 1;
        for (int i = 1; i <= 52; i++) {
            currentComp = (double)(i*i) / (i*i + 2*i + 3);
            comp *= currentComp;
        }
        System.out.println(comp);
    }
}