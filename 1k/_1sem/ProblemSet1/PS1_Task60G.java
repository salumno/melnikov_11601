/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 60г
 */

import java.util.Scanner;

public class PS1_Task60G {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        double ans = ((x*x + y*y <= 1) && (y >= 0) && ((x <= 0) || (x >= 0) && (x*x + y*y >= 0.09))) ? x*x - 1 : Math.sqrt(Math.abs(x-1));
        System.out.println(ans);
    }
}