/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 81
 */

import java.util.Scanner;

public class PS1_Task81 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("a = ");
        double a = sc.nextDouble();
        System.out.print("n = ");
        int n = sc.nextInt();
        double currentSqr = x;
        for (int i = 1; i <= n; i++) {
            currentSqr = (currentSqr + a) * (currentSqr + a);
        }
        currentSqr += a;
        System.out.println(currentSqr);
    }
}