/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 59b
 */

import java.util.Scanner;

public class PS1_Task59B {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        boolean flag;
        if (x * x + y * y >= 0.5 * 0.5 && x * x + y * y <= 1) {
            flag = true;
        } else {
            flag = false;
        }
        System.out.println(flag);
    }
}