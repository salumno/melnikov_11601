/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 31z
 */

import java.util.Scanner;

public class PS1_Task31Z {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("a = ");
        double a = sc.nextDouble();
        a = a * a * a; // 2 арифметические операции - a^3
        double temp = a;
        a = a * a; // 3 арифм. операции - a^6
        a = a * a; // 4 арифм. операции - a^12
        a = a * temp; // 5 арифм операция - a^15;
        System.out.println(a);
    }
}