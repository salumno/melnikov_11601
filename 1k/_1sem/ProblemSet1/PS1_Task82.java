/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 82
 */

import java.util.Scanner;

public class PS1_Task82 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        int currentPow = 2;
        double currentComp = 1;
        while (currentPow <= 64) {
            currentComp *= (x - currentPow)/(x - (currentPow - 1));
            currentPow *= 2;
        }
        System.out.println(currentComp);
    }
}