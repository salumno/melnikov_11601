/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 59д
 */

import java.util.Scanner;

public class PS1_Task59D {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        boolean flag;
        if ((y >= 2*x - 1) && (y >= -2*x - 1) && (y <= 2*x + 1) && (y <= -2*x + 1)) {
            flag = true;
        } else {
            flag = false;
        }
        System.out.println(flag);
    }
}