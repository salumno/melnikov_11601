/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 68a
 */

import java.util.Scanner;

public class PS1_Task68A {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        String n = sc.nextLine();
        boolean flag = true;
        for (int i = 0; i < n.length() / 2 & flag; i++) {
            if (n.charAt(i) != n.charAt(n.length() - i - 1)) {
                flag = false;
            }
        }
        System.out.println(flag);
    }
}