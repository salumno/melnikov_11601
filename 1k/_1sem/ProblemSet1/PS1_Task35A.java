/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 35a
 */

import java.util.Scanner;

public class PS1_Task35A {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("y = ");
        double y = sc.nextDouble();
        System.out.print("z = ");
        double z = sc.nextDouble();
        double sum = x + y + z;
        double comp = x * y * z;
        double ans = sum > comp ? sum : comp;
        System.out.println(ans);
    }
}