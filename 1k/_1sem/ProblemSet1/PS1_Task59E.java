/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 59e
 */

import java.util.Scanner;

public class PS1_Task59E {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        boolean flag;
        if ((x >= 0) && (x * x + y * y <= 1) || (x < 0) && (y <= 0.5 * x + 1) && (y >= -0.5 * x - 1)) {
            flag = true;
        } else {
            flag = false;
        }
        System.out.println(flag);
    }
}