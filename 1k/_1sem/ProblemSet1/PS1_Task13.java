/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 13
 */

import java.util.Scanner;

public class PS1_Task13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Length of pendulum = ");
        double l = sc.nextDouble();
        double t = 2 * Math.PI * Math.sqrt(l / 9.81);
        System.out.println(t);
    }
}