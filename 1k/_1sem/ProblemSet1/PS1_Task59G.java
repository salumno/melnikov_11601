/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 59г
 */

import java.util.Scanner;

public class PS1_Task59G {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        boolean flag;
        if ((y >= x - 1) && (y >= -x - 1) && (y <= x + 1) && (y <= -x + 1)) {
            flag = true;
        } else {
            flag = false;
        }
        System.out.println(flag);
    }
}