/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 84a
 */

import java.util.Scanner;

public class PS1_Task84A {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("n = ");
        double n = sc.nextInt();
        double currSin = Math.sin(x);
        double sum = currSin;
        for (int i = 2; i <= n; i++) {
            currSin *= Math.sin(x);
            sum += currSin;
        }
        System.out.println(sum);
    }
}