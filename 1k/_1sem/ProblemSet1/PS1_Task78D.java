/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 78d
 */

import java.util.Scanner;

public class PS1_Task78D {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("a = ");
        double a = sc.nextDouble();
        System.out.print("n = ");
        int n = sc.nextInt();
        double sum = 1 / a;
        double pow = a * a;
        double currPow = pow;
        for (int i = 1; i <= n; i++) {
            sum += 1 / currPow;
            currPow *= pow;
        }
        System.out.println(sum);
    }
}