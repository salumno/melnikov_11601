/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 79
 */

public class PS1_Task79 {
    public static void main(String[] args) {
        double ans = 1;
        double currSin = 0.1;
        while (currSin <= 10) {
            ans *= (1 + Math.sin(currSin));
            currSin += 0.1;
        }
        System.out.println(ans);
    }
}