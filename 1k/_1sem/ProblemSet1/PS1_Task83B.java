/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 83b
 */

import java.util.Scanner;

public class PS1_Task83B {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("a = ");
        double a = sc.nextDouble();
        double sum = 0;
        int n = 0;
        do {
            n += 1;
            sum += 1.0 / n;
        } while (sum <= a);
        System.out.println(n);
    }
}