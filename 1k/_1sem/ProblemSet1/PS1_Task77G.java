/*
 * Melnikov Semen
 * 11-601
 * Problem Set 1, Task 77g
 */

import java.util.Scanner;

public class PS1_Task77G {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = sc.nextInt();
        int count = n;
        double currSqrt = Math.sqrt(3*count);
        for (int i = 2; i <= n; i++) {
            count--;
            currSqrt = Math.sqrt(3*count + currSqrt);
        }
        System.out.println(currSqrt);
    }
}