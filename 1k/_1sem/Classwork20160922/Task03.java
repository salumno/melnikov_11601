/*
 * Меняем блоки массива местами.
 */

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Task03 {

    public static void backwardsArray (int startInx, int endInx, int[] arr) {
        int length = endInx - startInx + 1;
        int temp;
        int currentIndex = startInx;
        for (int i = 0; i < length / 2; i++) {
            temp = arr[currentIndex];
            arr[currentIndex] = arr[endInx - i];
            arr[endInx - i] = temp;
            currentIndex += 1;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int[] num = new int[10];
        for (int i = 0; i < num.length; i++) {
            num[i] = r.nextInt(20);
        }
        System.out.println(Arrays.toString(num));
        int k = sc.nextInt();
        backwardsArray(k+1, num.length - 1, num);
        backwardsArray(0, k-1, num);
        backwardsArray(0, num.length - 1, num);
        System.out.println(Arrays.toString(num));
    }
}