/*

*/

import java.util.Random;

public class Task01 {
    public static void main(String[] args) {
        Random r = new Random();
        int[] a = new int[5];
        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt(20);
            System.out.print(a[i] + " ");
        }
        System.out.println();
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
            }
        }
        System.out.println(max);
    }
}