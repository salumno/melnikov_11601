/*
 * Backwards array.
 */

import java.util.Random;

public class Task04 {
    public static void main(String[] args) {
        Random r = new Random();
        int[] a = {1, 2, 3, 4, 5, 6, 7};
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
        int temp = 1;
        for (int i = 0; i < a.length / 2; i++) {
            temp = a[i];
            a[i] = a[a.length - i - 1];
            a[a.length - i - 1] = temp;
        }
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
}