/*

*/

import java.util.Random;

public class Task02 {
    public static void main(String[] args) {
        Random r = new Random();
        int[] a = new int[5];
        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt(20);
            System.out.print(a[i] + " ");
        }
        System.out.println();
        int max = a[0];
        for (int x : a) {
            if (x > max) {
                max = x;
            }
        }
        System.out.println(max);
    }
}