import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Melnikov Semen
 * 11-601
 */

public class FindWordInTable {

    private final static int WEIGHT = 5;
    private final static int HEIGHT = 4;
    private final static int WORD_COUNT = 7;

    private static String lineInversion(String string) {
        char[] charArray = string.toCharArray();
        for (int i = 0; i < charArray.length / 2; i++) {
            char temp = charArray[i];
            charArray[i] = charArray[charArray.length - i - 1];
            charArray[charArray.length - i - 1] = temp;
        }
        return new String(charArray);
    }

    private static int[] prefixFunction(String s) {
        int[] pf = new int[s.length()];
        for (int i = 1; i < s.length(); i++) {
            int j = pf[i-1];
            while (j > 0 && s.charAt(i) != s.charAt(j)) {
                j = pf[j - 1];
            }
            if (s.charAt(i) == s.charAt(j)) {
                j++;
            }
            pf[i] = j;
        }
        return pf;
    }

    private static boolean findWordInString(String currentLine, String word) {
        String backwardsCurrentLine = lineInversion(currentLine);
        String concatString1 = word + "#" + currentLine;
        String concatString2 = word + "#" + backwardsCurrentLine;
        int[] pfString = prefixFunction(concatString1);
        boolean check1 = false;
        for (int i = 0; i < pfString.length && !check1; i++) {
            if (pfString[i] == word.length()) {
                check1 = true;
            }
        }
        pfString = prefixFunction(concatString2);
        boolean check2 = false;
        for (int i = 0; i < pfString.length && !check2; i++) {
            if (pfString[i] == word.length()) {
                check2 = true;
            }
        }
        return check1 || check2;
    }

    private static boolean rowCheck(char[][] table, String word, int i) {
        String currentLine = new String(table[i]);
        return findWordInString(currentLine, word);
    }

    private static boolean columnCheck(char[][] table, String word, int j) {
        String currentLine = "";
        for (int i = 0; i < table.length; i++) {
            currentLine += table[i][j];
        }
        return findWordInString(currentLine, word);
    }

    private static boolean diagonalCheck(char[][] table, String word) {
        return findWordInString(getDiagonalString(table), word);
    }

    private static String getDiagonalString(char[][] table) {
        String concatDiagonal = "";
        int line = 0;
        int column = 0;
        for (int i = 0; i < table[0].length; i++) {
            int x = line;
            int y = column;
            while (x < table.length && y < table[0].length) {
                concatDiagonal += table[x][y];
                x++;
                y++;
            }
            column++;
        }
        line = 1;
        column = 0;
        for (int i = 0; i < table.length - 1; i++) {
            int x = line;
            int y = column;
            while (x < table.length && y < table[0].length) {
                concatDiagonal += table[x][y];
                x++;
                y++;
            }
            line++;
        }
        line = 0;
        column = table[line].length - 1;
        for (int i = 0; i < table[0].length; i++) {
            int x = line;
            int y = column;
            while (x < table.length && y > -1) {
                concatDiagonal += table[x][y];
                x++;
                y--;
            }
            column--;
        }
        line = 1;
        column = table[0].length - 1;
        for (int i = 0; i < table.length - 1; i++) {
            int x = line;
            int y = column;
            while (x < table.length && y > 0) {
                concatDiagonal += table[x][y];
                x++;
                y--;
            }
            line++;
        }
        return concatDiagonal;
    }

    private static boolean wordCheck(char[][] table, String word) {
        boolean checkRow = false;
        for (int i = 0; i < table.length && !checkRow; i++) {
            checkRow = rowCheck(table, word, i);
        }

        boolean checkColumn = false;
        for (int i = 0; i < table[0].length && !checkColumn && !checkRow; i++) {
            checkColumn = columnCheck(table, word, i);
        }
        boolean checkDiagonal = false;
        if (!checkColumn && !checkRow) {
            checkDiagonal = diagonalCheck(table, word);
        }

        return checkColumn || checkRow || checkDiagonal;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("1.txt"));
        char[][] table = new char[HEIGHT][WEIGHT];
        int currentIndex = 0;
        do {
            String currentLine = sc.nextLine();
            char[] temp = currentLine.toCharArray();
            table[currentIndex] = temp;
            currentIndex++;
        } while (sc.hasNextLine());

        sc = new Scanner(new File("2.txt"));
        String[] word = new String[WORD_COUNT];
        currentIndex = 0;
        do {
            String currentLine = sc.nextLine();
            word[currentIndex] = currentLine;
            currentIndex++;
        } while (sc.hasNextLine());

        System.out.println(Arrays.toString(word));

        for (int i = 0; i < word.length; i++) {
            if (wordCheck(table, word[i])) {
                System.out.println(word[i]);
            }
        }
    }
}
