import javafx.scene.Group;

/**
 * Melnikov Semen
 * 11-601
 */

public class Button {

    private javafx.scene.control.Button button;
    private char[] letters;

    public Button(Group root, char[] lettersSet, javafx.scene.control.Button b, int x, int y) {
        letters = lettersSet;
        this.button = b;
        button.setText(new String(letters));
        button.setLayoutX(x);
        button.setLayoutY(y);
        root.getChildren().add(button);
    }

    public javafx.scene.control.Button getButton() {
        return button;
    }

    public char[] getLetters() {
        return letters;
    }
}
