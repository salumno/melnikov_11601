import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Melnikov Semen
 * 11-601
 */

public class Dictionary {

    private final int MAX_SIZE = 500;
    private int size;
    private WordField[] dictionary;

    public Dictionary() {
        size = 0;
        dictionary = new WordField[MAX_SIZE];
        try {
            Scanner sc = new Scanner(new File("dict.txt"));
            do {
                String currentLine = sc.nextLine();
                dictionary[size] = new WordField();
                dictionary[size].setCheck(true);
                dictionary[size].setWord(currentLine);
                size++;
            } while (sc.hasNextLine());
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public String wordCheck(char[] currentLetter, int currentIndex) {
        String firstNeededWord = "Didn't find";
        boolean neededWordCheck = true;
        for (int i = 0; i < size; i++) {
            if (dictionary[i].isCheck()) {
                try {
                    boolean checkLetter = false;
                    for (int j = 0; j < currentLetter.length && !checkLetter; j++) {
                        if (dictionary[i].getWord().charAt(currentIndex) == currentLetter[j]) {
                            checkLetter = true;
                            if (neededWordCheck) {
                                firstNeededWord = dictionary[i].getWord();
                                neededWordCheck = false;
                            }
                        }
                    }
                    if (!checkLetter) {
                        dictionary[i].setCheck(false);
                    }
                } catch (StringIndexOutOfBoundsException ex) {
                    //ex.printStackTrace();
                    continue;
                }
            }
        }
        return firstNeededWord;
    }

    public void vanish() {
        for (int i = 0; i < size; i++) {
            dictionary[i].setCheck(true);
        }
    }
}
