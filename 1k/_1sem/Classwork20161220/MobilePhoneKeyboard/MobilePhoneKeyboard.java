/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class MobilePhoneKeyboard extends Application {

    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        primaryStage.setTitle("SmartKeyboard");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));

        TextField userWindow = new TextField();
        userWindow.setText("User input");
        root.getChildren().add(userWindow);

        Dictionary dictionary = new Dictionary();
        LetterKeyboard letterKeyboard = new LetterKeyboard(root, userWindow, dictionary);

        primaryStage.show();
        root.requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
