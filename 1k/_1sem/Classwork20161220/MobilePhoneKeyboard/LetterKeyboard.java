import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.TextField;

/**
 * Melnikov Semen
 * 11-601
 */

public class LetterKeyboard {

    private Button[] button;
    private final byte LETTER_COUNT = 9;
    private byte clickCount;

    public LetterKeyboard(Group root, TextField userInput, Dictionary dictionary) {
        clickCount = 0;
        button = new Button[LETTER_COUNT];
        button[0] = new Button(root, new char[] {'B', 'S'}, new javafx.scene.control.Button(), 100, 100);
        button[1] = new Button(root, new char[] {'a','b','c'}, new javafx.scene.control.Button(), 150, 100);
        button[2] = new Button(root, new char[] {'d','e','f'}, new javafx.scene.control.Button(), 200, 100);
        button[3] = new Button(root, new char[] {'g','h','i'}, new javafx.scene.control.Button(), 100, 135);
        button[4] = new Button(root, new char[] {'j','k','l'}, new javafx.scene.control.Button(), 150, 135);
        button[5] = new Button(root, new char[] {'m','n','o'}, new javafx.scene.control.Button(), 200, 135);
        button[6] = new Button(root, new char[] {'p','q','r', 's'}, new javafx.scene.control.Button(), 100, 170);
        button[7] = new Button(root, new char[] {'t','u','v'}, new javafx.scene.control.Button(), 150, 170);
        button[8] = new Button(root, new char[] {'w','x','y','z'}, new javafx.scene.control.Button(), 200, 170);

        button[0].getButton().setOnAction(event -> {
            dictionary.vanish();
            clickCount = 0;
            userInput.setText("");
        });

        button[1].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[1].getLetters(), clickCount));
            clickCount++;
        });

        button[2].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[2].getLetters(), clickCount));
            clickCount++;
        });

        button[3].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[3].getLetters(), clickCount));
            clickCount++;
        });

        button[4].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[4].getLetters(), clickCount));
            clickCount++;
        });

        button[5].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[5].getLetters(), clickCount));
            clickCount++;
        });

        button[6].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[6].getLetters(), clickCount));
            clickCount++;
        });

        button[7].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[7].getLetters(), clickCount));
            clickCount++;
        });

        button[8].getButton().setOnAction(event -> {
            userInput.setText(dictionary.wordCheck(button[8].getLetters(), clickCount));
            clickCount++;
        });

    }
}
