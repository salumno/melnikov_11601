/**
 * Melnikov Semen
 * 11-601
 */

public class WordField {
    private String word;
    private boolean check;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String toString() {
        return word + " " + check;
    }
}
