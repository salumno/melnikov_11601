import java.util.Scanner;

public class MinNumSearch {

    public static int minNumSearch (int n) {
        int numeral = Math.abs(n);
        int min;
        if (numeral == 0) {
            min = 0;
        } else {
            min = 10;
        }
        while (numeral > 0) {
            int last = numeral % 10;
            min = Math.min(min, last);
            numeral = numeral / 10;
        }
        return min;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int ans = minNumSearch(n);
        System.out.println("Minimum numeral = " + ans);
    }

}
