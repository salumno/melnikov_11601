/**
 * Не доделано
 * 1)строку считали исходную, разбили её на слова
 * 2) Имеем особый словарь, который "отсортирован" не в алфавитном порядке, а по длине слов
 * 3) сохраняем его в массив
 * 4) Берем слово из исходной строки. мы знаем его длину. Бинарным поиском находим индекс первого слова из словаря, который имеет нужную нам длину
 * 5)теперь можно просто арифмитическим циклом идти по массиву, по слова имееют нужную длину (они же отсортированы)
 * 6) каждое слово побуквенно сравниваем с нашим исходным. 
 * 7) находим слово с минимальным кол-вом несовпадений.
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Task03 {

    public static int binarySearch(String[] arr, int length) {
        int index = -1;
        int left = 0;
        int right = arr.length - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (arr[mid].length == length) {
                index = mid;
                break;
            } else {
                if (length > arr[mid].length) {
                    left = mid + 1;
                } else {
                    right = mid;
                }
            }
        }
        return index;
    }   

    public static int mistakeCount(String word1, String word2) {
        word1 = word1.toLowerCase();
        word2 = word2.toLowerCase();
        int mistake = 0;
        for (int i = 0; i < word1.length; i++) {
            if (word1.charAt(i) != word2.charAt(i)) {
                mistake += 1;
            }
        }
        return mistake
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Task03_input.txt"));
        Scanner dic = new Scanner(new File("Dictionary.txt")); // А можно использовать sc, а не заводить новый Scanner?
        
        int size = 0;
        do {
            String str = sc.nextLine();
            size += 1;
        } while (sc.hasNextLine()); // посчитали количество строк в словаре (читай количество слов)
        String[] dictionary = new String[size];
        int inx = 0;
        sc = new Scanner(new File("input.txt"));
        do {
            dictionary[inx] = sc.nextLine();; // Храним все слова из словаря в оперативной памяти
            inx++;
        } while (sc.hasNextLine());

        PrintWriter pw = new PrintWriter("Task03_output.txt");
        String currentLine; // Текущая строка из input.
        String[] phrase; // Массив строк, в котором хранится разбитая на слова строка.
        String correctLine; // Текущая строка из input БЕЗ ОШИБОК
        do {
            currentLine = sc.nextLine(); // Считали строку из input
            phrase = currentLine.split(" "); // Разбили строку на слова
            for (String word : phrase) { // Пошли по массиву слов
                int start = binarySearch(dictionary, word.length); // Ищем в словаре индекс первого слова, которое имеет ту же длину, что и word.
                flag = true; // Пока слова нужной длины, идем по массиву словаря и сравниваем наше текущее слово со словарным.
                for (int i = start; flag; i++) { // Прем по массиву
                    if (dictionary[i].length != word.length) {
                        break; // TODO Булева переменная и break. Shit?
                    }
                    int mistakeCount = wordCompare(word, dictionary[i]);
                    if (mistakeCount < 2) {
                        correctLine = correctLine + dictionary[i];
                        break; // А если нет такого слова или ошибок всегда больше 1? 
                    }
                }
            }
        } while (sc.hasNextLine());
    }
}