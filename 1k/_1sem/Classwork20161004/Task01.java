/*
 * Sum of the elements from the input file.
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Task01 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Task01_input.txt"));
        PrintWriter pw = new PrintWriter("Task01_output.txt");
        int sum = 0;
        int currentNum;
        do {
            currentNum = sc.nextInt();
            sum += currentNum;
        } while(sc.hasNextInt());
        System.out.println(sum); 
        pw.println("Sum of the elements = " + sum);
        pw.close();
    }
}