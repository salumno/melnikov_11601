/*
 * SSH -> HTTPS
 * From git@bitbucket.org:salumno/melnikov_11601.git to https://salumno@bitbucket.org/salumno/melnikov_11601.git
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Task02 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Task02_input.txt"));
        PrintWriter pw = new PrintWriter("Task02_output.txt");
        String secure = "https://";
        String name;
        String end;
        String site;
        String link; 
        do {
            String currentLine = sc.nextLine();
            String[] temp1 = currentLine.split("@");
            String[] temp2 = temp1[1].split(":");
            site = temp2[0];
            String[] temp3 = temp2[1].split("/");
            name = temp3[0];
            end = temp3[1];
            link = secure + name + "@" + site + "/" + name + "/" + end;
            pw.println(link);
        } while (sc.hasNextLine());
        pw.close();
    }
}