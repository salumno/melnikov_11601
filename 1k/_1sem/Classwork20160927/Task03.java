/*
 * Проверка, все ли числа в массиве состоят из четных цифр.
 */

import java.util.Scanner;

public class Task03 {

    public static boolean figuresCheck (int n) {
        boolean flag = true;
        int mod;
        while (n > 0 && flag) {
            mod = n % 10;
            if (mod % 2 != 0) {
                flag = false;
            }
            n = n / 10;
        }
        return flag;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int curr;
        boolean flag = true;
        for (int i = 0; i < n && flag; i++) {
            curr = sc.nextInt();
            if (!figuresCheck(curr)) {
                flag = false;
            }
        }
        System.out.println(flag);
    }
}