/*
 * Проверка количества различный букв. Если количество различных букв равно n, то ответ считается положительным, иначе - нет.
 */

import java.util.Scanner;

public class Task04 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] alfa = new int[26];
        System.out.println("Write your line");
        String str = sc.nextLine();
        str = str.toLowerCase();
        int n = sc.nextInt();
        char currentSymbol;
        int index;
        for (int i = 0; i < str.length(); i++) {
            index = str.codePointAt(i) - 'a';
            alfa[index]++;
        }
        int count = 0;
        for (int i = 0; i < alfa.length; i++) {
            if (alfa[i] == 1) {
                count++;
            }
        }
        if (count == n) {
            System.out.println("Bingo!");
        } else {
            System.out.println("Bad situation ;(");
        }
    }
}