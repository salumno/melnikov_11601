/*
 * -1 2 -1 -5 -3
 * Есть ли во входных данных положительный элемент. 
 */

import java.util.Scanner;

public class Task01 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        boolean positive = false;
        int curr;
        for (int i = 0; i < n && !positive; i++) {
            curr = sc.nextInt();
            if (curr > 0) {
                positive = true;
            }
        }
        System.out.println(positive);
    }
}