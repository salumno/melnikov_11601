/*
 *  Есть ли во входных данных ровно 2 положительных элемента.
 */

import java.util.Scanner;

public class Task02 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        int curr;
        for (int i = 0; i < n && count < 2; i++) {
            curr = sc.nextInt();
            if (curr > 0) {
                count++;
            }
        }
        if (count == 2) {
            System.out.println("Two elements");
        } else {
            System.out.println("Haven't two elements");
        }
    }
}