/*  Melnikov Semen 11-601. Demidovich №42a
 *
 */

public class Task42A{

    public static void main (String[] args) {
        final double EPS = 1e-9;
        double n = 1;
        double prev = 0;
        double sign = 1;
        double current = 1;
        do {
            prev = current;
            n += 1;
            current = (-1) * sign / n;
            sign = (-1) * sign;
        } while (Math.abs(current - prev) > EPS);
        System.out.println(current);
    }
}