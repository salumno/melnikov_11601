/*  
 *  Melnikov Semen 11-601. Demidovich №55
 */

public class Task55 {

    public static void main (String[] args) {
        final double EPS = 1e-9;
        double n = 1;
        double prev = 0;
        double current = 0.5;
        double pow = 2;
        do {
            prev = current;
            n += 2;
            pow = pow * 2;
            current = prev + (n / pow);
        } while (Math.abs(current - prev) > EPS);
        System.out.println(current);
    }
}