/*  Melnikov Semen 11-601. Demidovich №42c
 *
 */

public class Task42C{

    public static void main (String[] args) {
        final double EPS = 1e-9;
        double prev = 0;
        double current = 1;
        double n = 1;
        double factorial = 1;
        do {
            prev = current;
            n += 1;
            factorial = factorial * n;
            current = 1 / factorial;
        } while (Math.abs(current - prev) > EPS);
        System.out.println(current);
    }
}