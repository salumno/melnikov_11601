/*  
 *  Melnikov Semen 11-601. Demidovich №42г
 */

public class Task42D {

    public static void main (String[] args) {
        final double EPS = 1e-9;
        double sign = 1;
        double prev = 0;
        double current = -0.999;
        double pow = 0.999;
        do {
            prev = current;
            pow = pow * 0.999;
            current = pow * sign;
            sign = (-1) * sign;
        } while (Math.abs(current - prev) > EPS);
        System.out.println(current);
    }
}