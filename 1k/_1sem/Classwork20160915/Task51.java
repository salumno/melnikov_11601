/*  
 *  Melnikov Semen 11-601. Demidovich №51
 */

public class Task51 {

    public static void main (String[] args) {
        final double EPS = 1e-9;
        double prev = 0;
        double sum = 0;
        double current = 0;
        double n = 1;
        do {
            prev = current;
            n += 1;
            sum += n - 1;
            current = sum / (n * n);
        } while (Math.abs(current - prev) > EPS);
        System.out.println(current);
    }
}