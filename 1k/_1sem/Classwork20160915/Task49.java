/*  
 *  Melnikov Semen 11-601. Demidovich №49
 */

public class Task49 {

    public static void main (String[] args) {
        final double EPS = 1e-9;
        double prev = 0;
        double current = 1 / 13;
        double twoPow = -2;
        double threePow = 3;
        do {
            prev = current;
            twoPow = twoPow * (-2);
            threePow = threePow * (3);
            current = (twoPow + threePow) / (twoPow * (-2) + threePow * 3);
        } while (Math.abs(current - prev) > EPS);
        System.out.println(current);
    }
}