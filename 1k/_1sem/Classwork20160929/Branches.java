/*
 * Убери палочки. 
 */

import java.util.Scanner;

public class Branches {

    public static void countCheck(int limit) {
        if (limit == 1) {
            System.out.println("Осталась 1 палочка!");
        } else {
            System.out.print("Осталось " + limit);
            if (limit > 1 && limit < 5) {
                System.out.println(" палочки!");
            } else {
                System.out.println(" палочек!");
            }
        }
    }

    public static void mapPrint(char[] map, int upperLimit) {
        if (upperLimit == 1) {
            System.out.println("Кажется, что сейчас кто-то сольется!");
        }
        countCheck(upperLimit);
        for (int i = 0; i < upperLimit; i++) {
            System.out.print(map[i] + " ");
        }
        System.out.println();
    }

    public static void currentPlayer(boolean player, String name1, String name2) {
        System.out.print("Хей, ");
        if (player) {
            System.out.print(name1 + ", ");
        } else {
            System.out.print(name2 + ", ");
        }
        System.out.println("вводи сколько палочек хочешь убрать!");
    }

    public static void loserText(boolean player, String name1, String name2) {
        System.out.print("Игрок под именем ");
        if (player) {
            System.out.print(name2);
        } else {
            System.out.print(name1);
        }
        System.out.println(" проиграл!");
    }

    public static void main(String[] args) {
        System.out.print("\033[H\033[2J"); // ? Screen cleaner
        Scanner sc = new Scanner(System.in);
        System.out.println("Привет, Игрок1! Как тебя зовут?");
        String name1 = sc.nextLine();
        System.out.println("Привет, Игрок2! Как тебя зовут?");
        String name2 = sc.nextLine();
        boolean gameCheck = true;
        while (gameCheck) {
            System.out.println("Сколько всего вы хотите палочек?");
            int size = sc.nextInt();
            char[] map = new char[size];
            int upperLimit = size;
            for (int i = 0; i < map.length; i++) {
                map[i] = '|';
            }
            boolean game = true;
            boolean player = true;
            int count = 0;
            int stepCount = 0;
            System.out.println("Игра началась!");
            while (game) {
                boolean fairPlayCheck = true;
                stepCount += 1;
                if (upperLimit == 0) {
                    loserText(player, name1, name2);
                    break;
                }
                mapPrint(map, upperLimit);
                currentPlayer(player, name1, name2);
                player = !player;
                while (fairPlayCheck) {
                    count = sc.nextInt();
                    if (upperLimit - count == 1 && stepCount == 1) {
                        System.out.println("Аккуратней с палочками! Вводите значение снова и впредь играйте честно!");
                    } else {
                        fairPlayCheck = false;
                    }
                }
                upperLimit -= count;
                System.out.println();
            }
            System.out.println("Конец игры! Хотите сыграть еще (Yes / No)?");
            sc.nextLine();
            String ans = sc.nextLine();
            if (ans.equals("No")) {
                gameCheck = false;
                System.out.println("Пока, пока!");
            }
        }
    }
}