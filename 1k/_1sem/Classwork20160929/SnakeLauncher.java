/*
 * Змейка
 */

import java.util.Random;
import java.util.Scanner;

class Coord {
    int row;
    int column;
}

class Snake {

    int length;

    // Элементы snake хранят свои текущие координаты на поле.
    Coord[] snake;

    // Изначальное заполнение
    public Snake(int size) {
        length = 4;
        int row = size / 2;
        int column = row;
        snake = new Coord[size * size];
        for (int i = 0; i < length; i++) {
            snake[i] = new Coord();
            snake[i].row = row;
            snake[i].column = column;
            column -= 1;
        }
    }

    // Обращение к конкретному элементу объекта класса Snake. Возвращает Coord.
    public Coord snakePoint(int i) {
        return snake[i];
    }

    // Простой сдвиг элементов змейки. На вход подаются координаты точки, в которую переместится голова.
    public void snakeMove(Coord coord) {
        if (!(snake[0].row == coord.row && snake[0].column == coord.column)) {
            for (int i = length - 1; i > 0; i--) {
                snake[i].row = snake[i - 1].row;
                snake[i].column = snake[i - 1].column; // Подвинули все элементы.
            }
            snake[0].row = coord.row; // Обновили голову
            snake[0].column = coord.column;
        }
    }

    // Удлинение змеи в случае поедания яблока.
    public void snakeAppleMove(Coord coord) {
        int tailRow = snake[length - 1].row;
        int tailCol = snake[length - 1].column;
        for (int i = length - 1; i > 0; i--) {
            snake[i].row = snake[i-1].row;
            snake[i].column = snake[i-1].column; // Подвинули все элементы.
        }
        snake[0].row = coord.row; // Обновили голову
        snake[0].column = coord.column;
        length += 1; // Увеличили длину на один элемент
        snake[length - 1] = new Coord();
        snake[length - 1].row = tailRow;// Сохранили настоящий хвостик.
        snake[length - 1].column = tailCol;
    }

}

class Field {

    char[][] field;
    final char HEAD = '0';
    final char BODY = 'z';
    final char EMPTY = '.';
    final char APPLE = '*';


    // Изначальная инициализация поля. Оно пустое.
    public Field(int size) {
        field = new char[size][size];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = EMPTY;
            }
        }
    }

    // Вывод текущего поля.
    public void fieldPrint(int score) {
        //System.out.print("\033[H\033[2J");
        System.out.println("Your current score " + score);
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    // Очистка поля. Применяется перед прокалыванием.
    public void vanish() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] != APPLE) {
                    field[i][j] = EMPTY;
                }
            }
        }
    }

    // Преобразование команды пользователя в координаты точки
    public Coord userCommand(String command, Coord coord) {
        int currRow = coord.row;
        int currCol = coord.column;
        switch (command) {
            case "W":
                currRow -= 1;
                break;
            case "S":
                currRow += 1;
                break;
            case "A":
                currCol -= 1;
                break;
            case "D":
                currCol += 1;
                break;
            default:
                System.out.println("Use only WASD controllers");
                break;
        }
        Coord newCoord = new Coord();
        newCoord.row = currRow;
        newCoord.column = currCol;
        return newCoord;
    }

    // Проверка корректности точки
    public boolean pointCheck(Coord currCoord, int size) {
        boolean flag = true;
        int row = currCoord.row;
        int col = currCoord.column;
        if (row >= size || row < 0 || col >= size || col < 0 || field[row][col] == BODY) {
            flag = false;
        }
        return flag;
    }

    // Проверка на яблочко
    public boolean appleCheck(Coord coord) {
        boolean flag = false;
        if (field[coord.row][coord.column] == APPLE) {
            flag = true;
        }
        return flag;
    }

    // Кладем яблочко в рандомное место на поле.
    public void appleShuffle(int size, Snake snake) {
        Random r = new Random();
        boolean check = false;
        int row = 0;
        int col = 0;
        while (!check) {
            check = true;
            row = r.nextInt(size - 1);
            col = r.nextInt(size - 1);
            for (int i = 0; i < snake.length && check; i++) {
                if (row == snake.snakePoint(i).row && col == snake.snakePoint(i).column) {
                    check = false;
                }
            }
        }
        field[row][col] = APPLE;
    }

    // Прокалывание поля - помещение змейки на травку.
    public void snakeToGrass(Snake snake) {
        int row = snake.snakePoint(0).row;
        int column = snake.snakePoint(0).column;
        field[row][column] = HEAD;
        for (int i = 1; i < snake.length; i++) {
            row = snake.snakePoint(i).row;
            column = snake.snakePoint(i).column;
            field[row][column] = BODY;
        }
    }

}

class SnakeGame {

    final int SIZE = 11;

    public void startGame() {
        Scanner sc = new Scanner(System.in);
        boolean repeat = true;
        int bestScore = -1;
        while (repeat) {
            Field grass = new Field(SIZE);
            Snake asp = new Snake(SIZE);
            int score = 0;
            grass.snakeToGrass(asp);
            grass.appleShuffle(SIZE, asp);
            grass.fieldPrint(score);
            boolean game = true;
            while (game) {
                Coord currCoord = asp.snakePoint(0);
                String command = sc.nextLine();
                Coord newCoord = grass.userCommand(command, currCoord);
                if (grass.pointCheck(newCoord, SIZE)) {
                    if (grass.appleCheck(newCoord)) {
                        score += 1;
                        asp.snakeAppleMove(newCoord);
                        grass.appleShuffle(SIZE, asp);
                    } else {
                        asp.snakeMove(newCoord);
                    }
                    grass.vanish();
                    grass.snakeToGrass(asp);
                    grass.fieldPrint(score);

                } else {
                    System.out.println("GAME OVER");
                    System.out.println("Your score " + score);
                    bestScore = score > bestScore ? score : bestScore;
                    System.out.println("Your best score " + bestScore);
                    game = false;
                    System.out.println("Do you wanna play again (YES / NO)?");
                    String ans = sc.nextLine();
                    if (ans.equals("NO")) {
                        repeat = false;
                        System.out.println("Bye Bye!");
                    }
                }
            }
        }
    }
}

public class SnakeLauncher {

    public static void main(String[] args) {
        SnakeGame newGame = new SnakeGame();
        newGame.startGame();
    }
}
