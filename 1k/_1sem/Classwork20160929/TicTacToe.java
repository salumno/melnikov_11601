/*
 *
 */

import java.util.Scanner;

public class TicTacToe {

    final static int SIZE = 3;
    final static char FIELD = '.';
    final static char CROSS = 'X';
    final static char ZERO = 'O';

    public static void fieldPrint(char[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void vanish(char[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = '.';
            }
        }
    }

    public static boolean pointCheck(char[][] field, int r, int c) {
        boolean flag = true;
        if (field[r-1][c-1] != FIELD) {
            flag = false;
        }
        return flag;
    }

    public static boolean diagonalCheck(char[][] field) {
        int mainCrossCount = 0;
        int mainZeroCount = 0;
        int secCrossCount = 0;
        int secZeroCount = 0;
        boolean check = false;
        for (int i = 0; i < field.length; i++) {
            switch(field[i][i]) {
                case CROSS:
                    mainCrossCount += 1;
                    break;
                case ZERO:
                    mainZeroCount += 1;
                    break;
            }
            switch(field[i][field.length - 1 - i]) {
                case CROSS:
                    secCrossCount += 1;
                    break;
                case ZERO:
                    secZeroCount += 1;
                    break;
            }
        }
        int l = field.length;
        if (mainZeroCount == l || mainCrossCount == l || secZeroCount == l || secCrossCount == l) {
            check = true;
        }
        return check;
    }

    public static boolean straightCheck(char[][] field, char symbol) {
        boolean check = false;
        char currentSymbol;
        for (int i = 0; i < field.length && !check; i++) {
            int crossCount = 0;
            int zeroCount = 0;
            for (int j = 0; j < field[i].length; j++) {
                currentSymbol = (symbol == 'r') ? field[i][j] : field[j][i];
                if (currentSymbol == CROSS) {
                    crossCount += 1;
                } else {
                    if (currentSymbol == ZERO) {
                        zeroCount += 1;
                    }
                }
            }
            if (crossCount == field[i].length || zeroCount == field[i].length) {
                check = true;
            }
        }
        return check;
    }

    public static boolean winCheck(char[][] field) {
        boolean flag = false;
        if (straightCheck(field, 'r') || straightCheck(field, 'c') || diagonalCheck(field)) {
            flag = true;
        }
        return flag;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[][] field = new char[SIZE][SIZE];
        vanish(field);
        int stepCount = 0;
        int maxStep = SIZE * SIZE;
        boolean gameCheck = true;
        boolean player = true;
        System.out.println("Первым ходит крестик");
        while (gameCheck) {
            System.out.println("Вводи координаты точки");
            fieldPrint(field);
            int r;
            int c;
            do {
                r = sc.nextInt();
                c = sc.nextInt();
                if (!pointCheck(field, r, c)) {
                    System.out.println("Эта клеточка уже занята. Вводи координаты еще раз!");
                }
            } while (!pointCheck(field, r, c));
            field[r-1][c-1] = player ? 'X' : 'O';
            stepCount += 1;
            if (winCheck(field)) {
                fieldPrint(field);
                String name = player ? "Крестики" : "Нолики";
                System.out.println(name + " победили!");
                gameCheck = false;
            }
            player = !player;
            if (stepCount == maxStep) {
                System.out.println("DRAW");
                fieldPrint(field);
                gameCheck = false;
            }
        }
    }
}