/*
 * Melnikov Semen 11-601. Task 01.
 * Исправлена ошибка с "целым" радиусом
 */

import java.util.Scanner;

public class Task01 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        double r = sc.nextDouble();
        double ans = 4 * Math.PI * r * r * r / 3;
        System.out.println(ans);
    }
}