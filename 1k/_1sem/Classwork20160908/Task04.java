/*
Melnikov Semen 11-601. Task 04.
*/

import java.util.Scanner;

public class Task04 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        double z = sc.nextDouble();
        double num1 = x + y + (z/2);
        double num2 = x*y*z;
        double min = num1;
        if (num2 < min) {
            min = num2;
        }
        double ans = min * min;
        System.out.println(ans);
    }
}