/*
Melnikov Semen 11-601. Task 07.
*/

import java.util.Scanner;

public class Task07 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int n = sc.nextInt();
        int ans = 1;
        for (int i = 0; i < n; i++) {
            ans = ans * x;
        }
        System.out.println(x + "^" + n + " = " + ans);
    }
}