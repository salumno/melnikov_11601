/*
Melnikov Semen 11-601. Task 03.
*/

import java.util.Scanner;

public class Task03 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        if (x % 2 == 0) {
            System.out.println("Even");
        } else {
            System.out.println("Odd");
        }
    }
}