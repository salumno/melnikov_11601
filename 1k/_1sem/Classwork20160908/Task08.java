/*
Melnikov Semen 11-601. Task 08.
*/

import java.util.Scanner;

public class Task08 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double sum = 0;
        if (n == 0) {
            System.out.println("Current number quals to zero");
        } else {
            for (int i = 2; i <= n; i += 2) {
                sum += (double)((i-1)*(i-1))/(i*i);
            }
            System.out.println("Sum = " + sum);
        }
    }
}