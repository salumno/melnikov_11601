/*
 * Melnikov Semen 11-601. Task 14.
 * Исправлена ошибка с "приписыванием" единиц. 
 * P.S. Строго говоря, в задании написано именно приписать.
*/

import java.util.Scanner;

public class Task14 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = n;
        int ans = 1;
        while (k > 0) {
            k = k / 10;
            ans *= 10;
        }
        n = (ans + n) * 10 + 1;
        System.out.println(n);
    }
}