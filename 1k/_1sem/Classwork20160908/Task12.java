/*
Melnikov Semen 11-601. Task 12.
*/

import java.util.Scanner;

public class Task12 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        while (n > 0) {
            int mod = n % 10;
            sum += mod;
            n = n / 10;
        }
        System.out.println("sum = " + sum);
    }
}