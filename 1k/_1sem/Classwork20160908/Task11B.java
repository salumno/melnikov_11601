/**
 * Melnikov Semen
 * 11-601
 * Task 11B
 */

import java.util.Scanner;

public class Task11B {

    public static int digitCount(int num) {
        int count = 0;
        while (num > 0) {
            count += 1;
            num /= 10;
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 1; i <= n; i++) {
            System.out.print(i);
            int sqrt = i*i;
            int space = digitCount(sqrt) - digitCount(i) + 1;
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }

        }
        System.out.println();
        for (int i = 1; i <= n; i++) {
            System.out.print(i*i + " ");
        }
        System.out.println();
    }
}