/*
Melnikov Semen 11-601. Task 09.
*/

import java.util.Scanner;

public class Task09 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        for (int i = 1; i < 11; i++) {
            int ans = k * i;
            System.out.println(k + "х" + i + " = " + ans);
        }
    }
}