/**
 * Melnikov Semen 11-601. Task 10.
 * UPD: Двумерный массив больше не используется. 
 * Массив table нужен только для хранения данных в условии коэффициентов при переменных.
 */

import java.util.Scanner;

public class Task10 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int table[] = {1, 10, 5, -12, 7, 10};
        int ans = 1;
        for (int i = 1; i < table.length; i++) {
            ans = table[i] + k * ans;
        }
        System.out.println(ans);
    }
}