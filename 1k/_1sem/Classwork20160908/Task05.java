/*
Melnikov Semen 11-601. Task 05.
*/

import java.util.Scanner;

public class Task05 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            System.out.println("Triangle is exist");
        } else {
            System.out.println("Triangle isn't exist");
        }
    }
}