/*
Melnikov Semen 11-601. Task 02.
*/

import java.util.Scanner;

public class Task02 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        boolean flag = false;
        if (x < -3.5 || x > 0) {
            flag = true;
        }
        if (flag) {
            System.out.println(x + " in interval");
        } else {
            System.out.println(x + " out interval");
        }
    }
}