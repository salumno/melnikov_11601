/*
Melnikov Semen 11-601. Task 11, first method.
*/

import java.util.Scanner;

public class Task11A {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 1; i <= n; i++) {
            int sqrNum = i * i;
            System.out.println(i + " " + sqrNum);
        }
    }
}