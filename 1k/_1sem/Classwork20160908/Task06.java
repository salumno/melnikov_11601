/*
Melnikov Semen 11-601. Task 06.
*/

import java.util.Scanner;

public class Task06 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        double discr = b*b - 4*a*c;
        if (discr < 0) {
            System.out.println("There is no decision");
        } else {
            if (discr == 0) {
                double x = (-b)/(2*a);
                System.out.println(x);
            } else {
                double x1 = (-b + Math.sqrt(discr))/(2*a);
                double x2 = (-b - Math.sqrt(discr))/(2*a);
                System.out.println("x1 = " + x1);
                System.out.println("x2 = " + x2);
            }
        }
    }
}