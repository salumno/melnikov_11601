/*
Melnikov Semen 11-601. Task 16.
*/

import java.util.Scanner;

public class Task16 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        double z = sc.nextDouble();
        x = x + 2;
        x = x * y;
        x -= z;
        x = x / y;
        y = y * z;
        x += y;
        System.out.println(x);
    }
}