/**
 * Melnikov Semen 11-601. Task 15.
 * Исправлено "неарифмитическое" построение нового числа.
 */

import java.util.Scanner;

public class Task15 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int temp = 0;
        while (n > 0) {
            int mod = n % 10;
            if (mod != k) {
                temp = (temp + mod) * 10;
            }
            n /= 10;
        }
        temp /= 10;
        int ans = 0;
        while (temp > 0) {
            ans = (ans + temp % 10) * 10;
            temp /= 10;
        }
        ans /= 10;
        System.out.println(ans);
    }
}