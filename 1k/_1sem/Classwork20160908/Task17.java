/*
Melnikov Semen 11-601. Task 17.
*/

import java.util.Scanner;

public class Task17 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        long x = sc.nextLong();
        x = x * x * x * x * x;
        x = x * x;
        x = x * x * x * x * x;
        System.out.println(x);
    }
}