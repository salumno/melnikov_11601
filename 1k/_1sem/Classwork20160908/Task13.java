/*
Melnikov Semen 11-601. Task 13.
*/

import java.util.Scanner;

public class Task13 {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int count = 0;
        while (n > 0) {
            int mod = n % 10;
            count += (mod == k) ? 1 : 0;
            n = n / 10;
        }
        System.out.println(count);
    }
}