/*

*/

class Player {

    private double hp = 10;
    private String name;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getHP() {
        return hp;
    }

    public void setHP(double hp) {
        this.hp = hp;
    }

    public void damageHP(double hit) {
        hp -= hit;
        System.out.println("У " + name + " осталось " + hp + " хп");
        System.out.println();
    }

}