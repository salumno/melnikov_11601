/*
 * Melnikov Semen
 * 11-601
 */

public class KickGameLauncher {
    public static void main(String[] args) {
        KickGame game = new KickGame();
        game.startGame();
    }
}