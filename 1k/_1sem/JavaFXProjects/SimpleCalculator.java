import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class SimpleCalculator extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        primaryStage.setTitle("Calculator");
        primaryStage.setScene(new Scene(root, 300, 300));

        TextField firstNum = new TextField();
        TextField secondNum = new TextField();

        root.getChildren().add(firstNum);
        root.getChildren().add(secondNum);

        String addition = "+";
        String subtraction = "-";
        String multiplication = "*";
        String division = "/";

        ComboBox mathOperationBox = new ComboBox();
        mathOperationBox.getItems().addAll(
                addition,
                subtraction,
                multiplication,
                division
        );
        mathOperationBox.setLayoutY(firstNum.getLayoutY() + 30);
        secondNum.setLayoutY(mathOperationBox.getLayoutY() + 30);
        root.getChildren().add(mathOperationBox);

        Button resultButton = new Button("RESULT");
        resultButton.setLayoutY(secondNum.getLayoutY() + 30);
        root.getChildren().add(resultButton);

        resultButton.setOnAction(event -> {
            double num1 = Double.parseDouble(firstNum.getText());
            double num2 = Double.parseDouble(secondNum.getText());
            String operation = mathOperationBox.getValue().toString();
            double result = 0;
            switch (operation) {
                case "+":
                    result = num1 + num2;
                    break;
                case "-":
                    result = num1 - num2;
                    break;
                case "*":
                    result = num1 * num2;
                    break;
                case "/":
                    result = num1 / num2;
                    break;
            }
            Alert alertResult = new Alert(Alert.AlertType.INFORMATION);
            alertResult.setTitle("Result");
            alertResult.setHeaderText(null);
            alertResult.setContentText(num1 + " " + operation + " " + num2 + " = " + result);
            alertResult.showAndWait();
            firstNum.clear();
            secondNum.clear();
        });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

