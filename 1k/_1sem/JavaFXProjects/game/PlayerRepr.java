package game;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class PlayerRepr {
    private int radius = 15;
    private Player player;
    private Circle circle;

    public PlayerRepr(Group root, Player p) {
        this.player = p;
        circle = new Circle(p.getX(), p.getY(), radius);
        root.getChildren().add(circle);
    }

    public int getRadius() {
        return radius;
    }

    public void updateX() {
        circle.setCenterX(player.getX());
    }

}
