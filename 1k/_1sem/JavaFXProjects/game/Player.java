package game;

import javafx.scene.Group;
import javafx.scene.control.TextField;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class Player {

    private int x;
    private int y;


    private int v;
    private int hp;
    private int score;
    private PlayerRepr pr;
    private TextField scoreTitle;
    private TextField hpTitle;

    public Player(int x, int y, Group group) {
        this.x = x;
        this.y = y;
        this.v = 8;
        this.score = 0;
        this.hp = 10;
        pr = new PlayerRepr(group, this);
        scoreTitle = new TextField("Current score: " + score);
        group.getChildren().add(scoreTitle);
        hpTitle = new TextField("Current HP: " + hp);
        hpTitle.setLayoutX(group.getScene().getWidth() - 170);
        group.getChildren().add(hpTitle);
    }

    public void updateX(int dir) {
        this.x = this.x + dir * v;
        pr.updateX();
    }

    public double getSpeed() {
        return v;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
        hpTitle.setText("Current HP: " + this.hp);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
        scoreTitle.setText("Current score: " + score);
    }

    public PlayerRepr getPr() {
        return pr;
    }
}
