package game;

/*
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;


public class Main extends Application {

    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;
    private Player player;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        primaryStage.setTitle("GAME");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));

        player = new Player(WIDTH / 2, HEIGHT - 30, root);

        Enemy enemies = new Enemy(WIDTH, HEIGHT, player, root);

        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case RIGHT:
                    if (player.getX() + player.getPr().getRadius() <= WIDTH) {
                        player.updateX(1);
                    }
                    break;
                case LEFT:
                    if (player.getX() - player.getPr().getRadius() >= 0) {
                        player.updateX(-1);
                    }
                    break;
                case SPACE:
                    Circle bullet = new Circle(player.getX(), player.getY(), 3);
                    root.getChildren().add(bullet);
                    new AnimationTimer() {

                        @Override
                        public void handle(long now) {
                            bullet.setCenterY(bullet.getCenterY() - 2);
                            for (int i = 0; i < enemies.getEnemies().size(); i++) {
                                boolean hitCheckX = bullet.getCenterX() - 10 <= enemies.getEnemies().get(i).getCenterX() && enemies.getEnemies().get(i).getCenterX() <= bullet.getCenterX() + 10;
                                boolean hitCheckY = bullet.getCenterY() - 3 <= enemies.getEnemies().get(i).getCenterY() && enemies.getEnemies().get(i).getCenterY() <= bullet.getCenterY() + 3;
                                if (hitCheckX && hitCheckY) {
                                    player.setScore(player.getScore() + 1);
                                    root.getChildren().remove(enemies.getEnemies().get(i));
                                    enemies.getEnemies().remove(i);
                                    bullet.setCenterY(0);
                                }
                            }

                            if (bullet.getCenterY() == 0) {
                                root.getChildren().remove(bullet);
                                this.stop();
                            }
                        }

                    }.start();
                    break;
            }
        });

        primaryStage.show();
        root.requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
