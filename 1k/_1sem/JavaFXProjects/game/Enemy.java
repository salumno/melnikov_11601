package game;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.shape.Circle;

import java.util.ArrayList;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class Enemy {

    private ArrayList<Circle> enemies;
    final private int speed = 1;

    public Enemy(int width, int height, Player player, Group root) {
        enemies = new ArrayList<>();

        new AnimationTimer() {
            long was = System.currentTimeMillis();
            @Override
            public void handle(long now) {
                for (int i = 0; i < enemies.size(); i++) {
                    enemies.get(i).setCenterY(enemies.get(i).getCenterY() + speed);
                }
                if (System.currentTimeMillis() - was > 5000) {
                    enemies.add(new Circle((int)(Math.random() * width), 0, 5));
                    enemies.get(enemies.size() - 1).setStyle("-fx-fill: red");
                    root.getChildren().add(enemies.get(enemies.size() - 1));
                    was = System.currentTimeMillis();
                }
                for (int i = 0; i < enemies.size(); i++) {
                    if (enemies.get(i).getCenterY() == height) {
                        player.setHp(player.getHp() - 1);
                        if (player.getHp() == 0) {
                            System.out.println("YOU LOSE");
                            this.stop();
                        }
                    }
                }
            }
        }.start();
    }

    public ArrayList<Circle> getEnemies() {
        return enemies;
    }

}
