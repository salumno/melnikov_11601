import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;


public class SceneCircle extends Application {

    final private static int STEP = 10;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 300));

        Circle circle = new javafx.scene.shape.Circle(30, 30, 20);
        circle.setStyle("-fx-fill: red");
        root.getChildren().add(circle);

        root.setOnKeyPressed(event -> {
            double width = root.getScene().getWidth();
            double height = root.getScene().getHeight();
            if (event.getCode() == KeyCode.RIGHT && circle.getCenterX() + circle.getRadius() + STEP <= width) {
                circle.setCenterX(circle.getCenterX() + STEP);
            } else if (event.getCode() == KeyCode.LEFT && circle.getCenterX() - circle.getRadius() - STEP >= 0) {
                circle.setCenterX(circle.getCenterX() - STEP);
            } else if (event.getCode() == KeyCode.UP && circle.getCenterY() - circle.getRadius() - STEP >= 0) {
                circle.setCenterY(circle.getCenterY() - STEP);
            } else if (event.getCode() == KeyCode.DOWN && circle.getCenterY() + circle.getRadius() + STEP <= height) {
                circle.setCenterY(circle.getCenterY() + STEP);
            }
        });

        primaryStage.show();
        primaryStage.getScene().getRoot().requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
