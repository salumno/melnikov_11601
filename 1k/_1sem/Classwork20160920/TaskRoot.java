/*

*/

import java.util.Scanner;

public class TaskRoot {

    final static double EPS = 10e-9;

    public static double ln (double y) {
        double x = y - 1;
        double current = x;
        double currentSum = current;
        double pow = x;
        int sign = -1;
        int n = 2;
        double prevSum = 0;
        do {
            prevSum = currentSum;
            pow = pow * x;
            current = sign * pow / n;
            n += 1;
            sign = -sign;
            currentSum = prevSum + current;
        } while (Math.abs(currentSum - prevSum) > EPS);
        return currentSum;
    }

    public static double exp (double a, double b) {
        double x = a * b;
        double current = 1;
        double prevSum = 0;
        double currentSum = current;
        double pow = 1;
        double fact = 1;
        int n = 1;
        do {
            prevSum = currentSum;
            pow = pow * x;
            fact = fact * n;
            n += 1;
            current = pow / fact;
            currentSum = prevSum + current;
        } while (Math.abs(currentSum - prevSum) > EPS);
        return currentSum;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Variable x in (0;2]");
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        y = 1 / y;
        if (x > 0 && x <= 2) {
            double ans = exp(ln(x), y);
            System.out.println("My result = " + ans);
            System.out.println("Java result = " + Math.pow(x,y));
        } else {
            System.out.println("Write a correct value for x from (0;2]");
        }    
    }
}