/*

*/

import java.util.Scanner;

public class TaskLn {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        System.out.println("Java result = " + Math.log(x));
        x -= 1;
        boolean numCheck = true;
        if (x > -1 && x <= 1) {
            numCheck = false;
        }
        if (numCheck) {
            System.out.println("Write a correct value from (0;2]");
        } else {
            final double EPS = 10e-9;
            double current = x;
            double currentSum = current;
            double pow = x;
            int sign = -1;
            int n = 2;
            double prevSum = 0;
            do {
                prevSum = currentSum;
                pow = pow * x;
                current = sign * pow / n;
                n += 1;
                sign = -sign;
                //System.out.println(current);
                currentSum = prevSum + current;
            } while (Math.abs(currentSum - prevSum) > EPS);
            System.out.println("My result = " + currentSum);
        }
    }
}