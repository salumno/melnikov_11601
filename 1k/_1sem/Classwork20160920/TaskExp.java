/*

*/

import java.util.Scanner;

public class TaskExp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        final double EPS = 10e-9;
        double current = 1;
        double prevSum = 0;
        double currentSum = current;
        double pow = 1;
        double fact = 1;
        int n = 1;
        do {
            prevSum = currentSum;
            pow = pow * x;
            fact = fact * n;
            n += 1;
            current = pow / fact;
            currentSum = prevSum + current;
        } while (Math.abs(currentSum - prevSum) > EPS);
        System.out.println();
        System.out.println("My result = " + currentSum);
        System.out.println("Java result = " + Math.exp(x));
    }
}