/*

*/

import java.util.Scanner;

public class TaskSin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        final double EPS = 10e-9;
        double fact = 1;
        double pow = x;
        double powGrowth = x*x;
        int sign = -1;
        double current = (1) * pow / fact;
        double prevSum = 0;
        double currentSum = current;
        int n = 2;
        do {
            prevSum = currentSum;;
            pow = pow * powGrowth;
            fact = fact * n * (n + 1);
            n += 2;
            current = (-1) * pow * sign / fact;
            sign = (-1) * sign;
            currentSum = prevSum + current;
        } while (Math.abs(currentSum - prevSum) > EPS);
        System.out.println();
        System.out.println("My result = " + currentSum);
        System.out.println("Java result = " + Math.sin(x));
    }
}