/*
 * Если основание степени (0;2], то корректная работа для любой степени.
 * В остальном корректная работа только для натуральных Y. 
 * Но, в отличие от возведения в степень через натуральный логарифм и экспоненту, основание степени - любое.
 */

import java.util.Scanner;

public class TaskPow2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        final double EPS = 10e-9;
        double x = sc.nextDouble();
        double y = sc.nextDouble();
        System.out.println("Java result = " + Math.pow(x, y));
        x -= 1;
        double current = 1;
        double currentSum = current;
        double prevSum = 0;
        int n = 1;
        double fact = 1;
        double currentY = y;
        double currentNumerator = 1;
        double prev = 0;
        do {
            prev = current;
            prevSum = currentSum;
            current = prev * (currentY * x / n);
            n += 1;
            currentY -= 1;
            currentSum = prevSum + current;
        } while (Math.abs(currentSum - prevSum) > EPS);
        System.out.println();
        System.out.println("My result = " + currentSum);
    }
}