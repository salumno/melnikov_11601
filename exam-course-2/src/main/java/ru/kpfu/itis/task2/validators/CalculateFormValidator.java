package ru.kpfu.itis.task2.validators;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Component;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.task2.form.CalculateForm;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Component
public class CalculateFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(CalculateForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        CalculateForm form = (CalculateForm) o;
        if (!StringUtils.isNumeric(form.getFirstDigit())) {
            errors.reject("bad.firstDigit", "Первый аргумент не является числом!");
        }
        if (!StringUtils.isNumeric(form.getSecondDigit())) {
            errors.reject("bad.secondDigit", "Второй аргумент не является числом!");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstDigit", "invalid.firstDigit", "Не введен первый аргумент");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "secondDigit", "invalid.secondDigit", "Не введен второй аргумент");
    }
}
