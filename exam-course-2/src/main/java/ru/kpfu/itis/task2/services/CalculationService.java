package ru.kpfu.itis.task2.services;

import ru.kpfu.itis.task2.form.CalculateForm;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface CalculationService {
    Double calculate(CalculateForm form);
}
