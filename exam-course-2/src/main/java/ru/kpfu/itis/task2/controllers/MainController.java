package ru.kpfu.itis.task2.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.task2.form.CalculateForm;
import ru.kpfu.itis.task2.services.CalculationService;
import ru.kpfu.itis.task2.validators.CalculateFormValidator;

import javax.validation.Valid;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Controller
public class MainController {

    private CalculateFormValidator calculateFormValidator;
    private CalculationService calculationService;

    public MainController(CalculateFormValidator calculateFormValidator, CalculationService calculationService) {
        this.calculateFormValidator = calculateFormValidator;
        this.calculationService = calculationService;
    }

    @InitBinder("calculateForm")
    public void initFormValidator(WebDataBinder binder) {
        binder.addValidators(calculateFormValidator);
    }

    @GetMapping("")
    public String getMainPage() {
        return "main-page";
    }

    @PostMapping("/calculate")
    public String calculateOperation(@Valid @ModelAttribute("calculateForm")CalculateForm form,
                                     BindingResult errors,
                                     RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            redirectAttributes.addFlashAttribute("form", form);
            return "redirect:/";
        }
        Double result = calculationService.calculate(form);
        if (result == null) {
            redirectAttributes.addFlashAttribute("error", "Проверьте правильность введенных данных!");
        } else {
            redirectAttributes.addFlashAttribute("result", result);
        }
        return "redirect:/";
    }
}
