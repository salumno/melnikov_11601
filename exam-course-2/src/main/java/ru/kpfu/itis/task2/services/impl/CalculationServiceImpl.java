package ru.kpfu.itis.task2.services.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.task2.form.CalculateForm;
import ru.kpfu.itis.task2.services.CalculationService;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Service
public class CalculationServiceImpl implements CalculationService {
    
    @Override
    public Double calculate(CalculateForm form) {
        Double firstDigit = Double.valueOf(form.getFirstDigit());
        Double secondDigit = Double.valueOf(form.getSecondDigit());
        switch (form.getOperation()) {
            case "*":
                return firstDigit * secondDigit;
            case "/":
                return firstDigit / secondDigit;
            case "+":
                return firstDigit + secondDigit;
            case "-":
                return firstDigit - secondDigit;
            default:
                return null;
        }
    }
}
