package ru.kpfu.itis.task1;

import lombok.SneakyThrows;

import java.io.*;
import java.util.*;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {

    private final static String IN_FILE_PATH = "src/main/java/ru/kpfu/itis/task1/in.txt";
    private final static String OUT_FILE_PATH = "src/main/java/ru/kpfu/itis/task1/out.txt";

    public static void main(String[] args) {
        Integer k = readNumberFromFile();
        List<String> strings = readStringsFromFile(k);
        sortCollectionOfStrings(strings, k);
    }

    @SneakyThrows
    private static Integer readNumberFromFile() {
        BufferedReader bf = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(IN_FILE_PATH)
                )
        );
        Integer number = Integer.valueOf(bf.readLine());
        bf.close();
        return number;
    }

    @SneakyThrows
    private static List<String> readStringsFromFile(Integer k) {
        BufferedReader bf = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(IN_FILE_PATH)
                )
        );
        List<String> strings = new ArrayList<>();
        String line = bf.readLine();
        while ((line = bf.readLine()) != null) {
            if (line.length() >= k) {
                strings.add(line);
            }
        }
        bf.close();
        return strings;
    }

    private static void sortCollectionOfStrings(List<String> strings, Integer k) {
        PrintWriter printWriter = createWriter();
        for (int i = 0; i < k; i++) {
            int currentPosition = k - i - 1;
            strings.sort((o1, o2) -> {
                char letterOfOneLine = o1.charAt(currentPosition);
                char letterOfAnotherLine = o2.charAt(currentPosition);
                return Character.compare(letterOfOneLine, letterOfAnotherLine);
            });
            writeStringsToFile(printWriter, strings);
        }
        printWriter.close();
    }

    @SneakyThrows
    private static PrintWriter createWriter() {
        return new PrintWriter(
                new OutputStreamWriter(
                        new FileOutputStream(OUT_FILE_PATH)
                )
                , true);
    }

    private static void writeStringsToFile(PrintWriter printWriter, List<String> strings) {
        for (String line: strings) {
            printWriter.println(line);
        }
        printWriter.println("");
    }
}
