<html>
<head>
</head>
<body>
<#if error??>
    <h1>${error}</h1>
</#if>
<#if result??>
    <h1>${result}</h1>
</#if>
<form action="/calculate" method="post">
    <#if error??>
        <input type="text" name="firstDigit" placeholder="First Digit" value="${form.firstDigit}" required>
        <input type="text" name="secondDigit" placeholder="Second Digit" value="${form.secondDigit}" required>
    <#else>
        <input type="text" name="firstDigit" placeholder="First Digit" required>
        <input type="text" name="secondDigit" placeholder="Second Digit" required>
    </#if>
    <label>
        <select name="operation">
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="/">/</option>
            <option value="*">*</option>
        </select>
    </label>
    <input type="submit">
</form>
</body>
</html>