import java.io.*;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

public class AppTable {
    public static void main(String[] args) {
        PrintWriter printWriter = getPrintWriter();
        printWriter.println("<html>");
        printWriter.println("<body>");
        printWriter.println("<table style=\"width:100%\">");
        printWriter.println("<tr>");
        for (int i = 2; i < 11; i++) {
            printWriter.println("<th>" + i + "</th>");
        }
        printWriter.println("</tr>");
        for (int i = 1; i < 11; i++) {
            printWriter.println("<tr>");
            for (int j = 2; j < 11; j++) {
                printWriter.println("<td>" + j + " * " + i + " = " + j * i + "</td>");
            }
            printWriter.println("</tr>");
        }
        printWriter.println("</table>");
        printWriter.println("</body>");
        printWriter.println("</html>");
        printWriter.close();
    }

    private static PrintWriter getPrintWriter() {
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(
                    new OutputStreamWriter(
                            new FileOutputStream("table.html")
                    )
            , true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return printWriter;
    }
}
