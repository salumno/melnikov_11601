package ru.kpfu.itis.g11601.dialog.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Client {
    private static boolean state = false;

    public static void main(String[] args) {
        int port = 1234;
        String host = "localhost";
        try {
            Socket socket = new Socket(host, port);
            PrintWriter printWriter = new PrintWriter(
                    socket.getOutputStream()
            );
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );
            while (true) {
                if (state) {
                    sendMessage(printWriter);
                } else {
                    String clientMessage = getMessage(bufferedReader);
                    System.out.println(clientMessage);
                }
                state = !state;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sendMessage(PrintWriter printWriter) {
        Scanner sc = new Scanner(System.in);
        String message = sc.nextLine();
        printWriter.println(message);
        printWriter.flush();
    }

    private static String getMessage(BufferedReader socketBufferedReader) {
        String message = "";
        String s;
        try {
            while ((s = socketBufferedReader.readLine()) == null) {
            }
            message += s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }
}
