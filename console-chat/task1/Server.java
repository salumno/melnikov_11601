package ru.kpfu.itis.g11601.dialog.task1;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Server {
    public static void main(String[] args) {
        final int port = 1234;
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            Socket client = serverSocket.accept();
            PrintWriter printWriter = new PrintWriter(
                    client.getOutputStream()
            );
            String message = "Привет! А тебя?";
            sendMessage(printWriter, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sendMessage(PrintWriter printWriter, String message) {
        printWriter.println(message);
        printWriter.close();
    }
}
