package ru.kpfu.itis.g11601.dialog.task1;

import java.io.*;
import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Client {
    public static void main(String[] args) {
        int port = 1234;
        String host = "localhost";
        try {
            Socket socket = new Socket(host, port);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );
            String message = getMessage(bufferedReader);
            System.out.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getMessage(BufferedReader socketBufferedReader) {
        String message = "";
        String s;
        try {
            while ((s = socketBufferedReader.readLine()) != null) {
                message += s;
            }
            socketBufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }
}
