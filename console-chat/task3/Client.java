package ru.kpfu.itis.g11601.dialog.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDate;
import java.util.Scanner;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Client {
    private static String name = "Clientik";
    private static String interlocutorName = "";

    private static boolean isGaveName = false;
    private static boolean state = false;

    public static void main(String[] args) {
        int port = 1234;
        String host = "localhost";
        try {
            Socket socket = new Socket(host, port);
            PrintWriter printWriter = new PrintWriter(
                    socket.getOutputStream()
            , true);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );
            while(!isGaveName) {
                if (state) {
                    sendName(printWriter);
                    isGaveName = true;
                } else {
                    interlocutorName = getInterlocutorName(bufferedReader);
                }
                state = !state;
            }
            while (true) {
                if (state) {
                    sendMessage(printWriter);
                } else {
                    String serverMessage = getMessage(bufferedReader);
                    System.out.println(LocalDate.now() + ". " + interlocutorName + ": " + serverMessage);
                }
                state = !state;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sendName(PrintWriter printWriter) {
        printWriter.println(name);
    }

    private static String getInterlocutorName(BufferedReader socketBufferedReader) {
        String message = "";
        String s;
        try {
            while ((s = socketBufferedReader.readLine()) == null) {
            }
            message += s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }

    private static void sendMessage(PrintWriter printWriter) {
        Scanner sc = new Scanner(System.in);
        String message = sc.nextLine();
        printWriter.println(message);
        printWriter.flush();
    }

    private static String getMessage(BufferedReader socketBufferedReader) {
        String message = "";
        String s;
        try {
            while ((s = socketBufferedReader.readLine()) == null) {
            }
            message += s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }
}
