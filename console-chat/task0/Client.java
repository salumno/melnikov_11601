package ru.kpfu.itis.g11601.dialog.task0;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Client {
    public static void main(String[] args) {
        int port = 1234;
        String host = "localhost";
        try {
            Socket socket = new Socket(host, port);
            InputStream inputStream = socket.getInputStream();
            int number = inputStream.read();
            System.out.println(number);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
