package ru.kpfu.itis.g11601.dialog.task0;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Server {
    public static void main(String[] args) {
        final int port = 1234;
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            Socket client = serverSocket.accept();
            OutputStream os = client.getOutputStream();
            int number = 5;
            os.write(number);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
