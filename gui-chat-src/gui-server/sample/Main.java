package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Main extends Application {

    private final int PORT = 1234;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Chatik Server");
        primaryStage.setScene(new Scene(root, 400, 400));
        primaryStage.show();
        Controller controller = loader.getController();

        ServerSocket serverSocket = new ServerSocket(PORT);
        Socket client = serverSocket.accept();
        PrintWriter printWriter = new PrintWriter(
                client.getOutputStream()
                , true);
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        client.getInputStream()
                )
        );

        controller.setClientSocketBufferedReader(bufferedReader);
        controller.setClientSocketPrintWriter(printWriter);
        controller.init();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
