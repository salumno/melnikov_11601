package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class Controller {
    @FXML
    private TextArea messageArea;

    @FXML
    private TextArea dialogArea;

    /*@FXML
    private Button buttonSend;*/

    private PrintWriter clientSocketPrintWriter;
    private BufferedReader clientSocketBufferedReader;
    private MessageWaitThread messageWaitThread;

    @FXML
    private void handleMessageSend() {
        String message = messageArea.getText();
        dialogArea.setText(dialogArea.getText() + "\n" + message);
        messageArea.clear();
        clientSocketPrintWriter.println(message);
    }

    public void setClientSocketPrintWriter(PrintWriter clientSocketPrintWriter) {
        this.clientSocketPrintWriter = clientSocketPrintWriter;
    }

    public void setClientSocketBufferedReader(BufferedReader clientSocketBufferedReader) {
        this.clientSocketBufferedReader = clientSocketBufferedReader;
    }

    public void init() {
        messageWaitThread = new MessageWaitThread(
                clientSocketPrintWriter,
                clientSocketBufferedReader,
                dialogArea
        );
        System.out.println("init loaded");
        messageWaitThread.start();
    }

}
