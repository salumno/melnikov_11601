package sample;

import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class MessageWaitThread extends Thread {

    private PrintWriter printWriter;
    private BufferedReader bufferedReader;
    private TextArea dialogTextArea;

    MessageWaitThread(PrintWriter printWriter, BufferedReader bufferedReader, TextArea dialogTextArea) {
        this.bufferedReader = bufferedReader;
        this.printWriter = printWriter;
        this.dialogTextArea = dialogTextArea;
    }

    @Override
    public void run() {
        System.out.println("Imma waiting");
        while (true) {
            String message = "";
            String s;
            try {
                while ((s = bufferedReader.readLine()) == null) {
                    System.out.println(1);
                }
                message += s;
            } catch (IOException e) {
                e.printStackTrace();
            }
            dialogTextArea.setText(dialogTextArea.getText() + "\n" + message);
        }
    }
}
