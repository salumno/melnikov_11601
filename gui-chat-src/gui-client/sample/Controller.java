package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class Controller {
    @FXML
    private TextArea messageArea;

    @FXML
    private TextArea dialogArea;

    /*@FXML
    private Button buttonSend;*/

    private PrintWriter serverSocketPrintWriter;
    private BufferedReader serverSocketBufferedReader;
    private MessageWaitThread messageWaitThread;

    @FXML
    private void handleMessageSend() {
        String message = messageArea.getText();
        dialogArea.setText(dialogArea.getText() + "\n" + message);
        messageArea.clear();
        serverSocketPrintWriter.println(message);
        
    }

    public void setServerSocketPrintWriter(PrintWriter serverSocketPrintWriter) {
        this.serverSocketPrintWriter = serverSocketPrintWriter;
    }

    public void setServerSocketBufferedReader(BufferedReader serverSocketBufferedReader) {
        this.serverSocketBufferedReader = serverSocketBufferedReader;
    }

    public void init() {
        messageWaitThread = new MessageWaitThread(
                serverSocketPrintWriter,
                serverSocketBufferedReader,
                dialogArea
        );
        messageWaitThread.start();
    }
}
