package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Main extends Application {

    private final int PORT = 1234;
    private final String HOST = "localhost";

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Chatik Client");
        primaryStage.setScene(new Scene(root, 400, 400));

        Socket socket = new Socket(HOST, PORT);
        PrintWriter printWriter = new PrintWriter(
                socket.getOutputStream()
                , true);
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        socket.getInputStream()
                )
        );

        Controller controller = loader.getController();
        controller.setServerSocketBufferedReader(bufferedReader);
        controller.setServerSocketPrintWriter(printWriter);
        controller.init();

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
