package ru.kpfu.itis.g11601.urlConnectionPractice;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public class Main {
    private static int count = 0;

    public static void main(String[] args) {
        loadDataFromServer();
    }

    private static List<String> getSubstringByPattern(String string) {
        System.out.println(string);
        List<String> strings = new ArrayList<>();
        Pattern pattern = Pattern.compile("(([а-яА-Яa-zA-Z0-9_-])*\\.pdf)");
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            strings.add(string);
        }
        System.out.println("String = " + strings);
        return strings;
    }

    private static void loadDataFromServer(String fileName) {
        URL url = null;
        try {
            url = new URL("file:///home/salumno/Downloads/" + fileName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            url.openStream()
                    )
            );
            BufferedWriter bufferedWriter = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(
                                    "File" + count + ".pdf"
                            )
                    )
            );
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null) {
                bufferedWriter.write(currentLine);
            }
            count++;
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void loadDataFromServer()  {
        URL url = null;
        try {
            url = new URL("file:///home/salumno/Downloads/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            url.openStream()
                    )
            );
            BufferedWriter bufferedWriter = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(
                                    "the-flow-temp"
                            )
                    )
            );
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null) {
                List<String> substrings = getSubstringByPattern(currentLine);
                for (String string: substrings) {
                    bufferedWriter.write(string);
                    loadDataFromServer(string);
                }
            }
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
