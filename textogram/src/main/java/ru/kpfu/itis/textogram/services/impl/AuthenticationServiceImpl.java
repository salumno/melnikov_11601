package ru.kpfu.itis.textogram.services.impl;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.textogram.model.User;
import ru.kpfu.itis.textogram.model.UserData;
import ru.kpfu.itis.textogram.repositories.UserDataRepository;
import ru.kpfu.itis.textogram.security.details.UserDetailsImpl;
import ru.kpfu.itis.textogram.services.AuthenticationService;

import java.util.Optional;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private UserDataRepository userDataRepository;

    public AuthenticationServiceImpl(UserDataRepository userDataRepository) {
        this.userDataRepository = userDataRepository;
    }

    @Override
    public User getUserByAuthentication(Authentication authentication) {
        return getUserDataByAuthentication(authentication).getUser();
    }

    @Override
    public UserData getUserDataByAuthentication(Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        UserData userData = userDetails.getUserData();
        Long userId = userData.getId();
        Optional<UserData> userDataOptional = userDataRepository.findOneById(userId);
        if (userDataOptional.isPresent()) {
            return userDataOptional.get();
        } else {
            throw new IllegalArgumentException("User not found");
        }
    }
}
