package ru.kpfu.itis.textogram.dto;

import lombok.*;
import ru.kpfu.itis.textogram.model.Comment;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class CommentDto {

    private Long id;

    private String value;

    private String authorName;

    private Long authorId;

    private CommentDto(Comment comment) {
        id = comment.getId();
        value = comment.getValue();
        authorId = comment.getAuthor().getId();
        authorName = comment.getAuthor().getName();
    }

    public static CommentDto from(Comment comment) {
        return new CommentDto(comment);
    }
}
