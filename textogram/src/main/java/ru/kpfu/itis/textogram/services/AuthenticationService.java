package ru.kpfu.itis.textogram.services;

import org.springframework.security.core.Authentication;
import ru.kpfu.itis.textogram.model.User;
import ru.kpfu.itis.textogram.model.UserData;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface AuthenticationService {
    User getUserByAuthentication(Authentication authentication);
    UserData getUserDataByAuthentication(Authentication authentication);
}
