package ru.kpfu.itis.textogram.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.textogram.model.Comment;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByPostId(Long postId);
}
