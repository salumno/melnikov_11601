package ru.kpfu.itis.textogram.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import ru.kpfu.itis.textogram.services.PostService;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Controller
public class PostController {
    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/posts/{id}")
    public String getPostPage(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id) {
        model.addAttribute("post", postService.getPostById(id));
        return "post-page";
    }
}
