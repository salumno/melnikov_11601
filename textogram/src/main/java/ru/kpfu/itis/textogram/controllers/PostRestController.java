package ru.kpfu.itis.textogram.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.textogram.dto.PostDto;
import ru.kpfu.itis.textogram.form.PostAddForm;
import ru.kpfu.itis.textogram.model.Post;
import ru.kpfu.itis.textogram.services.PostService;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private PostService postService;

    public PostRestController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/{postId}")
    public void userLikedPost(@PathVariable("postId") Long postId, Authentication authentication) {
        postService.addUserLike(postId, authentication);
    }

    @PostMapping("")
    public List<PostDto> addPost(@ModelAttribute PostAddForm form, Authentication authentication) {
        postService.addPost(form, authentication);
        return postService.getAllPostsDTO(authentication);
    }
}
