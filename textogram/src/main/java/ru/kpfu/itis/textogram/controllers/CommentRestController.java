package ru.kpfu.itis.textogram.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.textogram.dto.CommentDto;
import ru.kpfu.itis.textogram.form.CommentAddForm;
import ru.kpfu.itis.textogram.model.Comment;
import ru.kpfu.itis.textogram.services.CommentService;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {
    private CommentService commentService;

    public CommentRestController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("")
    public List<CommentDto> addComment(@ModelAttribute CommentAddForm form, Authentication authentication) {
        commentService.addComment(form, authentication);
        return commentService.getAllCommentsDTOByPostId(form.getPostId());
    }
}
