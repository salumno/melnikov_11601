package ru.kpfu.itis.textogram.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.kpfu.itis.textogram.services.AuthenticationService;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Controller
public class ProfileController {
    private AuthenticationService authenticationService;

    public ProfileController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @GetMapping("/profile")
    public String getProfilePage(@ModelAttribute("model")ModelMap model, Authentication authentication) {
        model.addAttribute("user", authenticationService.getUserByAuthentication(authentication));
        return "profile-page";
    }
}
