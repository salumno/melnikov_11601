package ru.kpfu.itis.textogram.services;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.textogram.dto.CommentDto;
import ru.kpfu.itis.textogram.form.CommentAddForm;
import ru.kpfu.itis.textogram.model.Comment;
import ru.kpfu.itis.textogram.model.Post;
import ru.kpfu.itis.textogram.model.User;
import ru.kpfu.itis.textogram.repositories.CommentRepository;
import ru.kpfu.itis.textogram.repositories.PostRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Service
public class CommentServiceImpl implements CommentService {

    private AuthenticationService authenticationService;
    private CommentRepository commentRepository;
    private PostRepository postRepository;

    public CommentServiceImpl(AuthenticationService authenticationService, CommentRepository commentRepository, PostRepository postRepository) {
        this.authenticationService = authenticationService;
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    @Override
    public void addComment(CommentAddForm form, Authentication authentication) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Post post = postRepository.findOneById(form.getPostId()).orElseThrow(
                () -> new IllegalArgumentException("Post with id: " + form.getPostId() + " not found.")
        );
        Comment comment = Comment.builder().author(user).post(post).value(form.getComment()).build();
        commentRepository.save(comment);
    }

    @Override
    public List<Comment> getAllCommentsByPostId(Long postId) {
        return commentRepository.findAllByPostId(postId);
    }

    @Override
    public List<CommentDto> getAllCommentsDTOByPostId(Long postId) {
        return getAllCommentsByPostId(postId).stream().map(CommentDto::from).collect(Collectors.toList());
    }
}
