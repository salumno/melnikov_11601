package ru.kpfu.itis.textogram.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.textogram.model.Post;

import java.util.Optional;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface PostRepository extends JpaRepository<Post, Long> {
    Optional<Post> findOneById(Long id);
}
