package ru.kpfu.itis.textogram.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.kpfu.itis.textogram.services.PostService;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Controller
public class MainPageController {

    private PostService postService;

    public MainPageController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/")
    public String getMainPage(@ModelAttribute("model")ModelMap model, Authentication authentication) {
        model.addAttribute("posts", postService.getAllPosts(authentication));
        return "main-page";
    }
}
