package ru.kpfu.itis.textogram.dto;

import lombok.*;
import ru.kpfu.itis.textogram.model.Post;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class PostDto {
    private Long id;

    private String value;

    private String description;

    private Long authorId;

    private String authorName;

    private Integer likeCount;

    private boolean like;

    private PostDto(Post post) {
        id = post.getId();
        value = post.getValue();
        description = post.getDescription();
        authorId = post.getAuthor().getId();
        authorName = post.getAuthor().getName();
        likeCount = post.getLikeCount();
        like = post.isLike();
    }

    public static PostDto from(Post post) {
        return new PostDto(post);
    }
}
