package ru.kpfu.itis.textogram.services;

import org.springframework.security.core.Authentication;
import ru.kpfu.itis.textogram.dto.PostDto;
import ru.kpfu.itis.textogram.form.PostAddForm;
import ru.kpfu.itis.textogram.model.Post;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface PostService {
    List<Post> getAllPosts();

    Post getPostById(Long id);

    List<Post> getAllPosts(Authentication authentication);

    void addUserLike(Long postId, Authentication authentication);

    void addPost(PostAddForm form, Authentication authentication);

    List<PostDto> getAllPostsDTO(Authentication authentication);
}
