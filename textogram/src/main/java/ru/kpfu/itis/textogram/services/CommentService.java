package ru.kpfu.itis.textogram.services;

import org.springframework.security.core.Authentication;
import ru.kpfu.itis.textogram.dto.CommentDto;
import ru.kpfu.itis.textogram.form.CommentAddForm;
import ru.kpfu.itis.textogram.model.Comment;

import java.util.List;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */
public interface CommentService {
    void addComment(CommentAddForm form, Authentication authentication);

    List<Comment> getAllCommentsByPostId(Long postId);

    List<CommentDto> getAllCommentsDTOByPostId(Long postId);
}
