package ru.kpfu.itis.textogram.services.impl;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.textogram.dto.PostDto;
import ru.kpfu.itis.textogram.form.PostAddForm;
import ru.kpfu.itis.textogram.model.Post;
import ru.kpfu.itis.textogram.model.User;
import ru.kpfu.itis.textogram.repositories.PostRepository;
import ru.kpfu.itis.textogram.repositories.UserRepository;
import ru.kpfu.itis.textogram.services.AuthenticationService;
import ru.kpfu.itis.textogram.services.PostService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Melnikov Semen
 * 11-601 ITIS KPFU
 */

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private UserRepository userRepository;
    private AuthenticationService authenticationService;

    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository, AuthenticationService authenticationService) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.authenticationService = authenticationService;
    }

    @Override
    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }

    @Override
    public Post getPostById(Long id) {
        return postRepository.findOneById(id).orElseThrow(
                () -> new IllegalArgumentException("Post with id: " + id + " not found.")
        );
    }

    @Override
    public List<Post> getAllPosts(Authentication authentication) {
        User user = authenticationService.getUserByAuthentication(authentication);
        List<Post> posts = getAllPosts();
        markPostsByUserLikes(posts, user.getLikedPost());
        return posts;
    }

    @Override
    public void addUserLike(Long postId, Authentication authentication) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Post post = getPostById(postId);
        post.setLikeCount(post.getLikeCount() + 1);
        user.getLikedPost().add(post);
        userRepository.save(user);
        postRepository.save(post);
    }

    @Override
    public void addPost(PostAddForm form, Authentication authentication) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Post post = Post.builder()
                .author(user)
                .value(form.getValue())
                .description(form.getDescription())
                .likeCount(0)
                .build();
        postRepository.save(post);
    }

    @Override
    public List<PostDto> getAllPostsDTO(Authentication authentication) {
        return getAllPosts(authentication).stream().map(PostDto::from).collect(Collectors.toList());
    }

    private void markPostsByUserLikes(List<Post> posts, List<Post> likedPost) {
        for (Post post: posts) {
            post.setLike(likedPost.contains(post));
        }
    }
}
