<html>
<head>
    <title>Textogram</title>
    <link rel="stylesheet" type="text/css" href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css"/>
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>
        Welcome, ${model.user.name}!
    </h2>
    <hr>
    <h3>
        Your info
    </h3>
    <p>
        ${model.user.info}
    </p>
    <hr>
    <h3>
        Your posts
    </h3>
    <#list model.user.posts as post>
    <div style="padding-top: 5px">
        <h4>${post.value} by ${post.author.name}</h4>
        <p>${post.description}</p>
        <p>Count of likes: ${post.likeCount}</p>
    </div>
    <hr>
    </#list>
</div>
</body>
</html>