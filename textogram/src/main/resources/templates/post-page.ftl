<html>
<head>
    <title>Textogram</title>
    <link rel="stylesheet" type="text/css" href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css"/>
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>${model.post.value} by ${model.post.author.name}</h2>
    <h3>${model.post.description}</h3>
    <h5>Count of likes: ${model.post.likeCount}</h5>
    <hr>
    <h3>Comments</h3>
    <div class="pre-scrollable" id="comments">
        <#list model.post.comments as comment>
            <h4>${comment.value}</h4>
            <p>by ${comment.author.name}</p>
            <hr>
        </#list>
    </div>
    <form id="comment-form">
        <div class="form-group">
            <label for="comment-textarea">Comment this post, please!</label>
            <textarea id="comment-textarea" name="comment" cols="20" class="form-control"></textarea>
            <button class="btn btn-success" type="button" onclick="addComment(${model.post.id})">Comment!</button>
        </div>
    </form>
</div>
<script>
    function addComment(postId) {
        var form = $('#comment-form')[0];
        var data = new FormData(form);
        data.append("postId", postId);

        $.ajax({
            url: '/api/comments',
            type: 'POST',
            data: data,
            dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data) {
                form.reset();
                fillComments(data);
            },
            error: function () {
                console.log("addComment produced the error!")
            }
        })
    }

    function fillComments(data) {
        $('#comments').html('');
        var comments = data;
        for (var i = 0; i < comments.length; i++) {
            var comment = comments[i];
            $('#comments').append(
                '<h4>' + comment.value + '</h4>' +
                '<p>by ' + comment.authorName + '</p>' +
                '<hr>'
            );
        }
    }
</script>
</body>
</html>