<html>
<head>
    <title>Textogram</title>
    <link rel="stylesheet" type="text/css" href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css"/>
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="pre-scrollable" id="posts">
        <#list model.posts as post>
            <div style="padding-top: 5px">
                <h4>${post.value} by ${post.author.name}</h4>
                <p>${post.description}</p>
                <p>Count of likes: <span id="like-count-${post.id}">${post.likeCount}</span></p>
            <#if post.like>
                <button id="like-button-${post.id}">Tvoy like naveki moy!!!</button>
            <#else>
                <button id="like-button-${post.id}" type="button" onclick="userLike(${post.id})">Like</button>
            </#if>
            </div>
            <a href="/posts/${post.id}">To Post</a>
        </#list>
    </div>
    <hr>
    <form id="post-form">
        <div class="form-group">
            <label for="post-value">Your post</label>
            <input id="post-value" name="value" type="text" class="form-control">
            <label for="post-description">Post Description</label>
            <textarea id="post-description" name="description" cols="20" class="form-control"></textarea>
            <button class="btn btn-success" type="button" onclick="addPost()">Post it!</button>
        </div>
    </form>
</div>
<script>
    function userLike(postId) {
        $.ajax({
            url: '/api/posts/' + postId,
            type: 'POST',
            success: function () {
                changeLikeButton(postId);
                //We have to change count of likes label too
            },
            error: function () {
                console.log("userLike produced the error!")
            }
        })
    }

    function changeLikeButton(postId) {
        $('#like-button-' + postId).html('Tvoy like naveki moy!!!');
    }

    function addPost() {
        var form = $('#post-form')[0];
        var data = new FormData(form);

        $.ajax({
            url: '/api/posts',
            type: 'POST',
            data: data,
            dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data) {
                form.reset();
                fillPosts(data);
            },
            error: function () {
                console.log("addPost produced the error!")
            }
        })
    }

    function fillPosts(data) {
        $('#posts').html('');
        var posts = data;
        for (var i = 0; i < posts.length; i++) {
            var post = posts[i];
            var buttonText = (post.like) ? "Tvoy like naveki moy!!!" : "Like";
            $('#posts').append(
            '<div style="padding-top: 5px">' +
                '<h4>' + post.value + ' by '  + post.authorName + '</h4>' +
                '<p>' + post.description + '</p>' +
                    '<p>Count of likes: <span id="like-count-' + post.id + '">' + post.likeCount + '</span></p>' +
                '<button id="like-button-' + post.id + '" onclick="userLike(' + post.id + ')">' + buttonText +'</button>' +
                '<a href="/posts/' + post.id + '">To Post</a>' +
            '</div>'
            );
        }
    }
</script>
</body>
</html>