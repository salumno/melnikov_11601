INSERT INTO "user"(name, info)
  SELECT 'Konstantin Kovalenko', 'Cool guy'
  WHERE NOT EXISTS(
      SELECT id
      FROM "user"
      WHERE id = 1
  );

INSERT INTO user_data(login, hash_password, user_id)
  SELECT 'user', '$2a$10$y3tC82XQRC3B5TZtPvz/SOTIK8Xgi7Gt0H2VoXj5hw6MNzWE4Vz/2', 1
  WHERE NOT EXISTS(
      SELECT id
      FROM user_data
      WHERE id = 1
  );